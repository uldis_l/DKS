#include <iostream>

#include "DKSBaseMuSR.h"

/** No accelerator device is used, this test is used to confirm, that search functions
 * used for auto-tuning work properly
 */

int main() {

  DKSBaseMuSR base;

  std::cout << "Start test" << std::endl;

  base.testAutoTuning();

  std::cout << "Test finished" << std::endl;
    
  

  return 0;
}
