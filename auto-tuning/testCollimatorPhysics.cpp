#include <iostream>
#include <vector>
#include <string>

#include "DKSOPAL.h"

typedef struct {
  int label;
  unsigned localID;
  double Rincol[3];
  double Pincol[3];
} PART;

PART initPartSmall(int d) {

  PART p;
  p.label = 0;
  p.localID = d;

  p.Rincol[0] = 0.0;
  p.Rincol[1] = 0.0;
  p.Rincol[2] = 0.02;

  p.Pincol[0] = 0.0;
  p.Pincol[1] = 0.0;
  p.Pincol[2] = 3.9920183237269791e-01;

  return p;
}

void printPart(PART p) {
  std::cout << "label: " << p.label << ", ";
  std::cout << "localid: " << p.localID << ",";
  std::cout << "Rincol: " << p.Rincol[0] << ", " << p.Rincol[1] << ", " << p.Rincol[2] << ", ";
  std::cout << "Pincol: " << p.Pincol[0] << ", " << p.Pincol[1] << ", " << p.Pincol[2];
  std::cout << std::endl;
}

void initParts(PART *p, int N) {
  for (int i = 0; i < N; i++)
    p[i] = initPartSmall(i);
}

void printParts(PART *p, int N) {
  for (int i = 0; i < N; i++)
    printPart(p[i]);
  std::cout << std::endl;
}

void initParams(double *data) {
  data[0]  = 0.0;//2.0000000000000000e-02;
  data[1]  = 1.0;//1.0000000000000000e-02;	
  data[2]  = 2.2100000000000000e+00;
  data[3]  = 6.0000000000000000e+00;	
  data[4]  = 1.2010700000000000e+01;	
  data[5]  = 2.6010000000000000e+00;	
  data[6]  = 1.7010000000000000e+03;	
  data[7]  = 1.2790000000000000e+03;	
  data[8]  = 1.6379999999999999e-02;	
  data[9]  = 1.9321266968325795e-01;	
  data[10] = 7.9000000000000000e+01;	
  data[11] = 1.0000000000000002e-12;
}

int main(int argc, char *argv[]) {

  int loop = 10;
  int numpart = 1e5;
  char *api_name = new char[10];
  char *device_name = new char[10];
  strcpy(api_name, "Cuda");
  strcpy(device_name, "-gpu");

  for (int i = 1; i < argc; i++) {

    if (argv[i] == std::string("-mic")) {
      strcpy(api_name, "OpenMP");
      strcpy(device_name, "-mic");
    }

    if (argv[i] == std::string("-npart")) {
      numpart = atoi(argv[i+1]);
      i++;
    }

    if (argv[i] == std::string("-loop")) {
      loop = atoi(argv[i+1]);
      i++;
    }

  }

  std::cout << "=========================BEGIN TEST=========================" << std::endl;
  std::cout << "Use api: " << api_name << "\t" << device_name << std::endl;
  std::cout << "Number of particles: " << numpart << std::endl;
  std::cout << "Number of loops: " << loop << std::endl;
  std::cout << "------------------------------------------------------------" << std::endl;

  //init part vector to test mc
  PART *parts = new PART[numpart];
  initParts(parts, numpart);

  double *params = new double[12];
  initParams(params);
  
  //init dks
  int ierr;
  DKSOPAL base;
  base.setAPI(api_name, strlen(api_name));
  base.setDevice(device_name, strlen(api_name));
  ierr = base.initDevice();
  if (ierr != DKS_SUCCESS)
    std::cout << "Error with init device!" << std::endl;

  //init random
  base.callInitRandoms(numpart);

  //**test collimator physics and sort***//
  void *part_ptr, *param_ptr;

  //allocate memory for particles
  part_ptr = base.allocateMemory<PART>(numpart, ierr);
  param_ptr = base.allocateMemory<double>(12, ierr);

  //transfer data to device
  base.writeData<PART>(part_ptr, parts, numpart);
  base.writeData<double>(param_ptr, params, 12);

  int numaddback;
  base.callCollimatorPhysics2(part_ptr, param_ptr, numpart);
  base.callCollimatorPhysicsSort(part_ptr, numpart, numaddback);  
  base.syncDevice();

  //read data from device
  base.readData<PART>(part_ptr, parts, numpart);

  //free memory
  base.freeMemory<PART>(part_ptr, numpart);
  base.freeMemory<double>(param_ptr, 12);  

  std::cout << std::fixed << std::setprecision(4);
  for (int i = 0; i < 10; i++) {
    std::cout << parts[i].label << "\t" 
	      << parts[i].Rincol[0] << "\t" << parts[i].Rincol[1] << "\t" 
	      << parts[i].Rincol[2] << "\t" << parts[i].Pincol[0] << "\t"
	      << parts[i].Pincol[1] << "\t" << parts[i].Pincol[2] << "\t"
	      << std::endl;
  }

  std:: cout << "..." << std::endl;
  
  for (int i = numpart - 10; i < numpart; i++) {
    std::cout << parts[i].label << "\t" 
	      << parts[i].Rincol[0] << "\t" << parts[i].Rincol[1] << "\t" 
	      << parts[i].Rincol[2] << "\t" << parts[i].Pincol[0] << "\t"
	      << parts[i].Pincol[1] << "\t" << parts[i].Pincol[2] << "\t"
	      << std::endl;
  }

  return 0;
}
