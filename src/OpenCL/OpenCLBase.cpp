#include "OpenCLBase.h"

cl_context OpenCLBase::m_context = NULL;
cl_command_queue OpenCLBase::m_command_queue = NULL;
cl_platform_id OpenCLBase::m_platform_id = NULL;
cl_device_id OpenCLBase::m_device_id = NULL;
cl_event OpenCLBase::m_last_event = NULL;

OpenCLBase::OpenCLBase() {
  m_program = NULL;
  m_kernel = NULL;
  m_kernel_file = NULL;
	
  m_last_event = NULL;
	
  defaultRndSet = 0;
}

OpenCLBase::~OpenCLBase() {
  ocl_cleanUp();
  m_last_event = NULL;

  if (defaultRndSet == 1)
    ocl_deleteRndStates();
}

/* create random states */
int OpenCLBase::ocl_createRndStates(int size) {
  //load kernel
  char * kernel_file = new char[500];
  kernel_file[0] = '\0';
  strcat(kernel_file, OPENCL_KERNELS);
  strcat(kernel_file, "OpenCL/OpenCLKernels/OpenCLCollimatorPhysics.cl");
  ocl_loadKernel(kernel_file);
  delete[] kernel_file;
  
  //allocate memory for rand states
  int ierr;
  defaultRndState = ocl_allocateMemory(sizeof(RNDState)*size, ierr);
  
  //exec kernel
  int seed = 0;
  ocl_createKernel("initRand");
  ocl_setKernelArg(0, sizeof(cl_mem), &defaultRndState);
  ocl_setKernelArg(1, sizeof(unsigned int), &seed);
  ocl_setKernelArg(2, sizeof(int), &size);
  
  size_t work_items = size;
  size_t work_group_size = 1;
  ocl_executeKernel(1, &work_items, &work_group_size);
  defaultRndSet = 1;
  
  return DKS_SUCCESS;
}

int OpenCLBase::ocl_createRandomNumbers(void *mem_ptr, int size) {
  //load kernel
  char * kernel_file = new char[500];
  kernel_file[0] = '\0';
  strcat(kernel_file, OPENCL_KERNELS);
  strcat(kernel_file, "OpenCL/OpenCLKernels/OpenCLCollimatorPhysics.cl");
  ocl_loadKernel(kernel_file);
  delete[] kernel_file;

  //set kernel variables
  cl_mem tmp_data = (cl_mem) mem_ptr;

  ocl_createKernel("createRandoms");
  ocl_setKernelArg(0, sizeof(cl_mem), &defaultRndState);
  ocl_setKernelArg(1, sizeof(cl_mem), &tmp_data);
  ocl_setKernelArg(2, sizeof(int), &size);

  size_t work_size = 128;
  size_t work_items = (size % work_size + 1) * work_size;
  ocl_executeKernel(1, &work_items, &work_size);

  return DKS_SUCCESS;
}

/* destroy rnd states */
int OpenCLBase::ocl_deleteRndStates() {

  ocl_freeMemory(defaultRndState);
  defaultRndSet = 0;

  return DKS_SUCCESS;

}


/*
  get platform id and device id of device specified by device_name (device name can be -mic, -cpu, -gpu, -all)
  finds the first device of the specified type and saves device id and platform id
*/
int OpenCLBase::ocl_getDevice(const char* device_name) {

  int ierr = 0;

  cl_platform_id *tmp_platform_ids;
  cl_uint num_of_platforms, num_of_devices;

  //get device type from name, return with error on failure
  ierr = ocl_getDeviceType(device_name, m_device_type);
  if (ierr != OCL_SUCCESS) {
    DEBUG_MSG("Can't find device, OpenCL error: " << ierr << ", " << device_name);
    return ierr;
  }

  //find all available platforms
  ierr = clGetPlatformIDs(0, NULL, &num_of_platforms);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find num platforms, OpenCL error: " << ierr); 
    return ierr;
  }
		
  tmp_platform_ids = new cl_platform_id[num_of_platforms];
  ierr = clGetPlatformIDs(num_of_platforms, tmp_platform_ids, NULL);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find platform id's, OpenCL error: " << ierr);
    return ierr;
  }

  //search each platform for specified device
  for (unsigned int i = 0; i < num_of_platforms; i++) {
	
    //get number of devices and first avaialble device id
    ierr = clGetDeviceIDs(tmp_platform_ids[i], m_device_type, 1, &m_device_id, &num_of_devices);
		
    if (ierr != CL_SUCCESS && ierr != CL_DEVICE_NOT_FOUND) {
      DEBUG_MSG("Can't find device id's, OpenCL error: " << ierr);
      return ierr;
    }
		
    //if device exists in current platform
    if (num_of_devices > 0) {
      //save platform id
      m_platform_id = tmp_platform_ids[i];

      //get the name of device that will be used and print its name
      size_t size;
      clGetDeviceInfo(m_device_id, CL_DEVICE_NAME, 0, NULL, &size);
			
      char* info = new char[size];
      clGetDeviceInfo(m_device_id, CL_DEVICE_NAME, size, info, NULL);

      DEBUG_MSG("Accelerator device: " << info);
      delete[] info;

      //get the name of the platform
      clGetPlatformInfo(m_platform_id, CL_PLATFORM_NAME, 0, NULL, &size);
      info = new char[size];
      clGetPlatformInfo(m_platform_id, CL_PLATFORM_NAME, size, info, NULL);
			
      DEBUG_MSG("Accelerator platform: " << info);

      return OCL_SUCCESS;
    }
  }

  return OCL_ERROR;
}

int OpenCLBase::ocl_getDeviceCount(int &ndev) {
  int ierr = DKS_SUCCESS;

  cl_platform_id *tmp_platform_ids;
  cl_uint num_of_platforms, num_of_devices, total_devices;
	
  //find platform count
  ierr = clGetPlatformIDs(0, NULL, &num_of_platforms);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find num of platforms, OpenCL error: " << ierr);
    return DKS_ERROR;
  }
	
  //find all platform IDs
  tmp_platform_ids = new cl_platform_id[num_of_platforms];
  ierr = clGetPlatformIDs(num_of_platforms, tmp_platform_ids, NULL);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find platform id's, OpenCL error: " << ierr);
    return ierr;
  }
	
  //for each platform find number of devices
  total_devices = 0;
  for (unsigned int i = 0; i < num_of_platforms; i++) {
    //get device count for platform
    ierr = clGetDeviceIDs(tmp_platform_ids[i], m_device_type, 0, NULL, &num_of_devices);
    if (ierr != CL_SUCCESS && ierr != CL_DEVICE_NOT_FOUND) {
      DEBUG_MSG("Can't find num of devices, OpenCL error: " << ierr);
      return OCL_ERROR;
    }
    total_devices += num_of_devices;
    num_of_devices = 0;
  }

  ndev = total_devices;
  return DKS_SUCCESS;
    
}

int OpenCLBase::ocl_getDeviceName(std::string &device_name) {

  int ierr = DKS_SUCCESS;
  size_t size;

  clGetDeviceInfo(m_device_id, CL_DEVICE_NAME, 0, NULL, &size);
  char* name = new char[size];
  clGetDeviceInfo(m_device_id, CL_DEVICE_NAME, size, name, NULL);

  device_name = name;
  delete[] name;
  return ierr;
}

int OpenCLBase::ocl_setDevice(int device) {
  
  int ierr;

  cl_device_id *tmp_device_ids;
  cl_platform_id *tmp_platform_ids;
  cl_int *tmp_device_counts;
  cl_uint num_of_platforms, num_of_devices;
  cl_uint total_devices = 0;

  //find all available platforms
  ierr = clGetPlatformIDs(0, NULL, &num_of_platforms);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find num platforms, OpenCL error: " << ierr); 
    return DKS_ERROR;
  }

  tmp_platform_ids = new cl_platform_id[num_of_platforms];
  tmp_device_counts = new cl_int[num_of_platforms];
  ierr = clGetPlatformIDs(num_of_platforms, tmp_platform_ids, NULL);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find platform id's, OpenCL error: " << ierr);
    return DKS_ERROR;
  }

  //search each platform for specified device
  for (unsigned int i = 0; i < num_of_platforms; i++) {

    //get the number of devices in the platform
    num_of_devices = 0;
    clGetDeviceIDs(tmp_platform_ids[i], m_device_type, 0, NULL, &num_of_devices);
    tmp_device_counts[i] = num_of_devices;
    total_devices += num_of_devices;
  }

  //check in which platform the selected device is located
  int tmp_count = 0;
  int checked_count = 0;
  int id = -1;
  int platform = -1; 
  for (unsigned int i = 0; i < num_of_platforms; i++) {
    tmp_count += tmp_device_counts[i];
    if (device < tmp_count) {
      id = device - checked_count;
      platform = i;
      break;
    }
    checked_count += tmp_device_counts[i];
  }

  ierr = DKS_ERROR;
  if (id > 0) {
    num_of_devices = tmp_device_counts[platform];
    tmp_device_ids = new cl_device_id[num_of_devices];
    clGetDeviceIDs(tmp_platform_ids[platform], m_device_type, num_of_devices, tmp_device_ids, NULL);

    m_device_id = tmp_device_ids[id];
    m_platform_id = tmp_platform_ids[platform];
    ierr = ocl_createContext();

    delete[] tmp_device_ids;
  }

  delete[] tmp_platform_ids;
  delete[] tmp_device_counts;

  return ierr;
}

int OpenCLBase::ocl_getUniqueDevices(std::vector<int> &devices) {

  int ierr;

  size_t size;
  cl_device_id *tmp_device_ids;
  cl_platform_id *tmp_platform_ids;
  cl_uint num_of_platforms, num_of_devices;

  //find all available platforms
  ierr = clGetPlatformIDs(0, NULL, &num_of_platforms);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find num platforms, OpenCL error: " << ierr); 
    return DKS_ERROR;
  }

  tmp_platform_ids = new cl_platform_id[num_of_platforms];
  ierr = clGetPlatformIDs(num_of_platforms, tmp_platform_ids, NULL);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find platform id's, OpenCL error: " << ierr);
    return DKS_ERROR;
  }

  std::vector< std::string > names;
  int checked_count = 0;
  int id = 0;
  for (unsigned int i = 0; i < num_of_platforms; i++) {

    //get the number of devices in the platform
    num_of_devices = 0;
    clGetDeviceIDs(tmp_platform_ids[i], m_device_type, 0, NULL, &num_of_devices);
    tmp_device_ids = new cl_device_id[num_of_devices];
    clGetDeviceIDs(tmp_platform_ids[i], m_device_type, num_of_devices, tmp_device_ids, NULL);
    
    for (unsigned int j = 0; j < num_of_devices; j++) {
      id = checked_count + j;
      clGetDeviceInfo(tmp_device_ids[j], CL_DEVICE_NAME, 0, NULL, &size);
      char* name = new char[size];
      clGetDeviceInfo(tmp_device_ids[j], CL_DEVICE_NAME, size, name, NULL);
      std::string target = name;
      if (id == 0) {
	devices.push_back(id);
	names.push_back(target);
      } else {
	bool isPresent = (std::find(names.begin(), names.end(), target) != names.end());
	if (!isPresent) {
	  devices.push_back(id);
	  names.push_back(target);
	}
      }
      delete[] name;
    }

    checked_count += num_of_devices;
    delete[] tmp_device_ids;
  }

  delete[] tmp_platform_ids;

  return DKS_SUCCESS;
}

/*
  checks wether device name is specified and sets device type to search for
  if invalid device name is specified set device type to default
*/
int OpenCLBase::ocl_getDeviceType(const char* device_name, cl_device_type &device_type) {

  device_type = CL_DEVICE_TYPE_DEFAULT;	

  if (strcmp(device_name, "-mic") == 0)
    device_type = CL_DEVICE_TYPE_ACCELERATOR;

  if (strcmp(device_name, "-cpu") == 0) 
    device_type = CL_DEVICE_TYPE_CPU;

  if (strcmp(device_name, "-gpu") == 0)
    device_type = CL_DEVICE_TYPE_GPU;
	
  if (strcmp(device_name, "-all") == 0)
    device_type = CL_DEVICE_TYPE_ALL;

  return OCL_SUCCESS;
}

/*
  creates a context and command queue between host and device
*/
int OpenCLBase::ocl_createContext() {
  int ierr;
	
  //context properties list
  m_context_properties[0] = CL_CONTEXT_PLATFORM;
  m_context_properties[1] = (cl_context_properties) m_platform_id;
  m_context_properties[2] = 0;

  //create a context with specified device
  m_context = clCreateContext(m_context_properties, 1, &m_device_id, NULL, NULL, &ierr);
	
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't create context, OpenCL error: " << ierr);
    return ierr;
  }

  //create command queue using context and device
  //m_command_queue = clCreateCommandQueue(m_context, m_device_id, CL_QUEUE_PROFILING_ENABLE, &ierr);
  m_command_queue = clCreateCommandQueue(m_context, m_device_id, 0, &ierr);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't create command queue, OpenCL error: " << ierr);
    return ierr;
  }
		
  return OCL_SUCCESS;
}

/*
  read file specified by kernel_file and compile the kernel code contained in kernel_file
  save reference to the built program to m_program, from witch individual kernels can be extracted
*/
int OpenCLBase::ocl_buildProgram(const char *kernel_file) {
	
  cl_int ierr;
  long fsize;
  char *kernel_source;
	
  //open file
  FILE *fp = fopen(kernel_file, "rb");
  if (!fp) {
    DEBUG_MSG("Can't open kernel file: " << kernel_file);
    return OCL_ERROR;
  }
	
  //get file size and allocate memory	
  fseek(fp, 0, SEEK_END);
  fsize = ftell(fp);
  kernel_source = new char[fsize+1];
	
  //read file and content in kernel source
  rewind(fp);
  fread(kernel_source, 1, sizeof(char)*fsize, fp);
  kernel_source[fsize] = '\0';
  fclose(fp);
	
  ierr = ocl_compileProgram(kernel_source);

  //save currently loaded kernel file
  m_kernel_file = new char[strlen(kernel_file) + 1];
  strcpy(m_kernel_file, kernel_file);

  return ierr;

}

//given kernel source compile the OpenCL programm
int OpenCLBase::ocl_compileProgram(const char* kernel_source, const char* opts) {
  
  int ierr;

  //create program from kernel
  m_program = clCreateProgramWithSource(m_context, 1, (const char **)&kernel_source, 
					NULL, &ierr);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Error creating program from source, OpenCL error: " << ierr);
    return DKS_ERROR;
  }
	
  //compile the program, if compilation
  ierr = clBuildProgram(m_program, 0, NULL, opts, NULL, NULL);
	
  /*
    check if compiling kernel source succeded, if failed return error code
    if in debug mode get compilation info and print program build log witch
    will give indication what made the compilation fail
  */
#ifdef DEBUG
  if (ierr != CL_SUCCESS) {
		
    //get build status
    cl_build_status status;
    clGetProgramBuildInfo(m_program, m_device_id, CL_PROGRAM_BUILD_STATUS, 
			  sizeof(cl_build_status), &status, NULL);

    //get log size
    size_t log_size;
    clGetProgramBuildInfo(m_program, m_device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		
    //get log message
    char *log = new char[log_size];
    clGetProgramBuildInfo(m_program, m_device_id, CL_PROGRAM_BUILD_LOG, log_size+1, log, NULL);
		
    //print log messsage
    DEBUG_MSG("Build failed! Status:" << status);
    DEBUG_MSG("LOG: " << log);
		
    delete[] log;
		
    return DKS_ERROR;
  }
#else
  if (ierr != CL_SUCCESS)
    return DKS_ERROR;
#endif
		
  return DKS_SUCCESS;

}



//=========================================//
//===============public functions==========//
//=========================================//

/*
  get all device from all platforms
*/
int OpenCLBase::ocl_getAllDevices() {

  int ierr = DKS_SUCCESS;
	
  cl_platform_id *tmp_platform_ids, *platform_ids;
  cl_uint num_of_platforms, num_of_devices, total_devices;
  cl_device_id *tmp_device_ids, *device_ids;
	
  //find platform count
  ierr = clGetPlatformIDs(0, NULL, &num_of_platforms);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find num of platforms, OpenCL error: " << ierr);
    return OCL_ERROR;
  }
	
  //find all platform IDs
  tmp_platform_ids = new cl_platform_id[num_of_platforms];
  ierr = clGetPlatformIDs(num_of_platforms, tmp_platform_ids, NULL);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Can't find platform id's, OpenCL error: " << ierr);
    return ierr;
  }
	
  //for each platform find number of devices
  total_devices = 0;
  for (unsigned int i = 0; i < num_of_platforms; i++) {
    //get device count for platform
    ierr = clGetDeviceIDs(tmp_platform_ids[i], CL_DEVICE_TYPE_ALL, 0, NULL, &num_of_devices);
    if (ierr != CL_SUCCESS) {
      DEBUG_MSG("Can't find num of devices, OpenCL error: " << ierr);
      return OCL_ERROR;
    }
    total_devices += num_of_devices;
  }
	
  //get all device ids
  int idx = 0;
  platform_ids = new cl_platform_id[total_devices];
  device_ids = new cl_device_id[total_devices];
  tmp_device_ids = new cl_device_id[total_devices];
	
  for (unsigned int i = 0; i < num_of_platforms; i++) {
    //get device ids
    ierr = clGetDeviceIDs(tmp_platform_ids[i], CL_DEVICE_TYPE_ALL, total_devices, tmp_device_ids, &num_of_devices);
    if (ierr != CL_SUCCESS) {
      DEBUG_MSG("Can't find num of devices, OpenCL error: " << ierr);
      return OCL_ERROR;
    }
		
    for (unsigned j = 0; j < num_of_devices; j++) {
      platform_ids[idx] = tmp_platform_ids[i];
      device_ids[idx] = tmp_device_ids[j];
      idx++;
    }
  }
	
  std::cout << std::endl;
  std::cout << "==============================" << std::endl;
  std::cout << "============OpenCL============" << std::endl;
  std::cout << "==============================" << std::endl;
	
  for (unsigned int i = 0; i < total_devices; i++) {
	
    //get the name of device that will be used and print its name
    size_t size;
		
    DEBUG_MSG("Device " << i+1 << ":");
		
    clGetDeviceInfo(device_ids[i], CL_DEVICE_NAME, 0, NULL, &size);	
    char *device_name = new char[size];
    clGetDeviceInfo(device_ids[i], CL_DEVICE_NAME, size, device_name, NULL);
    DEBUG_MSG("Name: \"" << device_name << "\"");
		
    clGetDeviceInfo(device_ids[i], CL_DEVICE_VENDOR, 0, NULL, &size);	
    char *device_vendor = new char[size];
    clGetDeviceInfo(device_ids[i], CL_DEVICE_VENDOR, size, device_vendor, NULL);
    DEBUG_MSG("Vendor: \"" << device_vendor << "\"");
		
    cl_device_type device_type;
    clGetDeviceInfo(device_ids[i], CL_DEVICE_TYPE, sizeof(cl_device_type), &device_type, NULL);

    if (device_type == CL_DEVICE_TYPE_GPU) {
      DEBUG_MSG("Device type: GPU");
    } else if (device_type == CL_DEVICE_TYPE_CPU) {
      DEBUG_MSG("Device type: CPU");
    } else if (device_type == CL_DEVICE_TYPE_ACCELERATOR) {
      DEBUG_MSG("Device type: Accelerator");
    }
	
    std::cout << "==============================" << std::endl;	
	
  }
	
  return OCL_SUCCESS;
}


/*
  find available device, create context and command queue, load kernel file and kompile kernel code
*/
int OpenCLBase::ocl_setUp(const char *device_name) {
  cl_int ierr;
  ierr = ocl_getDevice(device_name);
  if (ierr != CL_SUCCESS)
    return ierr;
	
  ocl_deviceInfo(false);
	
  ierr = ocl_createContext();
  if (ierr != CL_SUCCESS)
    return ierr;
  
  return DKS_SUCCESS;
}

/*
  load and compile kernel file if it has changed
*/
int OpenCLBase::ocl_loadKernel(const char * kernel_file) {
  int ierr = OCL_SUCCESS;
	
  //kernel file has changed
  if (m_kernel_file == NULL) {
    ierr = ocl_buildProgram(kernel_file);
  } else {
    if (strcmp(m_kernel_file, kernel_file) != 0) {
      ierr = ocl_buildProgram(kernel_file);
    }
  }
	
  if (ierr != DKS_SUCCESS) {
    DEBUG_MSG("Failed to build kernel file " << kernel_file);
    return DKS_ERROR;
  }
	
  return DKS_SUCCESS;
}

//compile kernel form source code provided
int OpenCLBase::ocl_loadKernelFromSource(const char *kernel_source, const char *opts) {

  int ierr = ocl_compileProgram(kernel_source, opts);

  return ierr;
}

/*
  Allocate memory buffer of specified size and type, 
  available types (read only, write only, read/write)
  return memory object
*/
cl_mem OpenCLBase::ocl_allocateMemory(size_t size, int type, cl_int &ierr) {
  cl_mem mem;
  mem = clCreateBuffer(m_context, type, size, NULL, &ierr);
  if (ierr != CL_SUCCESS)
    DEBUG_MSG("Error allocating memory, OpenCL error: " << ierr);
	
  return mem;
}

/*
  Allocate memory buffer of specified size, type is set to read/write
  return memory object
*/
cl_mem OpenCLBase::ocl_allocateMemory(size_t size, cl_int &ierr) {
  cl_mem mem;

  mem = clCreateBuffer(m_context, CL_MEM_READ_WRITE, size, NULL, &ierr);	
  if (ierr != CL_SUCCESS)
    DEBUG_MSG("Error allocating memory, OpenCL error: " << ierr);
	
  return mem;
}

/*
  write data specified by in_data to device memory, device memory space defined by cl_mem
*/
int OpenCLBase::ocl_writeData(cl_mem mem_ptr, const void * in_data, size_t size, 
			      size_t offset, int blocking) 
{

  cl_int ierr;

  ierr = clEnqueueWriteBuffer(m_command_queue, mem_ptr, blocking, offset, size, 
			      in_data, 0, NULL, NULL);
	
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Error writing data to device, OpenCL error: " << ierr);
    return ierr;
  }
	
  return OCL_SUCCESS;
}

/*
  copy src buffer into dst buffer
*/
int OpenCLBase::ocl_copyData(cl_mem src_ptr, cl_mem dst_ptr, size_t size) {

  int ierr;
  ierr = clEnqueueCopyBuffer(m_command_queue, src_ptr, dst_ptr, 0, 0, size, 0, NULL, NULL);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Error copying buffers, OpenCL error: " << ierr);
    return OCL_ERROR;
  }

  return OCL_SUCCESS;
}


/*
  create kernel specified by kernel_name from compiled program
*/
int OpenCLBase::ocl_createKernel(const char* kernel_name) {
  cl_int ierr;

  //release the old kernel
  if (m_kernel != NULL)
    clReleaseKernel(m_kernel);
  //create a new kernel
  m_kernel = clCreateKernel(m_program, kernel_name, &ierr);
  if (ierr != CL_SUCCESS) {
    DEBUG_MSG("Error creating kernel, OpenCL error: " << ierr);
    return ierr;
  }
  return OCL_SUCCESS;
}

/*
  set kernel argument, idx is the index of arument, size specifies data size, arg_value value of the argument
*/
int OpenCLBase::ocl_setKernelArg(int idx, size_t size, const void *arg_value) {
  cl_int ierr;
  ierr = clSetKernelArg(m_kernel, idx, size, arg_value);
	
  if (ierr != CL_SUCCESS)
    DEBUG_MSG("Error setting kernel arg, OpenCL error: " << ierr);
		
  return ierr;
}

/*
  executes set kernel, must provide dimensions ndim (1, 2 or 3) and total number of work items
  work_items should be an arry of size ndim
  optional: work_group_size - can specify how work items are divided in work groups, 
  if left NULL OpenCL implementation handles this part.
*/
int OpenCLBase::ocl_executeKernel(cl_uint ndim, const size_t *work_items, 
				  const size_t *work_group_size) 
{
  cl_int ierr;	

  ierr = clEnqueueNDRangeKernel(m_command_queue, m_kernel, ndim, NULL, 
				work_items, work_group_size, 
				0, NULL, NULL);
	
  if (ierr != CL_SUCCESS)
    DEBUG_MSG("Error executing kernel, OpenCL error: " << ierr 
	      << " work items: " << *work_items << ", " 
	      << " work group: " << *work_group_size);

  return ierr;
}

/*
  read data from device, mem_ptr points to data on device out_data points to memory in host
  blocking specifies wether the read operation is blocking (default CL_TRUE) or non blocking (CL_FALSE)
*/
int OpenCLBase::ocl_readData(cl_mem mem_ptr, void * out_data, size_t size, 
			     size_t offset, int blocking) 
{
  cl_int ierr;

  ierr = clEnqueueReadBuffer(m_command_queue, mem_ptr, blocking, offset, size, 
   			     out_data, 0, NULL, NULL);
	
  if (ierr != CL_SUCCESS)
    DEBUG_MSG("Error reading data from device, OpenCL error: " << ierr);
		
  return ierr;
}

/*
  free device memory specified by mem_ptr
*/
int OpenCLBase::ocl_freeMemory(cl_mem mem_ptr) {
  cl_int ierr;
  ierr = clReleaseMemObject(mem_ptr);
  if (ierr != CL_SUCCESS)
    DEBUG_MSG("Error freeing memory on device, OpenCL error: " << ierr);
	
  return ierr;
}

/*
  delete created OpenCL resources
*/
int OpenCLBase::ocl_cleanUp() {

  if (m_kernel != NULL) {
    clReleaseKernel(m_kernel);
    m_kernel = NULL;
  }
	
  if (m_program != NULL) {
    clReleaseProgram(m_program);
    m_program = NULL;
  }

  if (m_command_queue != NULL) {
    clReleaseCommandQueue(m_command_queue);
    m_command_queue = NULL;
  }
	
  if (m_context != NULL) {
    clReleaseContext(m_context);
    m_context = NULL;
  }
	
  return OCL_SUCCESS;
}

int OpenCLBase::ocl_deviceInfo(bool verbose) {

	
  if (m_device_id == NULL) {
    std::cout << "Device not set" << std::endl;
    return OCL_ERROR;	
  }
		

  char *info;
  cl_bool b_info;
  cl_ulong ul_info;
  cl_uint ui_info;
  size_t info_size;
  //size_t *wi_info;
  cl_device_type device_type;
	
  const int count = 12;
  const char *info_type[count] = {"char", "cl_device_type", "cl_bool",
				  "cl_bool", "cl_ulong", "cl_uint",
				  "cl_uint", "cl_ulong", "size_t",
				  "size_t[]", "cl_ulong", "char"};
  const char* info_name[count] = {"Name", "Device type","Device available", 
				  "Compiler available", "Global mem size (gb)", "Max clock freq (MHz)",
				  "Max compute units", "Max buffer size (bytes)", "Max work group size",
				  "Max work item sizes", "Local mem size (bytes)", "Extensions"};
  const cl_device_info info_value[count] = {CL_DEVICE_NAME, CL_DEVICE_TYPE, CL_DEVICE_AVAILABLE, 
					    CL_DEVICE_COMPILER_AVAILABLE, CL_DEVICE_GLOBAL_MEM_SIZE, CL_DEVICE_MAX_CLOCK_FREQUENCY,
					    CL_DEVICE_MAX_COMPUTE_UNITS, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, CL_DEVICE_MAX_WORK_GROUP_SIZE,
					    CL_DEVICE_MAX_WORK_ITEM_SIZES, CL_DEVICE_LOCAL_MEM_SIZE, CL_DEVICE_EXTENSIONS};
	
  int print_count;
  if (verbose)
    print_count = count;
  else
    print_count = 3;
	
	
  std::cout << "--------------------" << std::endl;
  std::cout << "OpenCL device information" << std::endl;
  std::cout << "--------------------" << std::endl;
	
  for (int k = 0; k < print_count; k++) {
    if (strcmp(info_type[k], "char") == 0) {
      clGetDeviceInfo(m_device_id, info_value[k], 0, NULL, &info_size);
      info = new char[info_size];
      clGetDeviceInfo(m_device_id, info_value[k], info_size, info, NULL);
      std::cout << info_name[k] << ": " << info << std::endl;
      delete[] info;
		
    } else if (strcmp(info_type[k], "cl_bool") == 0) {
      clGetDeviceInfo(m_device_id, info_value[k], sizeof(cl_bool), &b_info, NULL);
      std::cout << info_name[k] << ": " << b_info << std::endl;
		
    } else if (strcmp(info_type[k], "cl_ulong") == 0) {
      clGetDeviceInfo(m_device_id, info_value[k], sizeof(cl_ulong), &ul_info, NULL);
			
      if (info_value[k] == CL_DEVICE_GLOBAL_MEM_SIZE) {
	double gb = (double)ul_info*1e-9;
	std::cout << info_name[k] << ": " << gb << std::endl;
      } else if (info_value[k] == CL_DEVICE_LOCAL_MEM_SIZE) {
	std::cout << info_name[k] << ": " << ul_info << std::endl;
	std::cout << "512^2 bytes: " << sizeof(cl_double2)*512*5 << std::endl;
      } else {
	std::cout << info_name[k] << ": " << ul_info << std::endl;
      }
    } else if (strcmp(info_type[k], "cl_uint") == 0) {
      clGetDeviceInfo(m_device_id, info_value[k], sizeof(cl_uint), &ui_info, NULL);
      std::cout << info_name[k] << ": " << ui_info << std::endl;
		
    } else if (strcmp(info_type[k], "size_t") == 0) {
      clGetDeviceInfo(m_device_id, info_value[k], sizeof(size_t), &info_size, NULL);
      std::cout << info_name[k] << ": " << info_size << std::endl;
		
    } else if (strcmp(info_type[k], "size_t[]") == 0 ){
      clGetDeviceInfo(m_device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &ui_info, NULL);
      size_t wi_info[3];// = new size_t[ui_info];
      clGetDeviceInfo(m_device_id, info_value[k], 3 * sizeof(size_t), &wi_info, NULL);
      std::cout << info_name[k] << ": ";
      for (unsigned int m = 0; m < ui_info; m++)
	std::cout << wi_info[m] << " ";
      std::cout << std::endl;
		
    } else if (strcmp(info_type[k], "cl_device_type") == 0) {
      clGetDeviceInfo(m_device_id, info_value[k], sizeof(cl_device_type), &device_type, NULL);
      switch (device_type) {
      case CL_DEVICE_TYPE_CPU:
	std::cout << info_name[k] << ": CPU" << std::endl;
	break;
      case CL_DEVICE_TYPE_GPU:
	std::cout << info_name[k] << ": GPU" << std::endl;
	break;
      case CL_DEVICE_TYPE_ACCELERATOR:
	std::cout << info_name[k] << ": Accelerator" << std::endl;
	break;
      case CL_DEVICE_TYPE_DEFAULT:
	std::cout << info_name[k] << ": Default" << std::endl;
	break;
      default:
	std::cout << info_name[k] << ": Unknown" << std::endl;
	break;
      }
    }
  }
  return OCL_SUCCESS;
}

int OpenCLBase::ocl_checkKernel(const char* kernel_name, int work_group_size,
				bool double_precision, int &threadsPerBlock)
{

  //build kernel
  int ierr = ocl_createKernel(kernel_name);
  if (ierr != DKS_SUCCESS)
    return ierr;

  /* get device properties */
  //maximum number of work-items in a work group supported by device
  size_t max_group_size;
  clGetDeviceInfo(m_device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_group_size, 0);
  //maxumum local memory size per work group
  cl_ulong local_mem_size;
  clGetDeviceInfo(m_device_id, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &local_mem_size, 0);
  //get the supported extensions
  size_t ext_size;
  clGetDeviceInfo(m_device_id, CL_DEVICE_EXTENSIONS, 0, 0, &ext_size);
  char *ext = new char[ext_size];
  clGetDeviceInfo(m_device_id, CL_DEVICE_EXTENSIONS, ext_size, ext, 0);

  /* get kernel properties */
  //get max work group size that can be used for this kernel
  size_t kernel_group_size;
  clGetKernelWorkGroupInfo(m_kernel, m_device_id, CL_KERNEL_WORK_GROUP_SIZE, 
			   sizeof(size_t), &kernel_group_size, 0);
  threadsPerBlock = kernel_group_size;

  //get max local memory size that can be used for this kernel
  cl_ulong kernel_local_mem;
  clGetKernelWorkGroupInfo(m_kernel, m_device_id, CL_KERNEL_LOCAL_MEM_SIZE,
			   sizeof(cl_ulong), &kernel_local_mem, 0);


  std::cout << std::endl << "Begin " << kernel_name << " check..." << std::endl;


  std::cout << "Work group size: max for device " << max_group_size << " > "
	    << "max for kernel " << kernel_group_size << " > "
	    << "required " << work_group_size << std::endl;
  

  std::cout << "Local memory: device limit " << local_mem_size << std::endl;
  std::cout << "Local memory: kernel needs " << kernel_local_mem << std::endl;
  

  std::cout << std::endl << "Available extensions: " << ext << std::endl;

  std::cout << "End " << kernel_name << " check..." << std::endl << std::endl;   

  return DKS_SUCCESS;
}

void OpenCLBase::ocl_clearEvents() {

  m_events.clear();
	
  //delete[] m_events;
  //m_num_events = 0;
  //m_events = new cl_event[500];
	
}



void OpenCLBase::ocl_eventInfo() {
	
  std::cout << "Number of events launched: " << m_events.size() << std::endl;

  if (m_events.size() > 0) {
	
    cl_ulong twrite = 0;
    cl_ulong texec = 0;
    cl_ulong tread = 0;
    int cw = 0;
    int ce = 0;
    int cr = 0;
	
    for (unsigned i = 0; i < m_events.size(); i++) {
		
      cl_ulong tqueue, tsubmit, tstart, tend;
			
      clGetEventProfilingInfo(m_events[i], CL_PROFILING_COMMAND_QUEUED, 
			      sizeof(cl_ulong), &tqueue, NULL);

      clGetEventProfilingInfo(m_events[i], CL_PROFILING_COMMAND_SUBMIT, 
			      sizeof(cl_ulong), &tsubmit, NULL);
      
      clGetEventProfilingInfo(m_events[i], CL_PROFILING_COMMAND_START, 
			      sizeof(cl_ulong), &tstart, NULL);
      
      clGetEventProfilingInfo(m_events[i], CL_PROFILING_COMMAND_END, 
			      sizeof(cl_ulong), &tend, NULL);
			
      cl_command_type type;
      clGetEventInfo(m_events[i], CL_EVENT_COMMAND_TYPE, sizeof(cl_int), &type, NULL);
			
      if (type == CL_COMMAND_WRITE_BUFFER) {
	twrite += (tend - tstart);
	cw++;
      }
			
      if (type == CL_COMMAND_READ_BUFFER) {
	tread += (tend - tstart);
	cr++;
      }
			
      if (type == CL_COMMAND_NDRANGE_KERNEL) {
	texec += (tend - tstart); 
	ce++;	
      }
    }
		
    std::cout << "OpenCL write: " << (twrite * 1e-9) << " in: " << cw << std::endl;
    std::cout << "OpenCL exec: " << (texec * 1e-9) << " in: " << ce << std::endl;
    std::cout << "OpenCL read: " << (tread * 1e-9) << " in: " << cr << std::endl;
	
  }

  /*
    cl_ulong tqueue, tsubmit, tstart, tend, tref;
	
    int *list_bad_events = new int[m_num_events];
    int num_bad_events = 0;

    if (m_num_events > 0) {

    double *list_ended = new double[m_num_events];

    clGetEventProfilingInfo(m_events[0], CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &tref, NULL);

    std::cout << std::endl;
    std::cout << setw(10) << left << "Event\t| ";
    std::cout << setw(10) << left << "queued\t| ";
    std::cout << setw(10) << left << "submited\t| ";
    std::cout << setw(10) << left << "started\t| ";
    std::cout << setw(10) << left << "ended \t| ";

    std::cout << setw(10) << left << "in queue" << std::endl;
    std::cout << setw(10) << "-----------------------------------------------------------------------------------" << std::endl;
    for (unsigned int i = 0; i < m_num_events; i++) {
	
    clGetEventProfilingInfo(m_events[i], CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &tqueue, NULL);
    clGetEventProfilingInfo(m_events[i], CL_PROFILING_COMMAND_SUBMIT, sizeof(cl_ulong), &tsubmit, NULL);
    clGetEventProfilingInfo(m_events[i], CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &tstart, NULL);
    clGetEventProfilingInfo(m_events[i], CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &tend, NULL);
		
    cl_command_type type;
    clGetEventInfo(m_events[i], CL_EVENT_COMMAND_TYPE, sizeof(cl_int), &type, NULL);

    tqueue = (tqueue >= tref) ? tqueue - tref : tqueue;
    tsubmit = (tsubmit > tref) ? tsubmit - tref : tsubmit;
    tstart = (tstart > tref) ? tstart - tref : tstart;
    tend = (tend > tref) ? tend - tref : tend;
		
    if (type == CL_COMMAND_READ_BUFFER || type == CL_COMMAND_WRITE_BUFFER)
    std::cout << left << i << "*\t| ";
    else
    std::cout << left << i << "\t| ";
    std::cout << setw(7) << left << tqueue << "\t| ";
    std::cout << setw(7) << left << tsubmit << "\t| ";
    std::cout << setw(7) << left << tstart << "\t| ";
    std::cout << setw(7) << left << tend << "\t| ";

    int count = 0;
    if (i > 0) {
    for (unsigned int j = 0; j < i; j++) {
    if (list_ended[j] > tqueue)
    count++;
    }
    }
    list_ended[i] = tend;
			
    std::cout << setw(7) << left << count << std::endl;
			
    //this seems to be a problem on MIC sometimes
    if (tstart == 0) {
      list_bad_events[num_bad_events] = i;
      num_bad_events++;
    }
    }
    std::cout << setw(10) << "-----------------------------------------------------------------------------------" << std::endl << std::endl;
		
    //print info about failed events
    for (int i = 0; i < num_bad_events; i++) {
    cl_int event_status;
    int id = list_bad_events[i];
    clGetEventInfo(m_events[id], CL_EVENT_COMMAND_EXECUTION_STATUS, sizeof(cl_int), &event_status, NULL);
    std::cout << "Event " << id << " : ";
    switch(event_status) {
    case CL_QUEUED:
    std::cout << "queued" << std::endl;
    break;
    case CL_SUBMITTED:
    std::cout << "submited" << std::endl;
    break;
    case CL_RUNNING:
    std::cout << "running" << std::endl;
    break;
    case CL_COMPLETE:
    std::cout << "complete" << std::endl;
    break;
    default:
    std::cout << "error" << std::endl;
    break;
    }
    }
    }
  */
	
}










