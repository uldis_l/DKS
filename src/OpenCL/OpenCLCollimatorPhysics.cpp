#include "OpenCLCollimatorPhysics.h"

#define M_P 0.93827231e+00
#define C 299792458.0
#define PI 3.14159265358979323846
#define AVO 6.022e23
#define R_E 2.81794092e-15
#define eM_E 0.51099906e-03
#define Z_P 1
#define K 4.0*PI*AVO*R_E*R_E*eM_E*1e7

#define POSITION 0 
#define ZSIZE 1
#define RHO_M 2
#define Z_M 3
#define A_M 4
#define A2_C 5
#define A3_C 6
#define A4_C 7
#define A5_C 8
#define X0_M 9
#define I_M 10
#define DT_M 11

#define BLOCK_SIZE 128
#define NUMPAR 12

/*
TODO: 
1. test OpenCL kernel 
  - is it launched for all particles 
  - does the random number generatror function properly 
  - is particle structure updated correctly in memory
2. boost.compute sort for user defined structure crashes
*/
int OpenCLCollimatorPhysics::CollimatorPhysics(void *mem_ptr, void *par_ptr, 
					       int numparticles, bool enableRutherforScattering) 
{
  /*
  //set number of total threads, and number threads per block
  size_t threads = 1;
  size_t blocks = numparticles;

  //cast void ptrs to cl_mem ptrs
  cl_mem data = (cl_mem)mem_ptr;
  cl_mem params = (cl_mem)par_ptr;

  int numparams = 19;

  //set kernel to execute and kernel arguments
  ocl_createKernel("kernelCollimatorPhysics");
  ocl_setKernelArg(0, sizeof(cl_mem), &data);
  ocl_setKernelArg(1, sizeof(cl_mem), &params);
  ocl_setKernelArg(2, sizeof(cl_mem), &defaultRndState);
  ocl_setKernelArg(3, sizeof(int), &numparticles);
  ocl_setKernelArg(4, sizeof(double)*numparams, NULL);

  std::cout << "blocks: " << blocks << ", threads: " << threads << std::endl;

  //execute kernel on device
  ocl_executeKernel(1, &blocks, &threads);

  //create functions for comparing two particles and counting particles with labels < 0
  
  BOOST_COMPUTE_FUNCTION(bool, sort_by_label, (PART_OPENCL a, PART_OPENCL b),
			 {
			   return a.label < b.label;
			 });
  
  
  
  BOOST_COMPUTE_FUNCTION(bool, count_by_label, (PART_OPENCL a),
			 {
			   return a.label < 0;
			 });
  
  
  //wrap cl_mem memory object in Boost.Compute buffer
  std::cout << "wrap buffer" << std::endl;
  boost::compute::buffer buf(data);

  //count particles with labels < 0
  std::cout << "wrap command queue" << std::endl;
  boost::compute::command_queue queue(ocl_getQueue());
  
  std::cout << "count if" << std::endl;


  numaddback = boost::compute::count_if(boost::compute::make_buffer_iterator<PART_OPENCL>(buf,0), 
					boost::compute::make_buffer_iterator<PART_OPENCL>(buf,numparticles), 
					count_by_label, queue);

  //sort particles with dead and leaving particles at the end using boos::compute
  numaddback = 0;
  if (numaddback > 0) {
    std::cout << "sort" << std::endl;
    boost::compute::sort(boost::compute::make_buffer_iterator<PART_OPENCL>(buf,0),
			 boost::compute::make_buffer_iterator<PART_OPENCL>(buf, numparticles),
			 sort_by_label, queue);
  }
  

  return DKS_SUCCESS;
*/
  std::cout << "OpenCL implementation disabled" << std::endl;
  return DKS_ERROR;
}
