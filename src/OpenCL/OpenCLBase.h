#ifndef H_OPENCL_BASE
#define H_OPENCL_BASE

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <string.h>
#include <stdio.h>


#ifdef __APPLE__
#include <OpenCL/opencl.h>
#include <OpenCL/cl_ext.h>
#else
#include <CL/cl.h>
#include <CL/cl_ext.h>
#endif

#include "../DKSDefinitions.h"

/** struct for random number state. */
typedef struct {
  double s10;
  double s11;
  double s12;
  double s20;
  double s21;
  double s22;
  double z;
  bool gen;
} RNDState;

/**
 * OpenCL base class to handle device setup and basic communication wiht the device.
 * Handles initialization of OpenCL device, memory manegement, data transfer and kernel launch.
 * The OpenCL kernels are located in seperate files in OpenCLKernels folder, the OpenCLBase
 * class contains methods to read the kernel files, compile the kernel codes and launch kernels
 * from the compiled codes. Which kernel file needs to be loaded for the specif functin is 
 * handled by the base class that is launching the kernel.
 */
class OpenCLBase {

private:

  //variables containig OpenCL device and platform ids
  static cl_platform_id m_platform_id;
  static cl_device_id m_device_id;

  //variables containit compiled OpenCL program and kernel
  cl_context_properties m_context_properties[3];
  cl_program m_program;
  cl_kernel m_kernel;
	
  //variables for tracking OpenCL events
  static cl_event m_last_event;
  cl_int m_num_events;
  std::vector<cl_event> m_events;
	
  //currently load kernel file
  char * m_kernel_file;

  //type of device used by OpenCL
  cl_device_type m_device_type;
	
  /**
   * Get all available OpenCL platforms.
   * Get all avaialble platforms and save in m_platform_ids, save number of platforms
   *  Return: success or error code
   */
  int ocl_getPlatforms();
	
	
  /**
   * Get first available OpenCL device of specified type.
   * Get first avaialble devices and save device id and platform id for this device, 
   * device name: (-gpu, -mic, -cpu)
   *  ReturnL success or error code
   */
  int ocl_getDevice(const char* device_name);
	
  /**
   * Get cl_device_type from the specified device name.
   * get device type from device name (-gpu, -cpu, -mic)
   *  Return: success or error code
   */
  int ocl_getDeviceType(const char* device_name, cl_device_type &device_type);
	
  /**
   * Create OpenCL context with specified device.
   *  Return: success or error code
   */
  int ocl_createContext();
	
  /**
   * Build program from specified kernel file.
   * Return: success or error code.
  */
  int ocl_buildProgram(const char* kernel_file);

  /** 
   * Compile program from kernel source string.
   * Takes a string read from OpenCL kernel file saved in kernel_source and compiles the 
   * OpenCL program, that can be then executed on the device.
   * opts is a string specifiend additional compiler flags.
   */
  int ocl_compileProgram(const char* kernel_source, const char* opts = NULL);

protected:

  //memory for random number states
  int defaultRndSet;
  cl_mem defaultRndState;
	
	
public:

  //OpenCL context and commad queue
  static cl_context m_context;
  static cl_command_queue m_command_queue; 
    
  /**
   * constructor
   */
  OpenCLBase();
    
  /**
   * destructor
   */
  ~OpenCLBase();
    
  /**
   * Allocate memory for size random number states and init the rnd states.
   * Uses AMD clRng library for random numbers. 
   * This library is only compatible with AMD devices.
   */
  int ocl_createRndStates(int size);

  /** 
   * Create an array of random numbers on the device.
   * Filles hte mem_ptr with random numbers.
   */
  int ocl_createRandomNumbers(void *mem_ptr, int size);

  /**
   * Destroy rnd states and free device memory.
   * Return: success or error code
   */
  int ocl_deleteRndStates();


  /**
   * Prints info about all the available platforms and devices.
   * Can be used for information purposes to see what devices are available on the system.
   * ReturnL success or error code.
  */
  int ocl_getAllDevices();

  /** 
   * Get the OpenCL device count for the set type of device.
   * Device count is set in ndev parameter, returns success or error code.
   */
  int ocl_getDeviceCount(int &ndev);

  /** 
   * Get the name of the device currently us use.
   */
  int ocl_getDeviceName(std::string &device_name);

  /** 
   * Set the device to use for OpenCL kernels.
   * Device id to use is passed as integer.
   */
  int ocl_setDevice(int device);

  /** 
   * Get a list of all the unique devices of the same type that can run OpenCL kernels.
   * Used when GPUs of different types might be pressent on the system.
   */
  int ocl_getUniqueDevices(std::vector<int> &devices);
    
  /**
   * Initialize OpenCL connection with a device of specified type.
   * Find if specified device is avaialble, creates a contex and command queue.
   * Returns success or error code.
   */
  int ocl_setUp(const char* device_name);
	
  /**
   * Given a OpenCL kernel file name loads the content and compile the OpenCL code.
   * Load and compile opencl kernel file if it has changed.
   * Return: success or error code
  */
  int ocl_loadKernel(const char* kernel_file);


  /** 
   * Build program from kernel source.
   * Builds a program from source code provided in kernel_source.
   * If compilation fails will return DKS_ERROR
   */
  int ocl_loadKernelFromSource(const char* kernel_source, const char* opts = NULL);
	
  /**
   * Allocate memory on the device.
   * Return: return pointer to memory
  */
  cl_mem ocl_allocateMemory(size_t size, int &ierr);

  /**
   * Allocate memory of specific type on device.
   * The availabel types are cl_mem_flags type listed in OpenCL documentation:
   * CL_MEM_READ_WRITE, CL_MEM_WRITE_ONLY, CL_MEM_USE_HOST_PTR, 
   * CL_MEM_ALLOC_HOST_PTR and CL_MEM_COPY_HOST_PTR.
   * Return: return pointer to memory
  */
  cl_mem ocl_allocateMemory(size_t size, int type, int &ierr);
	
  /** 
   * Zero OpenCL memory buffer.
   * Set all the elemetns in the device array to zero.
   */
  template <typename T>
  int ocl_fillMemory(cl_mem mem_ptr, size_t size, T value, int offset = 0) {
  
    cl_int ierr;
    ierr = clEnqueueFillBuffer(m_command_queue, mem_ptr, &value, sizeof(T), offset, 
			       sizeof(T)*size, 0, nullptr, nullptr);
    if (ierr != CL_SUCCESS)
      return DKS_ERROR;
    return DKS_SUCCESS;
  }

  /**
   * Write data to device memory (needs ptr to mem object)
   * Return: success or error code
   */
  int ocl_writeData(cl_mem mem_ptr, const void * in_data, size_t size, size_t offset = 0, int blocking = CL_TRUE);
	
  /** 
   * Copy data from one buffer on the device to another
   * Return: success or error code
   */
  int ocl_copyData(cl_mem src_ptr, cl_mem dst_ptr, size_t size);
	
  /** 
   * Create kernel from compiled OpenCL program.
   * Return: success or error code
   */
  int ocl_createKernel(const char* kernel_name);
	
  /**
   * Set argiments for the kernel that will be launched.
   * Return: success or error code
   */
  int ocl_setKernelArg(int idx, size_t size, const void *arg_value);
	
  /**
   * Execute selected kernel.
   * Before kenrel can be executed buildProgram must be executed, create kernel must be executed
   * and kenre specifeid in execute kerenel must be in compiled source, and the necessary
   * kernel arguments must be set.
   * Return: success or error code
  */
  int ocl_executeKernel(cl_uint, const size_t *work_items, const size_t *work_grou_size = NULL);
	
  /**
   * Read data from device (needs pointer to mem object).
   * Return: success or error code
   */
  int ocl_readData(cl_mem mem_ptr, void * out_data, size_t size, size_t offset = 0, int blocking = CL_TRUE);
	
  /**
   * Free device memory (needs ptr to mem object).
   *  Return: success or error code
   */
  int ocl_freeMemory(cl_mem mem_ptr);
	
  /**
   * Free opencl resources.
   * Deletes the kernel, compiled program, command queue and colese the connection
   * to device by releasing the context.
   * Return: success or error code
   */
  int ocl_cleanUp();
	
  /**
   * Print info of currently selected device.
   * Mostly for debugging purposes, but in verbose mode can be used to see device properties.
   * Return: success or error code
   */
  int ocl_deviceInfo(bool verbose = true);

  /* 
   * Check OpenCL kernel.
   * Query device and check if it can run the kernel with required parameters.
   * Also check the available OpenCL extensions - usefull for checking the supported device
   * features, like double precission.
   */
  int ocl_checkKernel(const char* kernel_name, int work_group_size,
		      bool double_precision, int &threadsPerBlock);
	
  /**
   * Clear the event list.
   * Events can be used for timing and synchronization purposes.
   */
  void ocl_clearEvents();

  /**
   * print information about kernel timings from event list.
   * for debuging purposes
   */
  void ocl_eventInfo();

  /**
   * Return current command queue.
   */
  cl_command_queue ocl_getQueue() { return m_command_queue; }
};

#endif








