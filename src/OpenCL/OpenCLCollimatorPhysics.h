#ifndef H_OPENCL_DEGRADER
#define H_OPENCL_DEGRADER

#include <iostream>
#include <math.h>

#include "../Algorithms/CollimatorPhysics.h"
#include "OpenCLBase.h"

/*
#include "boost/compute/types/struct.hpp"
#include "boost/compute/type_traits/type_name.hpp"
#include "boost/compute/algorithm/count_if.hpp"
#include "boost/compute/algorithm/sort.hpp"
#include "boost/compute/container/vector.hpp"
#include "boost/compute/iterator/buffer_iterator.hpp"
#include "boost/compute/core.hpp"
*/

/** Double3 structure for use in OpenCL code. */
typedef struct {
  double x;
  double y;
  double z;
} Double3;

/**
 * Structure for stroing particles in OpenCL code.
 */
typedef struct {
  int label;
  unsigned localID;

  Double3 Rincol;
  Double3 Pincol;
} PART_OPENCL;

//adapt struct PART for use in Boost.Compute
//BOOST_COMPUTE_ADAPT_STRUCT(Double3, Double3, (x, y, z));
//BOOST_COMPUTE_ADAPT_STRUCT(PART_OPENCL, PART_OPENCL, (label, localID, Rincol, Pincol));

/**
 * OpenCLCollimatorPhysics class based on DKSCollimatorPhysics interface.
 * Implementes CollimatorPhysics for OPAL using OpenCL for execution on AMD GPUs.
 */
class OpenCLCollimatorPhysics : public DKSCollimatorPhysics {

private:
  OpenCLBase *m_oclbase;

public:

  /** 
   * Constructor with OpenCLBase as argument.
   * Create a new instace of the OpenCLCollimatorPhysics using existing OpenCLBase object.
   */
  OpenCLCollimatorPhysics(OpenCLBase *base) { 
    m_oclbase = base;
  }

  /** 
   * Destructor.
   */
  ~OpenCLCollimatorPhysics() { 
  }

  int CollimatorPhysics(void *mem_ptr, void *par_ptr, int numparticles, 
			bool enableRutherforScattering = true);

  int CollimatorPhysicsSoA(void *label_ptr, void *localID_ptr, 
			   void *rx_ptr, void *ry_ptr, void *rz_ptr, 
			   void *px_ptr, void *py_ptr, void *pz_ptr,
			   void *par_ptr, int numparticles) { return DKS_ERROR; }
  
  int CollimatorPhysicsSort(void *mem_ptr, int numparticles, int &numaddback) { return DKS_ERROR; }

  int CollimatorPhysicsSortSoA(void *label_ptr, void *localID_ptr, 
			       void *rx_ptr, void *ry_ptr, void *rz_ptr, 
			       void *px_ptr, void *py_ptr, void *pz_ptr,
			       void *par_ptr, int numparticles, int &numaddback) { return DKS_ERROR; }

  int ParallelTTrackerPush(void *r_ptr, void *p_ptr, int npart, void *dt_ptr, 
			   double dt, double c, bool usedt = false, int streamId = -1) 
    { 
      return DKS_ERROR; 
    }

  int ParallelTTrackerPushTransform(void *x_ptr, void *p_ptr, void *lastSec_ptr, 
				    void *orient_ptr, int npart, int nsec, void *dt_ptr, 
				    double dt, double c, bool usedt = false, 
				    int streamId = -1)
    { 
      return DKS_ERROR; 
    }

};

#endif
