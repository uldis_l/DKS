#include "OpenCLChiSquareRuntime.h"

OpenCLChiSquareRuntime::OpenCLChiSquareRuntime(OpenCLBase *base) {

  blockSize_m = BLOCK_SIZE;
  numBlocks_m = -1;

  m_oclbase = base;

  N0_m = 1.0;
  tau_m = 1.0;
  bkg_m = 1.0;
  alpha_m = 1.0;
  beta_m = 1.0;

  ptx_m = NULL;

  initDone_m = false;

}

//free temporary resources
OpenCLChiSquareRuntime::~OpenCLChiSquareRuntime() {
  delete[] ptx_m;
  freeChiSquare();
}

//build program string
std::string OpenCLChiSquareRuntime::buildProgram(std::string function) {

  long fsize;
  char *kernel_source;

  //get kernel source
  char * kernel_file = new char[500];
  kernel_file[0] = '\0';
  strcat(kernel_file, OPENCL_KERNELS);
  strcat(kernel_file, "OpenCL/OpenCLKernels/OpenCLChiSquareRuntime.cl");

  //read kernels from file
  FILE *fp = fopen(kernel_file, "rb");
  if (!fp)
    DEBUG_MSG("Can't open kernel file" << kernel_file);

  //get file size and allocate memory
  fseek(fp, 0, SEEK_END);
  fsize = ftell(fp);
  kernel_source = new char[fsize+1];

  //read file and content in kernel source
  rewind(fp);
  fread(kernel_source, 1, sizeof(char)*fsize, fp);
  kernel_source[fsize] = '\0';
  fclose(fp);

  std::string kernel_string (kernel_source);
  return kernel_string + openclFunctHeader + "return " + function + ";" + openclFunctFooter;

}

int OpenCLChiSquareRuntime::compileProgram(std::string function, bool mlh) {

  //build program string
  std::string openclProg = buildProgram(function);

  //compile flags
  std::string opts("");
  if (mlh)
    opts = "-DMLH";

  //compile opencl program from source string
  int ierr = m_oclbase->ocl_loadKernelFromSource(openclProg.c_str(), opts.c_str());

  return ierr;
}

double OpenCLChiSquareRuntime::calculateSum(cl_mem data, int length) {

  int ierr;
  //calc number of threads per workgroup and nr of work groups
  size_t work_size_sum = (size_t)blockSize_m;

  /*
  size_t work_items = (size_t)length;
  if (length % work_size_sum > 0)
    work_items = (length / work_size_sum + 1) * work_size_sum;
  int work_groups = length / work_size_sum + 1;
  */

  size_t work_items = 80 * work_size_sum;
  int work_groups = 80;

  //create tmp array for partial sums
  cl_mem tmp_ptr;

  double *partial_sums = new double[work_groups];
  tmp_ptr = m_oclbase->ocl_allocateMemory(work_groups * sizeof(double), ierr);

  //execute sum kernel
  m_oclbase->ocl_createKernel("parallelReductionTwoPhase");
  m_oclbase->ocl_setKernelArg(0, sizeof(cl_mem), &data);
  m_oclbase->ocl_setKernelArg(1, sizeof(cl_mem), &tmp_ptr);
  m_oclbase->ocl_setKernelArg(2, work_size_sum*sizeof(double), NULL);
  m_oclbase->ocl_setKernelArg(3, sizeof(int), &length);
  m_oclbase->ocl_executeKernel(1, &work_items, &work_size_sum);
  
  //read partial sums and free temp memory
  m_oclbase->ocl_readData(tmp_ptr, partial_sums, sizeof(double)*work_groups);
  m_oclbase->ocl_freeMemory(tmp_ptr);

  //sumup partial sums on the host
  double result = 0;
  for (int i = 0; i < work_groups; i++)
    result += partial_sums[i];

  delete[] partial_sums;

  return result;

}

int OpenCLChiSquareRuntime::launchChiSquare(int fitType,
					    void *mem_data, void *mem_err, int length,
					    int numpar, int numfunc, int nummap,
					    double timeStart, double timeStep, double &result)
{

  int ierr;

  //convert memory to cl_mem
  cl_mem cl_mem_data = (cl_mem)mem_data;
  cl_mem cl_mem_err = (cl_mem)mem_err;

  cl_mem cl_param = (cl_mem)mem_param_m;
  cl_mem cl_chisq = (cl_mem)mem_chisq_m;
  cl_mem cl_map = (cl_mem)mem_map_m;
  cl_mem cl_func = (cl_mem)mem_func_m;

  //set work item size
  size_t work_items;
  size_t work_size = (size_t)blockSize_m;

  if (numBlocks_m < 0)
    work_items = (size_t)length;
  else
    work_items = (size_t)numBlocks_m * (size_t)blockSize_m;

  if (work_items % work_size > 0)
    work_items = (work_items / work_size + 1) * work_size;

  if (fitType == FITTYPE_SINGLE_HISTO) {
    //create kernel
    ierr = m_oclbase->ocl_createKernel("kernelChiSquareSingleHisto");

    if (ierr != DKS_SUCCESS)
      return ierr;

    //set kernel args
    size_t num=1;
    m_oclbase->ocl_setKernelArg(0, sizeof(cl_mem), &cl_mem_data);
    m_oclbase->ocl_setKernelArg(1, sizeof(cl_mem), &cl_mem_err);
    m_oclbase->ocl_setKernelArg(2, sizeof(cl_mem), &cl_param);
    m_oclbase->ocl_setKernelArg(3, sizeof(cl_mem), &cl_chisq);
    m_oclbase->ocl_setKernelArg(4, sizeof(cl_mem), &cl_map);
    m_oclbase->ocl_setKernelArg(5, sizeof(cl_mem), &cl_func);
    m_oclbase->ocl_setKernelArg(6, sizeof(int), &length);
    m_oclbase->ocl_setKernelArg(7, sizeof(int), &numpar);
    m_oclbase->ocl_setKernelArg(8, sizeof(int), &numfunc);
    m_oclbase->ocl_setKernelArg(9, sizeof(int), &nummap);
    m_oclbase->ocl_setKernelArg(10, sizeof(double), &timeStart);
    m_oclbase->ocl_setKernelArg(11, sizeof(double), &timeStep);
    m_oclbase->ocl_setKernelArg(12, sizeof(double), &tau_m);
    m_oclbase->ocl_setKernelArg(13, sizeof(double), &N0_m);
    m_oclbase->ocl_setKernelArg(14, sizeof(double), &bkg_m);
    num = numpar; if (num == 0) num = 1;
    m_oclbase->ocl_setKernelArg(15, sizeof(double)*num, NULL);
    num = numfunc; if (num == 0) num = 1;
    m_oclbase->ocl_setKernelArg(16, sizeof(double)*num, NULL);
    num = nummap; if (num == 0) num = 1;
    m_oclbase->ocl_setKernelArg(17, sizeof(int)*num, NULL);

    if (ierr != DKS_SUCCESS)
      return ierr;
  } else if (fitType == FITTYPE_ASYMMETRY) {
    //create kernel
    ierr = m_oclbase->ocl_createKernel("kernelChiSquareAsymmetry");
    if (ierr != DKS_SUCCESS)
      return ierr;

    //set kernel args
    size_t num=1;
    m_oclbase->ocl_setKernelArg(0, sizeof(cl_mem), &cl_mem_data);
    m_oclbase->ocl_setKernelArg(1, sizeof(cl_mem), &cl_mem_err);
    m_oclbase->ocl_setKernelArg(2, sizeof(cl_mem), &cl_param);
    m_oclbase->ocl_setKernelArg(3, sizeof(cl_mem), &cl_chisq);
    m_oclbase->ocl_setKernelArg(4, sizeof(cl_mem), &cl_map);
    m_oclbase->ocl_setKernelArg(5, sizeof(cl_mem), &cl_func);
    m_oclbase->ocl_setKernelArg(6, sizeof(int), &length);
    m_oclbase->ocl_setKernelArg(7, sizeof(int), &numpar);
    m_oclbase->ocl_setKernelArg(8, sizeof(int), &numfunc);
    m_oclbase->ocl_setKernelArg(9, sizeof(int), &nummap);
    m_oclbase->ocl_setKernelArg(10, sizeof(double), &timeStart);
    m_oclbase->ocl_setKernelArg(11, sizeof(double), &timeStep);
    m_oclbase->ocl_setKernelArg(12, sizeof(double), &alpha_m);
    m_oclbase->ocl_setKernelArg(13, sizeof(double), &beta_m);
    num = numpar; if (num == 0) num = 1;
    m_oclbase->ocl_setKernelArg(14, sizeof(double)*num, NULL);
    num = numfunc; if (num == 0) num = 1;
    m_oclbase->ocl_setKernelArg(15, sizeof(double)*num, NULL);
    num = nummap; if (num == 0) num = 1;
    m_oclbase->ocl_setKernelArg(16, sizeof(int)*num, NULL);

    if (ierr != DKS_SUCCESS)
      return ierr;
  } else if (fitType == FITTYPE_MU_MINUS) {
    // not yet implemented
  } else {
    return DKS_ERROR;
  }

  //execute kernel
  ierr = m_oclbase->ocl_executeKernel(1, &work_items, &work_size);

  if (ierr != DKS_SUCCESS)
    return ierr;

  //execute sum kernel
  result = calculateSum((cl_mem)mem_chisq_m, length);

  return ierr;

}

int OpenCLChiSquareRuntime::writeParams(const double *params, int numparams) {
  //write params to gpu
  int ierr = m_oclbase->ocl_writeData( (cl_mem)mem_param_m, params, sizeof(double)*numparams);
  return ierr;
}


int OpenCLChiSquareRuntime::writeFunc(const double *func, int numfunc) {
  if (numfunc == 0)
    return DKS_SUCCESS;

  //write function values to the GPU
  int ierr = m_oclbase->ocl_writeData( (cl_mem)mem_func_m, func, sizeof(double)*numfunc);
  return ierr;
}

int OpenCLChiSquareRuntime::writeMap(const int *map, int nummap) {
  if (nummap == 0)
    return DKS_SUCCESS;

  //wrtie map values to the GPU
  int ierr = m_oclbase->ocl_writeData( (cl_mem)mem_map_m, map, sizeof(int)*nummap);
  return ierr;
}

int OpenCLChiSquareRuntime::initChiSquare(int size_data, int size_param,
					  int size_func, int size_map)
{

  int ierr = DKS_ERROR;
  if (initDone_m) {
    DEBUG_MSG("Reinitializing ChiSquare");
    freeChiSquare();
  }

  //allocate temporary memory, memory is allocated for the data set, parametrs, functions and maps
  mem_chisq_m = m_oclbase->ocl_allocateMemory(size_data*sizeof(double), ierr);
  mem_param_m = m_oclbase->ocl_allocateMemory(size_param*sizeof(double), ierr);
  if (size_func == 0)
    size_func = 1;
  mem_func_m = m_oclbase->ocl_allocateMemory(size_func*sizeof(double), ierr);
  if (size_map == 0)
    size_map = 1;
  mem_map_m = m_oclbase->ocl_allocateMemory(size_map*sizeof(int), ierr);
  initDone_m = true;

  return ierr;

}

int OpenCLChiSquareRuntime::freeChiSquare() {

  int ierr = DKS_ERROR;
  if (initDone_m) {

    //free GPU memory
    ierr = m_oclbase->ocl_freeMemory((cl_mem)mem_chisq_m);
    ierr = m_oclbase->ocl_freeMemory((cl_mem)mem_param_m);
    ierr = m_oclbase->ocl_freeMemory((cl_mem)mem_func_m);
    ierr = m_oclbase->ocl_freeMemory((cl_mem)mem_map_m);

    initDone_m = false;
  }

  return ierr;

}

int OpenCLChiSquareRuntime::checkChiSquareKernels(int fitType, int &threadsPerBlock) {

  int ierr;
  char kernel[64];

  switch (fitType) {
  case FITTYPE_SINGLE_HISTO:
    strncpy(kernel, "kernelChiSquareSingleHisto", sizeof(kernel));
    break;
  case FITTYPE_ASYMMETRY:
    strncpy(kernel, "kernelChiSquareAsymmetry", sizeof(kernel));
    break;
  case FITTYPE_MU_MINUS:
    // not yet implemented
  default:
    return DKS_ERROR;
  }

  //check the GPU kernel
  ierr = m_oclbase->ocl_checkKernel(kernel, blockSize_m, true, threadsPerBlock);
  if (threadsPerBlock < blockSize_m) {
    std::cout << "Default OpenCL blocksize changed in DKS to: " << threadsPerBlock << std::endl;
    blockSize_m = threadsPerBlock;
  }

  return ierr;

}
