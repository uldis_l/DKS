#ifndef H_OPENCL_CHISQUARE_RUNTIME
#define H_OPENCL_CHISQUARE_RUNTIME

#include <iostream>
#include <string>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include "../Algorithms/ChiSquareRuntime.h"
#include "OpenCLBase.h"

const std::string openclFunctHeader = "double fTheory(double t, __local double *p, __local double *f, __local int *m) {";

const std::string openclFunctFooter = "}\n";

/**
 * OpenCL implementation of ChiSquareRuntime class.
 * Implements ChiSquareRuntime interface to allow musrfit to target devices that
 * support OpenCL - Nvidia and AMD GPUs, Intel and AMD CPUs, Intel Xeon Phi.
 */
class OpenCLChiSquareRuntime : public ChiSquareRuntime {

private:

  OpenCLBase *m_oclbase;

  /** 
   * Private function to add user defined function to kernel string.
   */
  std::string buildProgram(std::string function);

  /**
   * Launch parallel reduction kernel to calculate the sum of data array
   */
  double calculateSum(cl_mem data, int length);

public:

  /** 
   * Constructor wiht openclbase argument.
   */
  OpenCLChiSquareRuntime(OpenCLBase *base);
  
  /** 
   * Default constructor
   */
  OpenCLChiSquareRuntime();

  /** 
   * Default destructor
   */
  ~OpenCLChiSquareRuntime();

  /** 
   * Compile program and save ptx.
   * Add function string to the calcFunction kernel and compile the program
   * Function must be valid C math expression. Parameters can be addressed in
   * a form par[map[idx]]
   */
  int compileProgram(std::string function, bool mlh = false);

  /** 
   * Launch selected kernel.
   * Launched the selected kernel from the compiled code.
   * Result is put in &result variable
   */
  int launchChiSquare(int fitType,
		      void *mem_data, void *mem_err, int length,
		      int numpar, int numfunc, int nummap,
		      double timeStart, double timeStep,
		      double &result);

  /** 
   * Write params to device.
   * Write params from double array to mem_param_m memory on the device.
   */
  int writeParams(const double *params, int numparams); 

  /** 
   * Write functions to device.
   * Write function values from double array to mem_func_m memory on the device.
   */
  int writeFunc(const double *func, int numfunc);

  /** 
   * Write maps to device.
   * Write map values from int array to mem_map_m memory on the device.
   */
  int writeMap(const int *map, int nummap);

  /** 
   * Allocate temporary memory needed for chi square.
   * Initializes the necessary temporary memory for the chi square calculations. Size_data needs to
   * the maximum number of elements in any datasets that will be used for calculations. Size_param,
   * size_func and size_map are the maximum number of parameters, functions and maps used in 
   * calculations.
   */
  int initChiSquare(int size_data, int size_param, int size_func, int size_map);

  /** 
   * Free temporary memory allocated for chi square.
   * Frees the chisq temporary memory and memory for params, functions and maps
   */
  int freeChiSquare();

  /** 
   * Check MuSR kernels for necessary resources.
   * Query device properties to get if sufficient resources are
   * available to run the kernels. Also checks if double precission is enabled on the device.
   */
  int checkChiSquareKernels(int fitType, int &threadsPerBlock);

};

#endif
