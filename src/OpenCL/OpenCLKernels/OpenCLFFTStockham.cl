#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#define TWOPI 6.28318530718

__kernel void fft_radix2(__global double2* src, __global double2* dst, const int p, const int t) {

  const int gid = get_global_id(0);
  const int k = gid & (p - 1);
  const int m = (gid << 1) - k;

  //src += gid;
  //dst += (gid << 1) - k;

  //const double2 in1 = src[0];
  //const double2 in2 = src[t];

  const double2 in1 = src[gid];
  const double2 in2 = src[gid+t];

  const double theta = (-2*M_PI*k) / (p << 1);
  double cs;

  double sn = sincos(theta, &cs);
  const double2 temp = (double2) (in2.x * cs - in2.y * sn, in2.y * cs + in2.x * sn);

  //dst[0] = in1 + temp;
  //dst[p] = in1 - temp;	
	
  dst[m] = in1 + temp;
  dst[m+p] = in1 - temp;

}

__kernel void fft3d_radix2_transpose(__global double2* src, __global double2* dst, const int p, const int t, const int ndim) {

  /* get ids */
  const int gid1 = get_global_id(0);
  const int gid2 = get_global_id(1);
  const int gid3 = get_global_id(2);

  /* calc indexes */
  int t2 = 2*t;
	
  int k = gid1 & (p - 1);
  int	m = (gid1 << 1) - k;
	
  int tmp = gid3*t2*t2 + gid2*t2;
	
  int in1 = tmp + gid1;
  int	in2 = in1 + t;
	
  int	out1 = tmp + m;
  int	out2 = out1 + p;
	
  /* calc FFT */
  const double2 d1 = src[in1];
  const double2 d2 = src[in2];

  const double theta = (-2*M_PI*k) / (p << 1);
  double cs;

  double sn = sincos(theta, &cs);
  const double2 temp = (double2) (d2.x * cs - d2.y * sn, d2.y * cs + d2.x * sn);
	
  dst[out1] = d1 + temp;
  dst[out2] = d1 - temp;
}

__kernel void fft3d_radix2(__global double2* src, __global double2* dst, const int p, const int t, const int ndim) {

  const int gid1 = get_global_id(0);
  const int gid2 = get_global_id(1);
  const int gid3 = get_global_id(2);

  int t2 = 2*t;
  int k, m, in1, in2, out1, out2;
  in1 = gid3*t2*t2 + gid2*t2 + gid1;
  if (ndim == 1) {
    k = gid1 & (p - 1);
    m = (gid1 << 1) - k;
    in2 = in1 + t;
    out1 = gid3*t2*t2 + gid2*t2 + m;
    out2 = out1 + p;
  } else if (ndim == 2) {
    k = gid2 & (p - 1);
    m = (gid2 << 1) - k;
    in2 = in1 + t2*t;
    out1 = gid3*t2*t2 + m*t2 + gid1;
    out2 = out1 + t2*p;
  } else if (ndim == 3) {
    k = gid3 & (p - 1);
    m = (gid3 << 1) - k;
    in2 = in1 + t2*t2*t;
    out1 = m*t2*t2 + gid2*t2 + gid1;
    out2 = out1 + p*t2*t2;
  }
	
  const double2 d1 = src[in1];
  const double2 d2 = src[in2];

  const double theta = (-2*M_PI*k) / (p << 1);
	
  double cs;
  double sn = sincos(theta, &cs);
  const double2 temp = (double2) (d2.x * cs - d2.y * sn, d2.y * cs + d2.x * sn);
	
  dst[out1] = d1 + temp;
  dst[out2] = d1 - temp;	
}


__kernel void transpose(__global double2 *data, int ndim, int dim) {

  int k = get_global_id(0);
  int j = get_global_id(1);
  int i = get_global_id(2);
  int nk = get_global_size(0);
  int nj = get_global_size(1);
  int ni = get_global_size(2);
	
  int n, m;
  n = i*ni*ni + j*nj + k;
  if (dim == 2) 
    m = i*ni*ni + k*nj + j;
  else
    m = k*ni*ni + j*nj + i;
		
  if (n < m) {
    double2 tmp = data[m];
    data[m] = data[n];
    data[n] = tmp;
  }
}

#define PI2 6.28318530718

__kernel void fft_batch3D(__global double2 *data_in, __local double2 *d, __local double2 *r, __local double2 *tmp, int N, int dim) {

  int id1 = get_global_id(0);
  int id2 = get_global_id(1);
  int id3 = get_global_id(2);
		
  //calc indexes
  int sid, offset;
  if (dim == 1) {
    sid = id3*N*N + id2*N;
    offset = 1;
  } else if (dim == 2) {
    sid = id3*N*N + id2;
    offset = N;
  } else if (dim == 3) {
    sid = id3*N + id2;
    offset = N*N;
  }
	
  //copy data from global memory to local
  int i1 = id1;
  int i2 = id1+N/2;
  d[i1] = data_in[sid + i1*offset];
  d[i2] = data_in[sid + i2*offset];
	
  barrier(CLK_LOCAL_MEM_FENCE);
  //barrier(CLK_GLOBAL_MEM_FENCE);
	
  //exec fft
  int p1, p2, j, k, out1, step, jump, t;
  double theta, cs, sn;
	
  t = 1;
  step = 1;
  while (step < N) {
    jump = step << 1;
		
    j = i1 >> (t - 1); // same as i1 / step, because t-1 = log2(step)
    k = i2 & (step - 1); // same as i2 % step
		
    out1 = j * jump + k;
		
    theta = -PI2 * k / jump;
    sn = sincos(theta, &cs);
		
    double2 temp = (double2) (d[i2].x*cs - d[i2].y*sn, d[i2].y*cs + d[i2].x * sn);
		
		
    r[out1] = d[i1] + temp;
    r[out1+step] = d[i1] - temp;

    t++;		
    step = jump;
		
    //swap local arrays
    tmp = r;
    r = d;
    d = tmp;
		
    //wait for all threads to finish this iteration
    barrier(CLK_LOCAL_MEM_FENCE);
  }
	
  tmp = r;
  r = d;
  d = tmp;

  //copy data from local memory to global
  data_in[sid + i1*offset] = r[i1];
  data_in[sid + i2*offset] = r[i2];

}







