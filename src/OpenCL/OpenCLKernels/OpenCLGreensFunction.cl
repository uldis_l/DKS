#pragma OPENCL EXTENSION cl_khr_fp64 : enable

/** compute the greens integral analytically */
__kernel void kernelTmpgreen(__global double *tmpgreen, double hr_m0, double hr_m1, double hr_m2,
			     int NI, int NJ, int NK)
{

  int tid = get_local_size(0);
  int id = get_global_id(0);

  if (id < NI * NJ * NK) {
    int i = id % NI;
    int k = id / (NI * NJ);
    int j = (id - k * NI * NJ) / NI;
  
    
    double cellVolume = hr_m0 * hr_m1 * hr_m2;
    
    double vv0 = i * hr_m0 - hr_m0 / 2;
    double vv1 = j * hr_m1 - hr_m1 / 2;
    double vv2 = k * hr_m2 - hr_m2 / 2;
  
    double r = sqrt(vv0 * vv0 + vv1 * vv1 + vv2 * vv2);
    
    double tmpgrn  = -vv2*vv2 * atan(vv0 * vv1 / (vv2 * r) );
    tmpgrn += -vv1*vv1 * atan(vv0 * vv2 / (vv1 * r) );
    tmpgrn += -vv0*vv0 * atan(vv1 * vv2 / (vv0 * r) );
  
    tmpgrn = tmpgrn / 2;

    tmpgrn += vv1 * vv2 * log(vv0 + r);
    tmpgrn += vv0 * vv2 * log(vv1 + r);
    tmpgrn += vv0 * vv1 * log(vv2 + r);

    tmpgreen[id] = tmpgrn / cellVolume;
    
  }

}

/** perform the actual integration */
__kernel void kernelIntegration(__global double *rho2_m, __global double *tmpgreen, 
				int NI, int NJ, int NI_tmp, int NJ_tmp, int NK_tmp) 
{
  
  int tid = get_local_id(0);
  int id = get_global_id(0);

  int ni = NI;
  int nj = NJ;
  
  double tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7;

  if (id < NI_tmp * NJ_tmp * NK_tmp) {
    int i = id % NI_tmp;
    int k = id / (NI_tmp * NJ_tmp);
    int j = (id - k * NI_tmp * NJ_tmp) / NI_tmp;

    tmp0 = 0; tmp1 = 0; tmp2 = 0; tmp3 = 0;
    tmp4 = 0; tmp5 = 0; tmp6 = 0; tmp7 = 0;
    
    if (i+1 < NI_tmp && j+1 < NJ_tmp && k+1 < NK_tmp)
      tmp0 = tmpgreen[(i+1) + (j+1) * NI_tmp + (k+1) * NI_tmp * NJ_tmp];
  
    if (i+1 < NI_tmp)
      tmp1 = tmpgreen[(i+1) +  j    * NI_tmp +  k * NI_tmp * NJ_tmp];
  
    if (j+1 < NJ_tmp)
      tmp2 = tmpgreen[ i    + (j+1) * NI_tmp +  k * NI_tmp * NJ_tmp];
  
    if (k+1 < NK_tmp)
      tmp3 = tmpgreen[ i    +  j    * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
    if (i+1 < NI_tmp && j+1 < NJ_tmp)
      tmp4 = tmpgreen[(i+1) + (j+1) * NI_tmp +  k * NI_tmp * NJ_tmp];  
  
    if (i+1 < NI_tmp && k+1 < NK_tmp)
      tmp5 = tmpgreen[(i+1) +  j    * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
    if (j+1 < NJ_tmp && k+1 < NK_tmp)
      tmp6 = tmpgreen[ i    + (j+1) * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
    tmp7 = tmpgreen[ i    +  j    * NI_tmp +  k * NI_tmp * NJ_tmp];
    
    double tmp_rho = tmp0 + tmp1 + tmp2 + tmp3 - tmp4 - tmp5 - tmp6 - tmp7;

    rho2_m[i + j*ni +  k*ni*nj] = tmp_rho;
  }
}

/** miror rho-field */
__kernel void kernelMirroredRhoField0(__global double *rho2_m, int NI, int NJ) {
  rho2_m[0] = rho2_m[NI*NJ];
}

__kernel void kernelMirroredRhoField(__global double *rho2_m, 
				     int NI, int NJ, int NK, 
				     int NI_tmp, int NJ_tmp, int NK_tmp,
				     int size) 
{

  int tid = get_local_id(0);
  int id = get_global_id(0);

  if (id == 0)
   rho2_m[0] = rho2_m[NI * NJ];

  barrier(CLK_GLOBAL_MEM_FENCE);

  int id1, id2, id3, id4, id5, id6, id7, id8;

  if (id < NI_tmp * NJ_tmp * NK_tmp) {
    int i = id % NI_tmp;
    int k = id / (NI_tmp * NJ_tmp);
    int j = (id - k * NI_tmp * NJ_tmp) / NI_tmp;

    int ri = NI - i;
    int rj = NJ - j;
    int rk = NK - k;

    id1 = k * NI * NJ + j * NI + i;
    id2 = k * NI * NJ + j * NI + ri;
    id3 = k * NI * NJ + rj * NI + i;
    id4 = k * NI * NJ + rj * NI + ri;

    id5 = rk * NI * NJ + j * NI + i;
    id6 = rk * NI * NJ + j * NI + ri;
    id7 = rk * NI * NJ + rj * NI + i;
    id8 = rk * NI * NJ + rj * NI + ri;
    
    double data = 0.0;
    if (id1 < size)
      data = rho2_m[id1];
    
    if (i != 0 && id2 < size) rho2_m[id2] = data;
    
    if (j != 0 && id3 < size) rho2_m[id3] = data;

    if (i != 0 && j != 0 && id4 < size) rho2_m[id4] = data;
    
    if (k != 0 && id5 < size) rho2_m[id5] = data;
    
    if (k !=  0 && i != 0 && id6 < size) rho2_m[id6] = data;
    
    if (k!= 0 && j != 0 && id7 < size) rho2_m[id7] = data;
    
    if (k != 0 && j != 0 & i != 0 && id8 < size) rho2_m[id8] = data;     
  }

}

/** multiply complex fields */
double2 ComplexMul(double2 a, double2 b) {
  double2 c;
  c.x = a.x * b.x - a.y * b.y;
  c.y = a.x * b.y + a.y * b.x;

  return c;
}

__kernel void multiplyComplexFields(__global double2 *ptr1, __global double2 *ptr2, 
				    int size) 
{
  
  int idx = get_global_id(0);

  if (idx < size)
    ptr1[idx] = ComplexMul(ptr1[idx], ptr2[idx]);
  
}
