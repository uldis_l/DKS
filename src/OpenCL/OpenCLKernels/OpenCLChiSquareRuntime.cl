#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#define PI     3.141592653589793115998
#define TWO_PI 6.283185307179586231996
#define DEG_TO_RAD 1.7453292519943295474371681e-2

/** From 'Numerical Recipes in C' by Press et.al, 1992. */
//Returns the Bessel function J0(x) for any real x.
double bessj0(double x) {
  double ax,z;
  double xx,y,ans,ans1,ans2; //Accumulate polynomials in double precision.
  
  if ((ax=fabs(x)) < 8.0) { //Direct rational function fit.
    y=x*x;
    ans1=57568490574.0+y*(-13362590354.0+y*(651619640.7+y*(-11214424.18+y*(77392.33017+y*(-184.9052456)))));
    ans2=57568490411.0+y*(1029532985.0+y*(9494680.718+y*(59272.64853+y*(267.8532712+y*1.0))));
    ans=ans1/ans2;
  } else { //Fitting function (6.5.9).
    z=8.0/ax;
    y=z*z;
    xx=ax-0.785398164;
    ans1=1.0+y*(-0.1098628627e-2+y*(0.2734510407e-4+y*(-0.2073370639e-5+y*0.2093887211e-6)));
    ans2 = -0.1562499995e-1+y*(0.1430488765e-3+y*(-0.6911147651e-5+y*(0.7621095161e-6-y*0.934945152e-7)));
    ans=sqrt(0.636619772/ax)*(cos(xx)*ans1-z*sin(xx)*ans2);
  }
  return ans;
}

/** Theory function declaration.
 * Definition of the theory function will be build during runtime before compilation.
 */
double fTheory(double t, __local double *p, __local double *f, __local int *m);

/** MusrFit predefined functions.
 * Predefined functions from MusrFit that can be used to define the theory function.
 * First parameter in all the functions is alwats time - t, rest of the parameters depend
 * on the function.
 */
double se(double t, double lamda) {
  return exp( -lamda*t );
}

double ge(double t, double lamda, double beta) {
  return exp( -pow(lamda*t, beta) );
}

double sg(double t, double sigma) {
  return exp( -0.5 * pow(sigma*t, 2) );
}

double stg(double t, double sigma) {
  double sigmatsq = pow(sigma*t,2);
  return (1.0/3.0) + (2.0/3.0)*(1.0 - sigmatsq) * exp(-0.5 * sigmatsq);
}

double sekt(double t, double lambda) {
  double lambdat = lambda*t;

  return (1.0/3.0) + (2.0/3.0)*(1.0 - lambdat) * exp(-lambdat);
}

double lgkt(double t, double lambda, double sigma) {
  double lambdat = lambda*t;
  double sigmatsq = pow(sigma*t, 2.0);

  return (1.0/3.0) + (2.0/3.0)*(1.0 - lambdat - sigmatsq) * exp(-lambdat - 0.5*sigmatsq);
}

double skt(double t, double sigma, double beta) {
  if (beta < 1.0e-3)
    return 0.0;
  double sigmatb = pow(sigma*t, beta);

  return (1.0/3.0) + (2.0/3.0)*(1.0 - sigmatb) * exp(-sigmatb/beta);
}

double spg(double t, double lambda, double gamma, double q) {
  double lam2 = lambda*lambda;
  double lamt2q = t*t*lam2*q;
  double rate2 = 4.0*lam2*(1.0-q)*t/gamma;
  double rateL = sqrt(fabs(rate2));
  double rateT = sqrt(fabs(rate2)+lamt2q);

  return (1.0/3.0)*exp(-rateL) + (2.0/3.0)*(1.0 - lamt2q / rateT)*exp(-rateT);
}

double rahf(double t, double nu, double lambda) {
  double nut  = nu*t;
  double nuth = nu*t/2.0;
  double lamt = lambda*t;

  return (1.0/6.0)*(1.0-nuth)*exp(-nuth) + (1.0/3.0)*(1.0-nut/4.0)*exp(-0.25*(nut+2.44949*lamt));
}

double tf(double t, double phi, double nu) {
  double tmp_nu = TWO_PI*nu*t;
  double tmp_phi = DEG_TO_RAD * phi;

  return cos(tmp_nu + tmp_phi);
}

double ifld(double t, double alpha, double phi, double nu, double lambdaT, double lambdaL) {
  double wt = TWO_PI*nu*t;
  double ph = DEG_TO_RAD*phi;

  return alpha*cos(wt+ph)*exp(-lambdaT*t) + (1.0-alpha)*exp(-lambdaL*t);
}

double ifgk(double t, double alpha, double nu, double sigma, double lambda, double beta) {
  double wt = TWO_PI*nu*t;
  double rate2 = sigma*sigma*t*t;
  double rateL = 0.0;
  double result = 0.0;

  // make sure lambda > 0
  if (lambda < 0.0)
    return 0.0;

  if (beta < 0.001) {
    rateL = 1.0;
  } else {
    rateL = pow(lambda*t, beta);
  }

  if (nu < 0.01) {
    result = (1.0-alpha)*exp(-rateL) + alpha*(1.0-rate2)*exp(-0.5*rate2);
  } else {
    result = (1.0-alpha)*exp(-rateL) + alpha*(cos(wt)-sigma*sigma*t*t/(wt)*sin(wt))*exp(-0.5*rate2);
  }

  return result;
}

double ifll(double t, double alpha, double nu, double a, double lambda, double beta) {
  double wt = TWO_PI*nu*t;
  double at = a*t;
  double rateL = 0.0;
  double result = 0.0;

  // make sure lambda > 0
  if (lambda < 0.0)
    return 0.0;

  if (beta < 0.001) {
    rateL = 1.0;
  } else {
    rateL = pow(lambda*t, beta);
  }

  if (nu < 0.01) {
    result = (1.0-alpha)*exp(-rateL) + alpha*(1.0-at)*exp(-at);
  } else {
    result = (1.0-alpha)*exp(-rateL) + alpha*(cos(wt)-a/(TWO_PI*nu)*sin(wt))*exp(-at);
  }

  return result;
}

double b(double t, double phi, double nu) {
  return bessj0(TWO_PI*nu*t + DEG_TO_RAD*phi);
}

double ib(double t, double alpha, double phi, double nu, double lambdaT, double lambdaL) {
  double wt = TWO_PI * nu * t;
  double ph = DEG_TO_RAD * phi;

  return alpha*bessj0(wt+ph)*exp(-lambdaT*t) + (1.0-alpha)*exp(-lambdaL*t);
}

double ab(double t, double sigma, double gamma) {
  double gt = gamma*t;

  return exp(-pow(sigma/gamma,2.0)*(exp(-gt) - 1.0 + gt));
}

double snkzf(double t, double Delta0, double Rb) {
  double D0t2 = pow(Delta0*t, 2.0);
  double aa = 1.0/(1.0+pow(Rb,2.0)*D0t2);

  return (1.0/3.0) + (2.0/3.0)*pow(aa,1.5)*(1.0-D0t2*aa)*exp(-0.5*D0t2*aa);
}

double snktf(double t, double phi, double nu, double Delta0, double Rb) {
  double wt = TWO_PI*nu*t;
  double ph = DEG_TO_RAD*phi;
  double D0t2 = pow(Delta0*t, 2.0);
  double aa = 1.0/(1.0+pow(Rb,2.0)*D0t2);

  return sqrt(aa)*exp(-0.5*D0t2*aa)*cos(wt+ph);
}

double dnkzf(double t, double Delta0, double Rb, double nuc) {
  double nuct = nuc*t;
  double theta = (exp(-nuct) - 1.0 -nuct)/pow(nuc, 2.0);
  double aa = 1.0/(1.0+4.0*pow(Rb*Delta0,2.0)*theta);

  return sqrt(aa)*exp(-2.0*Delta0*Delta0*theta*aa);
}

double dnktf(double t, double phi, double nu, double Delta0, double Rb, double nuc) {
  double wt = TWO_PI*nu*t;
  double ph = DEG_TO_RAD*phi;
  double nuct = nuc*t;
  double theta = (exp(-nuct) - 1.0 -nuct)/pow(nuc, 2.0);
  double aa = 1.0/(1.0+2.0*pow(Rb*Delta0,2.0)*theta);

  return sqrt(aa)*exp(-Delta0*Delta0*theta*aa)*cos(wt+ph);
}

__kernel void kernelChiSquareSingleHisto(__global double *data, __global double *err,
            __global double *par, __global double *chisq, __global  int *map, __global double *funcv,
			      int length, int numpar, int numfunc, int nummap,
			      double timeStart, double timeStep,         
			      double tau, double N0, double bkg, 
			      __local double *p, __local double *f, __local int *m)
{

  //get thread id and calc global id
  int tid = get_local_id(0);
  int j = get_global_id(0);
  int lsize = get_local_size(0);

  //load parameters from global to shared memory                           
  while (tid < numpar) {
    p[tid] = par[tid];
    tid += lsize;
  }

  //load functions from global to shared memory
  tid = get_local_id(0);
  while (tid < numfunc) {
    f[tid] = funcv[tid];
    tid += lsize;
  }

  //load maps from global memory
  tid = get_local_id(0);
  while (tid < nummap) {
    m[tid] = map[tid];
    tid += lsize;
  }

  //sync threads
  barrier(CLK_LOCAL_MEM_FENCE);

  while (j < length) {
    
    double t = timeStart + j*timeStep;                                  
    double ldata = data[j];
    double lerr = err[j];

    double theo = N0 * exp (-t/tau ) * (1.0 + fTheory(t, p, f, m)) + bkg;

    #ifdef MLH
    if ((ldata > 1.0e-9) && (fabs(theo) > 1.0e-9))
      chisq[j] = 2.0 * ((theo - ldata) + ldata * log(ldata / theo));
    else
      chisq[j] = 2.0 * (theo - ldata);
    #else
    if (lerr != 0.0)
      chisq[j] = (theo - ldata) * (theo - ldata) / (lerr * lerr);
    else
      chisq[j] = theo * theo;
    #endif

    j += get_global_size(0); 
  }

}

__kernel void kernelChiSquareAsymmetry(__global double *data, __global double *err,
            __global double *par, __global double *chisq, __global  int *map, __global double *funcv,
            int length, int numpar, int numfunc, int nummap,
            double timeStart, double timeStep,
            double alpha, double beta,
            __local double *p, __local double *f, __local int *m)
{

  //get thread id and calc global id
  int tid = get_local_id(0);
  int j = get_global_id(0);
  int lsize = get_local_size(0);

  //load parameters from global to shared memory
  while (tid < numpar) {
    p[tid] = par[tid];
    tid += lsize;
  }

  //load functions from global to shared memory
  tid = get_local_id(0);
  while (tid < numfunc) {
    f[tid] = funcv[tid];
    tid += lsize;
  }

  //load maps from global memory
  tid = get_local_id(0);
  if (tid < nummap) {
    m[tid] = map[tid];
    tid += lsize;
  }

  //sync threads
  barrier(CLK_LOCAL_MEM_FENCE);

  while (j < length) {

    double t = timeStart + j*timeStep;
    double ldata = data[j];
    double lerr = err[j];

    double ab = alpha*beta;
    double theoVal = fTheory(t, p, f, m);
    double theo = ((ab+1.0)*theoVal - (alpha-1.0))/((alpha+1.0)-(ab-1.0)*theoVal);

    #ifdef MLH
    chisq[j] = 0.0; // max log likelihood not defined for asymmetry fit
    #else
    if (lerr != 0.0)
      chisq[j] = (theo - ldata) * (theo - ldata) / (lerr * lerr);
    else
      chisq[j] = theo * theo;
    #endif

    j += get_global_size(0); 
  }

}

__kernel void parallelReductionSum(__global double *data_in, __global double *data_out,
				   __local double *data_local, int size) 
{

  //get local and global ids, and work group size
  int local_id = get_local_id(0);
  int global_id = get_global_id(0);
  int group_size = get_local_size(0);

  //copy from global memory to local, if global id out of bounds fill with 0s
  if (global_id < size)
    data_local[local_id] = data_in[global_id];
  else
    data_local[local_id] = 0;

  //loop trough reduction steps
  for (uint stride = group_size / 2; stride > 0; stride /= 2) {

    //synch all work items in work group
    barrier(CLK_LOCAL_MEM_FENCE);

    //create partials summs each step
    if (local_id < stride)
      data_local[local_id] += data_local[local_id + stride];
  }
 
  //local thread 0 writes final partial sum to global memory
  if (local_id == 0)
    data_out[get_group_id(0)] = data_local[0];

}

__kernel void parallelReductionTwoPhase(__global double *data_in, __global double *data_out, 
					__local double *data_local, int size)
{
  //get local and global ids, and work group size
  int local_id = get_local_id(0);
  int global_id = get_global_id(0);
  int global_size = get_global_size(0);
  int group_size = get_local_size(0);

  double acc = 0;
  while (global_id < size) {
    acc += data_in[global_id];
    global_id += global_size;
  }

  //parallel reduction on local work group
  data_local[local_id] = acc;
  barrier(CLK_LOCAL_MEM_FENCE);
  for (uint stride = group_size / 2; stride > 0; stride /= 2) {
    //synch all work items in work group
    barrier(CLK_LOCAL_MEM_FENCE);

    //create partials summs each step
    if (local_id < stride)
      data_local[local_id] += data_local[local_id + stride];
  }

  //local thread 0 writes final partial sum to global memory
  if (local_id == 0)
    data_out[get_group_id(0)] = data_local[0];

}
