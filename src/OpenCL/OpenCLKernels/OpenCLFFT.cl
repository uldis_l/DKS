#pragma OPENCL EXTENSION cl_khr_fp64 : enable

/* 3D normalize FFT kernel */
__kernel void normalizeFFT(__global double2 *input, int N) {
  int i1 = get_global_id(0);
  int i2 = get_global_id(1);
  int i3 = get_global_id(2);
  int n1 = get_global_size(0);
  int n2 = get_global_size(1);
  int n3 = get_global_size(2);
    
  int id = i1;
  if (n2 > 1)
    id += i2*n2;
  if (n3 > 1)
    id += i3*n2*n2;
	
  input[id].x = input[id].x / N;
  input[id].y = input[id].y / N;
}

/* 3D radix 2 FFT kernel */
__kernel void FFT3D(__global double2 *input, int step, int dim, int forward) {
    
  int n1 = get_global_size(0);
  int n2 = get_global_size(1);
  int n3 = get_global_size(2);
  int i1 = get_global_id(0);
  int i2 = get_global_id(1);
  int i3 = get_global_id(2);
    
  int jump = step << 1;
    
  int d, idGroup, idLoc, idTwidle, id, match;
  if (dim == 0) {
	
    d = n1 / step;	// n1 >> log2(step)
    idLoc = i1 / d;
    idGroup = i1 & (d-1); //modulo

    idTwidle = idGroup * jump + idLoc;
    id = i3*n3*n3 + i2*n2 + idTwidle;
    match = id + step;
  } else if (dim == 1) {
	
    d = n2 / step;
    idLoc = i2 / d;
    idGroup = i2 & (d-1);
	
    idTwidle = idGroup * jump + idLoc;
    id = i3*n3*n3 + idTwidle*n1 + i1;
    match = id + step*n1;
  } else if (dim == 2) {
	
    d = n3 / step;
    idLoc = i3 / d;
    idGroup = i3 & (d-1);
	
    idTwidle = idGroup * jump + idLoc;
    id = idTwidle*n1*n1 + i2*n2 + i1;
    match = id + step*n1*n1;
  }
    
  double alpha;
  if (forward == 1)
    alpha = -( 2 * M_PI / jump ) * idTwidle;
  else
    alpha = ( 2 * M_PI / jump ) * idTwidle;
	
  double wr, wi;
  wi = sincos(alpha, &wr);
    
  double2 cTemp;
  double2 cTempId = input[id];
  double2 cTempMatch = input[match];
    
  cTemp.x = wr*cTempMatch.x - wi*cTempMatch.y;
  cTemp.y = wr*cTempMatch.y + wi*cTempMatch.x;
    
  input[match] = cTempId - cTemp;
  input[id] = cTempId + cTemp;
    
}

/* 3D bit reversal sort */
__kernel void BitReverseSort3D(__global double2 *input, int bits, int dim) {

  int n = get_global_size(0);
  int i1 = get_global_id(0);
  int i2 = get_global_id(1);
  int i3 = get_global_id(2);

  int irev, itmp, istart;
  if (dim == 0) {
    istart = i1;
    irev = i1;
    itmp = i1;
  } else if (dim == 1) {
    irev = i2;
    itmp = i2;
    istart = i2;
  } else if (dim == 2) {
    irev = i3;
    itmp = i3;
    istart = i3;
  }
     
  for (int j = 1; j < bits; j++) {
    itmp >>= 1;
    irev <<= 1;
    irev |= itmp & 1;
  }
  irev &= n - 1;
    
  int id1, id2;
  if (istart < irev) {
    double2 tmp;
    id1 = i3*n*n + i2*n + i1;
    if (dim == 0) { //i1, irev - w, i2 - h, i3 - d
      id2 = i3*n*n + i2*n + irev;
      tmp = input[id1];
      input[id1] = input[id2];
      input[id2] = tmp;
    } else if (dim == 1) { // i1 - w, i2, irev - h, i3 - d
      id2 = i3*n*n + irev*n + i1;
      tmp = input[id1];
      input[id1] = input[id2];
      input[id2] = tmp;
    } else if (dim == 2) { // i1 - w, i2 - h, i3, irev - d
      id2 = irev*n*n + i2*n + i1;
      tmp = input[id1];
      input[id1] = input[id2];
      input[id2] = tmp;
    }
  }
}


/* 3D FFT kernel based on Stockham's out-of-place algorithm */
__kernel void fft3d_radix2(__global double2* src, __global double2* dst, const int p, const int t, const int ndim, const int forward) {

  const int gid1 = get_global_id(0);
  const int gid2 = get_global_id(1);
  const int gid3 = get_global_id(2);

  int t2 = 2*t;
  int k, m, in1, in2, out1, out2;
  in1 = gid3*t2*t2 + gid2*t2 + gid1;
  if (ndim == 1) {
    k = gid1 & (p - 1);
    m = (gid1 << 1) - k;
    in2 = in1 + t;
    out1 = gid3*t2*t2 + gid2*t2 + m;
    out2 = out1 + p;
  } else if (ndim == 2) {
    k = gid2 & (p - 1);
    m = (gid2 << 1) - k;
    in2 = in1 + t2*t;
    out1 = gid3*t2*t2 + m*t2 + gid1;
    out2 = out1 + t2*p;
  } else if (ndim == 3) {
    k = gid3 & (p - 1);
    m = (gid3 << 1) - k;
    in2 = in1 + t2*t2*t;
    out1 = m*t2*t2 + gid2*t2 + gid1;
    out2 = out1 + p*t2*t2;
  }
	
  const double2 d1 = src[in1];
  const double2 d2 = src[in2];

  const double theta = (forward*2*M_PI*k) / (p << 1);
		
  double cs;

  double sn = sincos(theta, &cs);
  const double2 temp = (double2) (d2.x * cs - d2.y * sn, d2.y * cs + d2.x * sn);
	
  dst[out1] = d1 + temp;
  dst[out2] = d1 - temp;	
}
