#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#define TAU 2.197019


__kernel void parallelReductionSum(__global double *data_in, __global double *data_out, 
				   __local double *data_local, int size) 
{

  //get local and global ids, and work group size
  int local_id = get_local_id(0);
  int global_id = get_global_id(0);
  int group_size = get_local_size(0);

  //copy from global memory to local, if global id out of bounds fill with 0s
  if (global_id < size)
    data_local[local_id] = data_in[global_id];
  else
    data_local[local_id] = 0;

  //loop trough reduction steps
  for (uint stride = group_size / 2; stride > 0; stride /= 2) {

    //synch all work items in work group
    barrier(CLK_LOCAL_MEM_FENCE);

    //create partials summs each step
    if (local_id < stride)
      data_local[local_id] += data_local[local_id + stride];
  }
 
  //local thread 0 writes final partial sum to global memory
  if (local_id == 0)
    data_out[get_group_id(0)] = data_local[0];

}

__kernel void kernelPHistoTFFcn(__global double *data, __global double *par, __global double *chisq,
				double fTimeResolution, double fRebin,
				int length, int sensors, int numpar,
				__local double *p)
{

  //get work item id and calc global id
  int tid = get_local_id(0);
  int j = get_global_id(0);

  //load parameters from global to shared memory
  if (tid < numpar)
    p[tid] = par[tid];

  //sync work items inside work group
  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

  if (j < length) {
    
    double dt0 = fTimeResolution * 0.5 * (fRebin - 1);
    double time = dt0 + fTimeResolution * fRebin * j;  
    double w = p[0]*0.08516155035269027;
    double tt = exp(-time/TAU);
    double pp = exp(-0.5 * pow(p[1]*time, 2.0));
    double wt = w * time;
    

    int idx;
    double ldata, theo;
    for (int i = 0; i < sensors; i++) {
      idx = i * length + j;
      ldata = data[idx];
  
      theo = p[2+i*4]*tt*(1.0+p[3+i*4]*pp*cos(wt+p[4+i*4]*1.74532925199432955e-2))+p[5+i*4]; 
  
      if (ldata != 0.0)
	chisq[idx] = (theo - ldata) * (theo - ldata) / ldata;
      else
	chisq[idx] = theo * theo;
    }
  }
}

__kernel void kernelSingleGaussTF(__global double *data, __global unsigned int *t0,
				  __global double *par, __global double *result,
				  double fTimeResolution, double fRebin, double fGoodBinOffset,
				  int length, int sensors, int numpar, __local double *p)
{

  //get work item id and calc global id
  int tid = get_local_id(0);
  int j = get_global_id(0);
  
  //load para,eters from global to shared memory
  if (tid < numpar)
    p[tid] = par[tid];

  //sync work items inside work group
  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

  if (j < length) {
    double dt0 = fTimeResolution*0.5*(fRebin - 1);
    double w1 = par[0]*0.08516155035269027;

    int idx;
    double ldata, lft0, theo, time;
    for (int  i = 0; i < sensors; i++) {
      idx = i * length + j;
      lft0 = t0[i];
      if (j >= lft0 + fGoodBinOffset/fRebin) {
	ldata = data[idx];
	time = dt0 + fTimeResolution * fRebin* (j - lft0);
	theo = p[2+i*4]*exp(-time/TAU)*(1.0+p[3+i*4]*exp(-0.5*pow(p[1]*time,2.0))
					*cos(w1*time+p[4+i*4]*1.74532925199432955e-2))+p[5+i*4]; 
	// 1.74532925199432955e-2 = pi/180

	if ( (ldata > 1.0e-9) && (fabs(theo) > 1.0e-9) )
	  result[idx] = (theo - ldata) + ldata*log(ldata/theo);
	else
	  result[idx] = theo - ldata;
      } else {
	result[idx] = 0;
      }
    }
  }

}

__kernel void kernelDoubleLorentzTF(__global double *data, __global unsigned int *t0,
				    __global double *par, __global double *result,
				    double fTimeResolution, double fRebin, double fGoodBinOffset,
				    int length, int sensors, int numpar, __local double *p)
{

  //get work item id and calc global id
  int tid = get_local_id(0);
  int j = get_global_id(0);
  
  //load para,eters from global to shared memory
  if (tid < numpar)
    p[tid] = par[tid];

  //sync work items inside work group
  barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

  if (j < length) {
    double dt0 = fTimeResolution*0.5*(fRebin - 1);
    double w1 = p[0]*0.08516155035269027;
    double w2 = p[2]*0.08516155035269027;

    int idx;
    double ldata, lft0, theo, time;
    for (int  i = 0; i < sensors; i++) {
      
      idx = i * length + j;
      lft0 = t0[i];
      if (j >= lft0 + fGoodBinOffset/fRebin) {
        ldata = data[idx];
        time = dt0+fTimeResolution*fRebin*(j-lft0);

	theo = p[4+i*5]*exp(-time/TAU)*
	  (1.0+p[8+i*5]*p[5+i*5]*exp(-p[1]*time)*
	   cos(w1*time+p[6+i*5]*1.74532925199432955e-2)+
	   (1.0-p[8+i*5])*p[5+i*5]*exp(-p[3]*time)*
	   cos(w2*time+p[6+i*5]*1.74532925199432955e-2))+p[7+i*5]; 
	// 1.74532925199432955e-2 = pi/180
	if ((ldata > 1.0e-9) && (fabs(theo) > 1.0e-9))
	  result[idx] = (theo - ldata) + ldata*log(ldata/theo);
	else
	  result[idx] = theo - ldata;
      } else {
      result[idx] = 0;
      }
    }
  }

}

