#pragma OPENCL EXTENSION cl_khr_fp64 : enable

/* transpose matrix */
__kernel void transpose(__global double2 *input, __global double2 *output, 
			int width, int height, __local double2 *block)
{

  //transfer row in shared memory
  unsigned int xIdx = get_global_id(0);
  unsigned int yIdx = get_global_id(1);
  int block_dim = get_local_size(0);

  if ( (xIdx < width) && (yIdx < height) ) {
    unsigned int idx_in = yIdx * width + xIdx;
    block[get_local_id(1)*(block_dim+1)+get_local_id(0)] = input[idx_in];
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  xIdx = get_group_id(1) * block_dim + get_local_id(0);
  yIdx = get_group_id(0) * block_dim + get_local_id(1);

  if ( (xIdx < height) && (yIdx < width) ) {
    unsigned int idx_out = yIdx * height + xIdx;
    output[idx_out] = block[get_local_id(0)*(block_dim+1)+get_local_id(1)];
  }

}

/* naive transpose matrix kernel */
__kernel void transpose_naive(__global double2 *input, __global double2 *output, int width, int height)
{
  unsigned int xIdx = get_global_id(0);
  unsigned int yIdx = get_global_id(1);

  if (xIdx < width && yIdx < height) {
    unsigned int idx_in = xIdx + width * yIdx;
    unsigned int idx_out = yIdx + height * xIdx;
    output[idx_out] = input[idx_in];
  }
}
