#ifndef H_OPENCL_CHI_SQUARE
#define H_OPENCL_CHI_SQUARE

#include <iostream>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include "OpenCLBase.h"

#define DKS_SUCCESS 0
#define DKS_ERROR 1

/** Deprecated, SimpleFit implementation of ChiSquare. */
class OpenCLChiSquare {

private:
  
  OpenCLBase *m_oclbase;

  double ocl_sum(cl_mem data, int length);
  
public:
	
  OpenCLChiSquare(OpenCLBase *base) {
    m_oclbase = base;
  }

  ~OpenCLChiSquare() { }
	
  int ocl_PHistoTFFcn(void *mem_data, void *mem_par, void *mem_result, 
		      double fTimeResolution, double fRebin,
		      int sensors, int length, int numpar,
		      double &result);
  
  int ocl_singleGaussTF(void *mem_data, void *mem_t0, void *mem_par, void *mem_result,
			double fTimeResolution, double fRebin, double fGoodBinOffset,
			int sensors, int length, int numpar,
			double &result);

  int ocl_doubleLorentzTF(void *mem_data, void *mem_t0, void *mem_par, void *mem_result,
			  double fTimeResolution, double fRebin, double fGoodBinOffset,
			  int sensors, int length, int numpar,
			  double &result);



};

#endif
