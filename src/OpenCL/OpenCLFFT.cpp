#include "OpenCLFFT.h"

//=====================================//
//==========Private functions==========//
//=====================================//

/*
  call fft kernels to execute FFT of the given domain, data - devevice memory ptr, cdim - current dim to transform, 
  ndim - totla number of dimmensions, N - size of dimension
*/
int OpenCLFFT::ocl_callFFTKernel(cl_mem &data, int cdim, int ndim, int N, bool forward) {

  //set the number of work items in each dimension
  size_t work_items[3];
  work_items[0] = N;
  work_items[1] = (ndim > 1) ? N : 1;
  work_items[2] = (ndim > 1) ? N : 1;
  work_items[cdim] = N / 2;
	
  int f = (forward) ? 1 : 0;
	
  //create kernel and set kernel arguments
  if (m_oclbase->ocl_createKernel("FFT3D") != OCL_SUCCESS)
    return OCL_ERROR;
	
  if (m_oclbase->ocl_setKernelArg(0, sizeof(cl_mem), &data) != OCL_SUCCESS)
    return OCL_ERROR;

  if (m_oclbase->ocl_setKernelArg(2, sizeof(int), &cdim) != OCL_SUCCESS)
    return OCL_ERROR;
	
  if (m_oclbase->ocl_setKernelArg(3, sizeof(int), &f) != OCL_SUCCESS)
    return OCL_ERROR;
	
  //execute kernel
  for (int step = 1; step < N; step <<= 1) {
    if (m_oclbase->ocl_setKernelArg(1, sizeof(int), &step) != OCL_SUCCESS)
      return OCL_ERROR;

    if (m_oclbase->ocl_executeKernel(ndim, work_items) != OCL_SUCCESS) 
      return OCL_ERROR;
  }
	
  return OCL_SUCCESS;
}

/*
  call ifft kernel to execute the bit reverse sort data - devevice memory ptr, cdim - current dim to transform, 
  ndim - totla number of dimmensions, N - size of dimension
*/
int OpenCLFFT::ocl_callBitReverseKernel(cl_mem &data, int cdim, int ndim, int N) {
  //set work item size
  size_t work_items[3];
  work_items[0] = N;
  work_items[1] = (ndim > 1) ? N : 1;
  work_items[2] = (ndim > 2) ? N : 1;

  //create kernel and set kernel arguments
  if (m_oclbase->ocl_createKernel("BitReverseSort3D") != OCL_SUCCESS)
    return OCL_ERROR;
	
  int bits = log2(N);
  if (m_oclbase->ocl_setKernelArg(0, sizeof(cl_mem), &data) != OCL_SUCCESS)
    return OCL_ERROR;
	
  if (m_oclbase->ocl_setKernelArg(1, sizeof(int), &bits) != OCL_SUCCESS)
    return OCL_ERROR;
	
  if (m_oclbase->ocl_setKernelArg(2, sizeof(int), &cdim) != OCL_SUCCESS)
    return OCL_ERROR;
	
  //execute kernel
  if (m_oclbase->ocl_executeKernel(ndim, work_items) != OCL_SUCCESS) {
    DEBUG_MSG("Error executing kernel");
    return OCL_ERROR;
  }

  return OCL_SUCCESS;
	
}


//=====================================//
//==========Public functions==========//
//=====================================//

/*
  call fft execution on device for every dimension
*/
int OpenCLFFT::executeFFT(void *data, int ndim, int N[3], int streamId, bool forward) {

  int dkserr = DKS_SUCCESS;
  cl_int ierr;
  cl_mem inout = (cl_mem)data;

  if (forward)
    ierr = clfftEnqueueTransform(planHandleZ2Z, CLFFT_FORWARD, 1, &m_oclbase->m_command_queue, 
				0, NULL, NULL, &inout, NULL, NULL);
  else
    ierr = clfftEnqueueTransform(planHandleZ2Z, CLFFT_BACKWARD, 1, &m_oclbase->m_command_queue, 
				0, NULL, NULL, &inout, NULL, NULL);  

  if (ierr != OCL_SUCCESS) {
    dkserr = DKS_ERROR;
    DEBUG_MSG("Error executing cfFFT\n");
    if (ierr == CLFFT_INVALID_PLAN)
      std::cout << "Invlalid plan" << std::endl;
    else 
      std::cout << "CLFFT error" << std::endl;
  }

  return dkserr;
}

/*
  call rcfft execution on device for every dimension
*/
int OpenCLFFT::executeRCFFT(void *real_ptr, void *comp_ptr, int ndim, int N[3], int streamId) {

  int dkserr = DKS_SUCCESS;
  cl_int ierr;
  cl_mem real_in = (cl_mem)real_ptr;
  cl_mem comp_out = (cl_mem)comp_ptr;

  ierr = clfftEnqueueTransform(planHandleD2Z, CLFFT_FORWARD, 1, &m_oclbase->m_command_queue, 
			       0, NULL, NULL, &real_in, &comp_out, NULL);
  
  if (ierr != OCL_SUCCESS) {
    dkserr = DKS_ERROR;
    DEBUG_MSG("Error executing cfFFT\n");
    if (ierr == CLFFT_INVALID_PLAN)
      std::cout << "Invlalid plan" << std::endl;
    else 
      std::cout << "CLFFT error" << std::endl;
  }

  return dkserr;
}

/*
  call rcfft execution on device for every dimension
*/
int OpenCLFFT::executeCRFFT(void *real_ptr, void *comp_ptr, int ndim, int N[3], int streamId) {

  int dkserr = DKS_SUCCESS;
  cl_int ierr;
  cl_mem real_in = (cl_mem)real_ptr;
  cl_mem comp_out = (cl_mem)comp_ptr;

  ierr = clfftEnqueueTransform(planHandleZ2D, CLFFT_BACKWARD, 1, &m_oclbase->m_command_queue, 
				0, NULL, NULL, &comp_out, &real_in, NULL);
  
  if (ierr != OCL_SUCCESS) {
    dkserr = DKS_ERROR;
    DEBUG_MSG("Error executing cfFFT\n");
    if (ierr == CLFFT_INVALID_PLAN)
      std::cout << "Invlalid plan" << std::endl;
    else 
      std::cout << "CLFFT error" << std::endl;
  }

  return dkserr;
}
	
/*
  execute ifft
*/
int OpenCLFFT::executeIFFT(void *data, int ndim, int N[3], int streamId) {
  executeFFT(data, ndim, N, streamId, false);
  return OCL_SUCCESS;
}
	
/*
  call kernel to normalize fft. clFFT inverse already includes the scaling so this is disabled.
*/
int OpenCLFFT::normalizeFFT(void *data, int ndim, int N[3], int streamId) {

/*
  cl_mem inout = (cl_mem)data;

  int n = N[0];

  //set work item size
  size_t work_items[3];
  work_items[0] = n;
  work_items[1] = (ndim > 1) ? n : 1;
  work_items[2] = (ndim > 2) ? n : 1;
	
  //create kernel
  if (m_oclbase->ocl_createKernel("normalizeFFT") != OCL_SUCCESS)
    return OCL_ERROR;
	
  //set kernel args
  unsigned int elements = pow(n, ndim);
  if (m_oclbase->ocl_setKernelArg(0, sizeof(cl_mem), &inout) != OCL_SUCCESS)
    return OCL_ERROR;
  if (m_oclbase->ocl_setKernelArg(1, sizeof(int), &elements) != OCL_SUCCESS)
    return OCL_ERROR;
		
  //execute kernel
  if (m_oclbase->ocl_executeKernel(ndim, work_items) != OCL_SUCCESS) {
    DEBUG_MSG("Error executing kernel");
    return OCL_ERROR;
  }
*/	
  return OCL_SUCCESS;
}

int OpenCLFFT::setupFFT(int ndim, int N[3]) {

  cl_int err;

  clfftDim dim;
  if (ndim == 1)
    dim = CLFFT_1D;
  else if (ndim == 2)
    dim = CLFFT_2D;
  else 
    dim = CLFFT_3D;
  size_t clLength[3] = {(size_t)N[0], (size_t)N[1], (size_t)N[2]};

  /* Create 3D fft plan*/
  err = clfftCreateDefaultPlan(&planHandleZ2Z, m_oclbase->m_context, dim, clLength);

  /* Set plan parameters */
  err = clfftSetPlanPrecision(planHandleZ2Z, CLFFT_DOUBLE);
  if (err != CL_SUCCESS)
    std::cout << "Error setting precision" << std::endl;
  err = clfftSetLayout(planHandleZ2Z, CLFFT_COMPLEX_INTERLEAVED, CLFFT_COMPLEX_INTERLEAVED);
  if (err != CL_SUCCESS)
    std::cout << "Error setting layout" << std::endl;
  err = clfftSetResultLocation(planHandleZ2Z, CLFFT_INPLACE);
  if (err != CL_SUCCESS)
    std::cout << "Error setting result location" << std::endl;
  /* Bake the plan */
  err = clfftBakePlan(planHandleZ2Z, 1, &m_oclbase->m_command_queue, NULL, NULL);

  if (err != CL_SUCCESS) {
    DEBUG_MSG("Error creating Complex-to-complex plan");
    return DKS_ERROR;
  }

  return DKS_SUCCESS;
}

int OpenCLFFT::setupFFTRC(int ndim, int N[3], double scale) {
  cl_int err;
  
  clfftDim dim;
  if (ndim == 1)
    dim = CLFFT_1D;
  else if (ndim == 2)
    dim = CLFFT_2D;
  else 
    dim = CLFFT_3D;

  size_t clLength[3] = {(size_t)N[0], (size_t)N[1], (size_t)N[2]};

  size_t half = (size_t)N[0] / 2 + 1;
  size_t clInStride[3] = {1, (size_t)N[0], (size_t)N[0]*N[1]};
  size_t clOutStride[3] = {1, half, half * N[1]};

  /* Create 3D fft plan*/
  err = clfftCreateDefaultPlan(&planHandleD2Z, m_oclbase->m_context, dim, clLength);

  /* Set plan parameters */
  err = clfftSetPlanPrecision(planHandleD2Z, CLFFT_DOUBLE);
  err = clfftSetLayout(planHandleD2Z, CLFFT_REAL, CLFFT_HERMITIAN_INTERLEAVED);
  err = clfftSetResultLocation(planHandleD2Z, CLFFT_OUTOFPLACE);
  err = clfftSetPlanInStride(planHandleD2Z, dim, clInStride);
  err = clfftSetPlanOutStride(planHandleD2Z, dim, clOutStride);

  /* Bake the plan */
  err = clfftBakePlan(planHandleD2Z, 1, &m_oclbase->m_command_queue, NULL, NULL);

  if (err != CL_SUCCESS) {
    DEBUG_MSG("Error creating Real-to-complex plan");
    return DKS_ERROR;
  }

  return DKS_SUCCESS;
}

int OpenCLFFT::setupFFTCR(int ndim, int N[3], double scale) {
  cl_int err;
  
  clfftDim dim;
  if (ndim == 1)
    dim = CLFFT_1D;
  else if (ndim == 2)
    dim = CLFFT_2D;
  else
    dim = CLFFT_3D;
  
  size_t clLength[3] = {(size_t)N[0], (size_t)N[1], (size_t)N[2]};

  size_t half = (size_t)N[0] / 2 + 1;
  size_t clInStride[3] = {1, half, half * N[1]};
  size_t clOutStride[3] = {1, (size_t)N[0], (size_t)N[0]*N[1]};

  /* Create 3D fft plan*/
  err = clfftCreateDefaultPlan(&planHandleZ2D, m_oclbase->m_context, dim, clLength);

  /* Set plan parameters */
  err = clfftSetPlanPrecision(planHandleZ2D, CLFFT_DOUBLE);
  err = clfftSetLayout(planHandleZ2D, CLFFT_HERMITIAN_INTERLEAVED, CLFFT_REAL);
  err = clfftSetResultLocation(planHandleZ2D, CLFFT_OUTOFPLACE);
  err = clfftSetPlanInStride(planHandleZ2D, dim, clInStride);
  err = clfftSetPlanOutStride(planHandleZ2D, dim, clOutStride);

  /* Bake the plan */
  err = clfftBakePlan(planHandleZ2D, 1, &m_oclbase->m_command_queue, NULL, NULL);

  if (err != CL_SUCCESS) {
    DEBUG_MSG("Error creating Complex-to-real plan");
    return DKS_ERROR;
  }

  return DKS_SUCCESS;
}

int OpenCLFFT::destroyFFT() {
  clfftDestroyPlan(&planHandleZ2Z);
  clfftDestroyPlan(&planHandleD2Z);
  clfftDestroyPlan(&planHandleZ2D);

  clfftTeardown();

  return DKS_SUCCESS;
}


void OpenCLFFT::printError(clfftStatus err) {

  if (err != CL_SUCCESS) {
    std::cout << "Error creating default plan " << err <<  std::endl;
    switch(err) {
    case CLFFT_BUGCHECK: 
      std::cout << "bugcheck" << std::endl; 
      break;
    case CLFFT_NOTIMPLEMENTED: 
      std::cout << "not implemented" << std::endl; 
      break;
    case CLFFT_TRANSPOSED_NOTIMPLEMENTED: 
      std::cout << "transposed not implemented" << std::endl; 
      break;
    case CLFFT_FILE_NOT_FOUND: 
      std::cout << "file not found" << std::endl; 
      break;
    case CLFFT_FILE_CREATE_FAILURE: 
      std::cout << "file create failure" << std::endl; 
      break;
    case CLFFT_VERSION_MISMATCH: 
      std::cout << "version missmatch" << std::endl; 
      break;
    case CLFFT_INVALID_PLAN: 
      std::cout << "invalid plan" << std::endl; 
      break;
    case CLFFT_DEVICE_NO_DOUBLE: 
      std::cout << "no double" << std::endl; 
      break;
    case CLFFT_DEVICE_MISMATCH: 
      std::cout << "device missmatch" << std::endl; 
      break;
    case CLFFT_ENDSTATUS: 
      std::cout << "end status" << std::endl; 
      break;
    default: 
      std::cout << "other: " << err << std::endl;
      break;
    }
  }

}

/*
void OpenCLFFT::printData3DN4(cl_double2* &data, int N) {
    
  for (int j = 0; j < N; j++) {
    for (int i = 0; i < N; i++) {
      for (int k = 0; k < N; k++) {
	double d = data[i*N*N + j*N + k].x;
	if (d > 10e-5 || d < -10e-5)
	  std::cout << d << "\t";
	else 
	  std::cout << 0 << "\t";
      }
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
    
}
*/




