#include "OpenCLGreensFunction.h"
#define GREENS_KERNEL "OpenCL/OpenCLKernels/OpenCLGreensFunction.cl"

OpenCLGreensFunction::OpenCLGreensFunction(OpenCLBase *base) {
  m_base = base;
  base_create = false;
}

OpenCLGreensFunction::OpenCLGreensFunction() {
  m_base = new OpenCLBase();
  base_create = true;
}

OpenCLGreensFunction::~OpenCLGreensFunction() {
  if (base_create)
    delete m_base;
}

int OpenCLGreensFunction::buildProgram() {
  char *kernel_file = new char[500];
  kernel_file[0] = '\0';
  strcat(kernel_file, OPENCL_KERNELS);
  strcat(kernel_file, GREENS_KERNEL);

  return m_base->ocl_loadKernel(kernel_file);
}

int OpenCLGreensFunction::greensIntegral(void *tmpgreen, int I, int J, int K, int NI, int NJ, 
					 double hr_m0, double hr_m1, double hr_m2, 
					 int streamId)
{
  int ierr = DKS_SUCCESS;

  //compile opencl program from source
  buildProgram();

  //cast the input data ptr to cl_mem
  cl_mem tmpgreen_ptr = (cl_mem)tmpgreen;
  
  //set the work item size
  size_t work_size = 128;
  size_t work_items = I * J * K;
  if (work_items % work_size > 0) 
    work_items = (work_items / work_size + 1) * work_size;

  //create kernel
  ierr = m_base->ocl_createKernel("kernelTmpgreen");

  //set kernel parameters
  m_base->ocl_setKernelArg(0, sizeof(cl_mem), &tmpgreen_ptr);
  m_base->ocl_setKernelArg(1, sizeof(double), &hr_m0);
  m_base->ocl_setKernelArg(2, sizeof(double), &hr_m1);
  m_base->ocl_setKernelArg(3, sizeof(double), &hr_m2);
  m_base->ocl_setKernelArg(4, sizeof(int), &I);
  m_base->ocl_setKernelArg(5, sizeof(int), &J);
  m_base->ocl_setKernelArg(6, sizeof(int), &K);
  
  //execute kernel
  ierr = m_base->ocl_executeKernel(1, &work_items, &work_size);

  return ierr;
}
		
int OpenCLGreensFunction::integrationGreensFunction(void *rho2_m, void *tmpgreen, int I, int J,
						    int K, int streamId)
{
  int ierr = DKS_SUCCESS;

  //compile opencl program from source
  buildProgram();

  //cast the input data ptr to cl_mem
  cl_mem rho2_ptr = (cl_mem)rho2_m;
  cl_mem tmpgreen_ptr = (cl_mem)tmpgreen;
  int NI = 2*(I - 1);
  int NJ = 2*(J - 1);

  //set the work item size
  size_t work_size = 128;
  size_t work_items = I * J * K;
  if (work_items % work_size > 0) 
    work_items = (work_items / work_size + 1) * work_size;

  //create kernel
  ierr = m_base->ocl_createKernel("kernelIntegration");

  //set kernel parameters
  m_base->ocl_setKernelArg(0, sizeof(cl_mem), &rho2_ptr);
  m_base->ocl_setKernelArg(1, sizeof(cl_mem), &tmpgreen_ptr);
  m_base->ocl_setKernelArg(2, sizeof(int), &NI);
  m_base->ocl_setKernelArg(3, sizeof(int), &NJ);
  m_base->ocl_setKernelArg(4, sizeof(int), &I);
  m_base->ocl_setKernelArg(5, sizeof(int), &J);
  m_base->ocl_setKernelArg(6, sizeof(int), &K);
  
  //execute kernel
  double zero = 0.0;
  int sizerho = 2*(I - 1) * 2*(J - 1) * 2*(K - 1);
  m_base->ocl_fillMemory(rho2_ptr, sizerho, zero, 0);
  ierr = m_base->ocl_executeKernel(1, &work_items, &work_size);

  return ierr;

}
		
 
int OpenCLGreensFunction::mirrorRhoField(void *rho2_m, int I, int J, int K, int streamId) 
{
  int ierr = DKS_SUCCESS;

  //compile opencl program from source
  buildProgram();

  //cast the input data ptr to cl_mem
  cl_mem rho2_ptr = (cl_mem)rho2_m;
  int NI = I + 1;
  int NJ = J + 1;
  int NK = K + 1;
  int I2 = 2*I;
  int J2 = 2*J;
  int K2 = 2*K;

  int rhosize = ( (I - 1) * 2 ) * ( (J - 1) * 2 ) * ( (K - 1) * 2 );

  //set the work item size
  size_t work_size = 128;
  size_t work_items = NI * NJ * NK;
  if (work_items % work_size > 0) 
    work_items = (work_items / work_size + 1) * work_size;

  //create kernel
  ierr = m_base->ocl_createKernel("kernelMirroredRhoField");

  //set kernel parameters
  m_base->ocl_setKernelArg(0, sizeof(cl_mem), &rho2_ptr);
  m_base->ocl_setKernelArg(1, sizeof(int), &I2);
  m_base->ocl_setKernelArg(2, sizeof(int), &J2);
  m_base->ocl_setKernelArg(3, sizeof(int), &K2);
  m_base->ocl_setKernelArg(4, sizeof(int), &NI);
  m_base->ocl_setKernelArg(5, sizeof(int), &NJ);
  m_base->ocl_setKernelArg(6, sizeof(int), &NK);
  m_base->ocl_setKernelArg(7, sizeof(int), &rhosize);

  //execute kernel
  ierr = m_base->ocl_executeKernel(1, &work_items, &work_size);

  return ierr;
}


int OpenCLGreensFunction::multiplyCompelxFields(void *ptr1, void *ptr2, int size, int streamId)
{
    int ierr = DKS_SUCCESS;

  //compile opencl program from source
  buildProgram();

  //cast the input data ptr to cl_mem
  cl_mem mem_ptr1 = (cl_mem) ptr1;
  cl_mem mem_ptr2 = (cl_mem) ptr2;

  //set the work item size
  size_t work_size = 128;
  size_t work_items = size;
  if (work_items % work_size > 0) 
    work_items = (work_items / work_size + 1) * work_size;

  //create kernel
  ierr = m_base->ocl_createKernel("multiplyComplexFields");

  //set kernel parameters
  m_base->ocl_setKernelArg(0, sizeof(cl_mem), &mem_ptr1);
  m_base->ocl_setKernelArg(1, sizeof(cl_mem), &mem_ptr2);
  m_base->ocl_setKernelArg(2, sizeof(int), &size);

  //execute kernel
  ierr = m_base->ocl_executeKernel(1, &work_items, &work_size);

  return ierr;

}
