#ifndef H_OPENCL_FFT
#define H_OPENCL_FFT


#include <iostream>
#include <math.h>
#include <complex>

#include "../Algorithms/FFT.h"
#include "OpenCLBase.h"

#include "clFFT.h"

/**
 * OpenCL FFT class based on BaseFFT interface.
 * Uses clFFT library to perform FFTs on AMD gpus.
 * clFFT library works also on nvida GPUs and other devices that
 * support OpenCL.
 */
class OpenCLFFT : public BaseFFT {

private:

  OpenCLBase *m_oclbase;

  clfftSetupData fftSetup;
  clfftPlanHandle planHandleZ2Z;
  clfftPlanHandle planHandleD2Z;
  clfftPlanHandle planHandleZ2D;

  /*
    Info: call fft kernels to execute FFT of the given domain,
    data - devevice memory ptr, cdim - current dim to transform, 
    ndim - totla number of dimmensions, N - size of dimension
    Return: success or error code
  */
  int ocl_callFFTKernel(cl_mem &data, int cdim, int ndim, int N, bool forward = true);
	
  /*
    Info: call ifft kernel to execute the bit reverse sort
    data - devevice memory ptr, cdim - current dim to transform, 
    ndim - totla number of dimmensions, N - size of dimension
    Return: success or error code
  */
  int ocl_callBitReverseKernel(cl_mem &data, int cdim, int ndim, int N);

  /** Get clfftStatus and print the corresponding error message.
   *  clfftStatus is returned from all clFFT library functions, print error displays the
   *  corresponding error message. If "other" is printed then error code corresponds to 
   *  OpenCL error code and not specifically to clFFT library, then OpenCL error codes should
   *  be checked to determine the reason for the error.
   */
  void printError(clfftStatus err);
  
public:

  /* constructor - currently does nothing*/
  OpenCLFFT(OpenCLBase *base) {
    m_oclbase = base;

    /* Set up fft */
    cl_int err;
    err = clfftInitSetupData(&fftSetup);
    err = clfftSetup(&fftSetup);
    
    if (err != CL_SUCCESS)
      DEBUG_MSG("Error seting up clFFT");
  }
	
  /* destructor - currently does nothing*/
  ~OpenCLFFT() { destroyFFT(); }
	
  /*
    Info: execute forward fft function with data set on device
    Return: success or error code
  */
  //int ocl_executeFFT(cl_mem &data, int ndim, int N, bool forward = true);
  int executeFFT(void *data, int ndim, int N[3], int streamId = -1, bool forward = true);
	
  /*
    Info: execute inverse fft with data set on device
    Return: success or error code
  */
  //int ocl_executeIFFT(cl_mem &data, int ndim, int N);
  int executeIFFT(void *data, int ndim, int N[3], int streamId = -1);
	
  /*
    Info: execute normalize kernel
    Return: success or error code
  */
  //int ocl_normalizeFFT(cl_mem &data, int ndim, int N);
  int normalizeFFT(void *data, int ndim, int N[3], int streamId = -1);
	
  /*
    Info: set FFT size
    Return: success or error code
  */
  int setupFFT(int ndim, int N[3]);

  int setupFFTRC(int ndim, int N[3], double scale = 1.0);

  int setupFFTCR(int ndim, int N[3], double scale = 1.0);

  int destroyFFT();
	
  int executeRCFFT(void * real_ptr, void * comp_ptr, int ndim, int N[3], 
		   int streamId = -1);
  int executeCRFFT(void * real_ptr, void * comp_ptr, int ndim, int N[3], 
		   int streamId = -1);
  int normalizeCRFFT(void *real_ptr, int ndim, int N[3], int streamId = -1) {
    return DKS_ERROR;
  }

  //void printData3DN4(cl_double2* &data, int N);

};

#endif
