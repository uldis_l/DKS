/*

Author: Uldis Locans

Info: class that holds information about the compute device

Data: 25.09.2014

*/

#define DKS_DEVICE_TYPE_GPU 1
#define DKS_DEVICE_TYPE_MIC 2
#define DKS_DEVICE_TYPE_CPU 3

class Device {

	private:
		int m_device_id;
		int m_device_type;
		char *m_device_name;
		char *m_device_vendor;
		
		bool m_sup_opencl;
		bool m_sup_cuda;
		bool m_sup_openmp;
		bool m_sup_openacc;
		
		int m_pci_bus_id;
	
	public:
	
		Device();
		~Device();
		
		

};