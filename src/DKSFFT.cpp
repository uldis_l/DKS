#include "DKSFFT.h"

DKSFFT::DKSFFT() {
  dksfft = nullptr;
}

DKSFFT::~DKSFFT() {
  delete dksfft;
}

/* setup fft plans to reuse if multiple ffts of same size are needed */
int DKSFFT::setupFFT(int ndim, int N[3]) {

  if (apiCuda()) {
    dksfft = CUDA_SAFEINIT( new CudaFFT(getCudaBase()) );
    return dksfft->setupFFT(ndim, N);
  } else if (apiOpenCL()) {
    dksfft = OPENCL_SAFEINIT_AMD( new OpenCLFFT(getOpenCLBase()) );
    int ierr1 = dksfft->setupFFT(ndim, N);
    int ierr2 = dksfft->setupFFTRC(ndim, N);
    int ierr3 = dksfft->setupFFTCR(ndim, N);
    if (ierr1 != DKS_SUCCESS || ierr2 != DKS_SUCCESS || ierr3 != DKS_SUCCESS)
      return DKS_ERROR;

    return DKS_SUCCESS;
  } else if (apiOpenMP()) {
    //micbase.mic_setupFFT(ndim, N);
    //BENI: setting up RC and CR transformations on MIC
    dksfft = MIC_SAFEINIT( new MICFFT(getMICBase()) );
    int ierr1 = dksfft->setupFFTRC(ndim, N, 1.);
    int ierr2 = dksfft->setupFFTCR(ndim, N, 1./(N[0]*N[1]*N[2]));
    if (ierr1 != DKS_SUCCESS)
      return ierr1;
    if (ierr2 != DKS_SUCCESS)
      return ierr2;
    return DKS_SUCCESS;
  }

  return DKS_ERROR;

}
//BENI:
int DKSFFT::setupFFTRC(int ndim, int N[3], double scale) {

  if (apiCuda())
    return dksfft->setupFFT(ndim, N);
  if (apiOpenCL())
    return dksfft->setupFFTRC(ndim, N);
  else if (apiOpenMP())
    return dksfft->setupFFTRC(ndim, N, scale);

  return DKS_ERROR;

}

//BENI:
int DKSFFT::setupFFTCR(int ndim, int N[3], double scale) {

  if (apiCuda())
    return dksfft->setupFFT(ndim, N);
  if (apiOpenCL())
    return dksfft->setupFFTCR(ndim, N);
  else if (apiOpenMP())
    return dksfft->setupFFTCR(ndim, N, scale);

  return DKS_ERROR;

}

/* call OpenCL FFT function for selected platform */
int DKSFFT::callFFT(void * data_ptr, int ndim, int dimsize[3], int streamId) {

  if (apiOpenCL() || apiOpenMP()) 
    return dksfft->executeFFT(data_ptr, ndim, dimsize);
  else if (apiCuda())
    return dksfft->executeFFT(data_ptr, ndim, dimsize, streamId);

  DEBUG_MSG("No implementation for selected platform");
  return DKS_ERROR;
}

/* call OpenCL IFFT function for selected platform */
int DKSFFT::callIFFT(void * data_ptr, int ndim, int dimsize[3], int streamId) {
  if (apiOpenCL() || apiOpenMP())
      return dksfft->executeIFFT(data_ptr, ndim, dimsize);
  else if (apiCuda()) 
    return dksfft->executeIFFT(data_ptr, ndim, dimsize, streamId);

  DEBUG_MSG("No implementation for selected platform");
  return DKS_ERROR;
}

/* call normalize FFT function for selected platform */
int DKSFFT::callNormalizeFFT(void * data_ptr, int ndim, int dimsize[3], int streamId) {

  if (apiOpenCL()) {
    if ( loadOpenCLKernel("OpenCL/OpenCLKernels/OpenCLFFT.cl") == DKS_SUCCESS )
      return dksfft->normalizeFFT(data_ptr, ndim, dimsize);
    else 
      return DKS_ERROR;
  } else if (apiCuda()) {
    return dksfft->normalizeFFT(data_ptr, ndim, dimsize, streamId);
  } else if (apiOpenMP()) {
    return dksfft->normalizeFFT(data_ptr, ndim, dimsize);
  }

  DEBUG_MSG("No implementation for selected platform");
  return DKS_ERROR;
}

/* call real to complex FFT */
int DKSFFT::callR2CFFT(void * real_ptr, void * comp_ptr, int ndim, int dimsize[3], int streamId) {

  if (apiCuda())
    return dksfft->executeRCFFT(real_ptr, comp_ptr, ndim, dimsize, streamId);
  else if (apiOpenCL() || apiOpenMP())
    return dksfft->executeRCFFT(real_ptr, comp_ptr, ndim, dimsize);

  DEBUG_MSG("No implementation for selected platform");
  return DKS_ERROR;
}

/* call complex to real FFT */
int DKSFFT::callC2RFFT(void * real_ptr, void * comp_ptr, int ndim, int dimsize[3], int streamId) {
  if (apiCuda())
    return dksfft->executeCRFFT(real_ptr, comp_ptr, ndim, dimsize, streamId);
  else if (apiOpenCL() || apiOpenMP())
    return dksfft->executeCRFFT(real_ptr, comp_ptr, ndim, dimsize);

  DEBUG_MSG("No implementation for selected platform");
  return DKS_ERROR;
}

/* normalize complex to real iFFT */
int DKSFFT::callNormalizeC2RFFT(void * real_ptr, int ndim, int dimsize[3], int streamId) {
  if (apiCuda())
    return dksfft->normalizeCRFFT(real_ptr, ndim, dimsize, streamId);
  else if (apiOpenCL())
    return DKS_ERROR;
  else if (apiOpenMP())
    return DKS_ERROR;

  DEBUG_MSG("No implementation for selected platform");
  return DKS_ERROR;
}


