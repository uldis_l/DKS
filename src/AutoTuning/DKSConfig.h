#ifndef DKS_CONFIG
#define DKS_CONFIG

#include <boost/property_tree/ptree.hpp>
#include <boost/optional/optional.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
//#include <boost/filesystem.hpp>
#include <string>
#include <iostream>
#include <cstdlib>
#include <exception>
#include <limits>
#include <cmath>
#include <algorithm>
#include <cctype>

#include "../DKSDefinitions.h"

namespace pt = boost::property_tree;
//namespace fs = boost::filesystem;

const std::string config_dir = "/.config/DKS";
const std::string config_file = "/autotuning.xml";

/** Class to save and load DKS autotunning configs.
 * Autotuning settings are saved and loaded from $HOME/.config/DKS/autotuning.xml.
 * Uses boost xml_parser to read and write the xml file and boost property tree to store
 * the xml content.
 * TODO: need an update boost::filesystem is disabled at the moment, no configuration file is saved
 * so the auto-tuning has no effect.
 */
class DKSConfig {

private:

  pt::ptree tree_m;
  const char *homedir_m;
  bool homeset_m;
  bool treeloaded_m;

public:

  /** Constructor, set home variable.
   * If home directory is not set config file can not be read or saved
   */
  DKSConfig();

  ~DKSConfig();

  /** Load autotuinig.xml into tree variable if this file exists */
  int loadConfigFile();
  
  /** Save autotuning.xml file */
  int saveConfigFile();

  /** Add config parameter to tree */
  int addConfigParameter(const std::string api, const std::string device,
			 const std::string name, const std::string func, 
			 int size, std::string param, int value);

  /** Get config parameter from the tree */
  int getConfigParameter(const std::string api, const std::string device,
			 const std::string name, const std::string func,
			 int size, std::string param, int &value);


};

#endif
