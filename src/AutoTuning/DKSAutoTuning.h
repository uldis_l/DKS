#ifndef DKS_AUTOTUNIG
#define DKS_AUTOTUNIG

#include <iostream>
#include <functional>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <chrono>
#include <ctime>


#include "../DKSBase.h"
#include "../Utility/DKSTimer.h"
#include "DKSSearchStates.h"

typedef std::vector<Parameter> Parameters;
typedef std::vector<State> States;

/** 
 * DKS autotuning class, allows to auto-tune the defince function.
 * Executes the defined function for auto-tuning and searches for optimal parameters to improve
 * the function execution time. The function that is auto-tuned, parameters and the ranges
 * need to be set. Includes multiple search methods, that searches the parameter space to finde 
 * the optimal solution.
 *  1) exaustive search
 *  2) line search
 *  3) hill climbimg
 *  4) simulated annealing
 */
class DKSAutoTuning {

private:

  bool evaluate_time_m;

  std::string api_name_m;
  std::string device_name_m;
  std::string function_name_m;

  std::function<int()> f_m;
  std::function<double()> fd_m;
  Parameters params_m;

  DKSBase *base_m;

  int loops_m;

  /** Update parameters from a state. */
  int setParameterValues(States states);

  /** 
   * Evaluate the function and set execution time 
   * Returns DKS_ERROR if errors occured during function execution. 
   * Returns DKS_SUCCESS if function executed as planned. 
   */
  int evaluateFunction(double &value);

public:

  /** Constructor */
  DKSAutoTuning(DKSBase *base, std::string api, std::string device, int loops = 100);

  /** Destructor. */
  ~DKSAutoTuning();

  /** 
   * Set function to auto tune.
   * Caller of setFunction is responsible to bind the correct parameters 
   * to the function with std::bind.
   */
  void setFunction(std::function<int()> f, std::string name, bool evaluate_time = true) {
    f_m = f;
    function_name_m = name;
    evaluate_time_m = evaluate_time;
  }

  /** 
   * Set function to auto tune.
   * Caller of setFunction is responsible to bind the correct parameters 
   * to the function with std::bind.
   */
  void setFunction(std::function<double()> f, std::string name, bool evaluate_time = false) {
    fd_m = f;
    function_name_m = name;
    evaluate_time_m = evaluate_time;
  }

  /** 
   * Set parameter for auto tuning.
   * Provide a pointer to a parameter that will be changed during auto-tuning
   * and a min-max value for this element
   */
  template <typename T1>
  void addParameter(T1 *value, T1 min, T1 max, T1 step, std::string name) {
    Parameter p(value, min, max, step, name);
    params_m.push_back(p);
  }

  /** Delete all added parameters */
  void clearParameters();

  /** Perform exaustive search evaluating all the parameter configurations */
  void exaustiveSearch();

  /**
   * Perform line-search auto-tuning by variying parameters one at a time.
   * After one parameter is auto-tuned the next on is varied
   */
  void lineSearch();  

  /** Perform hill climbing
   */
  void hillClimbing(int restart_loops = 1);

  /** Perfor simulated annealing to find the parameters */
  void simulatedAnnealing(double Tstart, double Tstep);

};

#endif
