#ifndef DKS_SEARCHSTATES
#define DKS_SEARCHSTATES

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <limits>

enum VALUE_TYPE { DKS_INT, DKS_DOUBLE };

/** 
 * Parameter class allows to change the searchable parameters during the auto-tuning.
 */
class Parameter {

private:
  int *ivalue;
  double *dvalue;
  VALUE_TYPE type;

public:
  double min;
  double max;
  double step;
  std::string name;

  Parameter(int *_value, int _min, int _max, int _step, std::string _name) {

    ivalue = _value;
    min = (double)_min;
    max = (double)_max;
    step = (double)_step;
    name = _name;
    type = DKS_INT;
  }

  Parameter(double *_value, double _min, double _max, double _step, std::string _name) {

    std::cout << "Double" << std::endl;

    dvalue = _value;
    min = _min;
    max = _max;
    step = _step;
    name = _name;
    type = DKS_DOUBLE;
  }

  template <typename T>
  void setValue(T v) {
    if (type == DKS_INT)
      *ivalue = (int)v;
    if (type == DKS_DOUBLE)
      *dvalue = (double)v;
  }

  double getValue() {
    switch (type) {
    case DKS_INT:
      return (double)*ivalue;
    case DKS_DOUBLE:
      return *dvalue;
    };
    return -1.0;
  }

};

/**
 * Struct to hold a auto-tuning state.
 * Holds the current value, min, max and a step to witch a state can change.
 */ 
struct State {
  double value;
  double min;
  double max;
  double step;
};

typedef std::vector<Parameter> Parameters;
typedef std::vector<State> States;

/** 
 * Used by auto-tuning search algorithms to move between parameter configurations.
 * Allows to move from one parameter stat to another, get neighboring states, 
 * move to neighboring states and save state information. Print functions are available
 * for debugging purposes, to follow how algorithm muves between sates.
 */
class DKSSearchStates {

private:

  States current_state_m;
  States neighbour_state_m;

  States best_state_m;
  double best_time_m;

  std::vector< std::vector<double> > neighbours_m;
  int next_neighbour_m;

public:

  /** Constructor alwats takes params array as variable.
   *  Params array is needed to know how many params will be searched and what are thou bounds
   *  of each parameter.
   */
  DKSSearchStates(Parameters params);

  ~DKSSearchStates();

  /** Set current state using parameter vector */
  void setCurrentState(Parameters current_state);

  /** set current state using the state vector */
  void setCurrentState(States current_state);

  /** init random current state */
  void initCurrentState();

  /** get current state */
  States getCurrentState();

  /** get next neighbour state.
   *  if there are no next neighbore stay at the curretn neighbour
   */
  States getNextNeighbour();

  /** get random neighbour state */
  States getRandomNeighbour();

  /** calculate all the neighbour states */
  void getNeighbours(int dist = 1);

  /** Chech if there are more neighbours to evaluate
   *  Return true if more neighbors exist, false if we are at the last neighbour
   */
  bool nextNeighbourExists();

  /** move to next neighbour.
   *  set the current state as the next neighbour, 
   *  calculate the neighbours of the new current state.
   */
  void moveToNeighbour();
  
  /** Save the current state and the evaluation time of the current state.
   *  If evaluation time of the current state is better than the evaluation time of the 
   *  best state, save the current state as best.
   */
  void saveCurrentState(double current_time);


  //Print functions - mostly usefull for debugging purposes, or for benchmark runs to print the
  //status of the search

  /** Print current state.
   *  cout the current state. Mostly used for debuging purposes
   */
  void printCurrentState(double time = 0.0);

  /** Print current neighbour info */
  void printNeighbour(double time = 0.0);

  /** Print info.
   *  Print the whole info about the search: current state, current neighbour, total neighbors
   */
  void printInfo();

  /** Print the best saved state */
  void printBest();

};

#endif
