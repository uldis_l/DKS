#include "DKSConfig.h"

DKSConfig::DKSConfig() {

  //get home directory
  homeset_m = true;
  if ((homedir_m = getenv("HOME")) == NULL)
    homeset_m = false;

  loadConfigFile();

}

DKSConfig::~DKSConfig() {
  //delete tree_m;
  
  saveConfigFile();
}


int DKSConfig::loadConfigFile() {
  
  int ierr = DKS_ERROR;
  /*
  if (homeset_m) {
    //check if $HOME/.config/DKS exists
    std::string filename = homedir_m + config_dir + config_file;
    std::cout << "Check for: " << filename << std::endl;
    if (fs::exists(filename)) {
      try {
	pt::read_xml(filename, tree_m);
	treeloaded_m = true;
	ierr = DKS_SUCCESS;
      } catch (std::exception &e) {
	DEBUG_MSG("Error loading autotuning file!");
	treeloaded_m = false;
	ierr = DKS_ERROR;
      }
    }
  }
  */
  return ierr;
}


int DKSConfig::saveConfigFile() {

  int ierr = DKS_ERROR;
  /*
  std::string savedir = homedir_m + config_dir;
  std::string savefile = homedir_m + config_dir + config_file;

  std::cout << savedir << std::endl;
  std::cout << savefile << std::endl;

  if (homeset_m) {
    //check if $HOME/.config/DKS directory exists, if not create
    bool homecreated = false;
    fs::path p (savedir);
    if (!fs::is_directory(p))
      homecreated = fs::create_directory(p);

    try {
      if (homecreated) {
	pt::write_xml(savefile, tree_m);
	ierr =  DKS_SUCCESS;
      }
    } catch(std::exception &e) {
      ierr = DKS_ERROR;
    }

  }
  */
  return ierr;
}


int DKSConfig::addConfigParameter(const std::string api, const std::string device,
				  const std::string name, const std::string func, 
				  int size, std::string param, int value) {


  //keys to acces data in the tree
  std::string device_name = name;
  device_name.erase(std::remove_if(device_name.begin(), device_name.end(), ::isspace), device_name.end());
  std::string key = "DKS.autotune." + api + "." + device + "." + device_name + "." + func;
  std::string parameter = key + ".parameter";
  std::string attr_size = "<xmlattr>.size";
  std::string attr_param = "<xmlattr>." + param;

  //tmp node where new attributes are cteated in case the attribute doesn't exist in the tree
  pt::ptree *tmp;
  bool newNode = true;

  //loop trough all the items in the node and see if new param needs to be created
  //or old one updated
  boost::optional< pt::ptree& > child = tree_m.get_child_optional(key);
  if (child) {
    BOOST_FOREACH(pt::ptree::value_type &v, tree_m.get_child(key)) {
      int oldsize = v.second.get<int>(attr_size,-1);
    
      //if param with the same size already exists in the tree save pointer to this
      if (size == oldsize) {
	tmp = &v.second;
	newNode = false;
      }
    }
  }
  
  //if parameter doesnt exist with this size, create a new parameter
  if (newNode) {
    tmp = new pt::ptree();
    tmp->add(attr_size, size);
    tmp->add(attr_param, value);
    tree_m.add_child(parameter, *tmp);
  } else {
    //if parameter exists update the parameter value
    tmp->put(attr_param, value);
  }

  return DKS_SUCCESS;
}

int DKSConfig::getConfigParameter(const std::string api, const std::string device,
				  const std::string name, const std::string func, 
				  int size, std::string param, int &value) {

  //get the value of the tree, default to -1 if value doesn't exist
  int ierr = DKS_SUCCESS;

  //define key and attribute values to find parameters in the tree
  std::string device_name = name;
  device_name.erase(std::remove_if(device_name.begin(), device_name.end(), ::isspace), device_name.end());
  std::string key = "DKS.autotune." + api + "." + device + "." + device_name + "." + func;
  std::string attr_size = "<xmlattr>.size";
  std::string attr_param = "<xmlattr>." + param;
  
  float maxDist = std::numeric_limits<float>::max();

  //check if the parameters exist
  boost::optional< pt::ptree& > child = tree_m.get_child_optional(key);
  if (child) {
    //loop trough parameters and get the one that is closes to the size specified
    BOOST_FOREACH(pt::ptree::value_type &v, tree_m.get_child(key)) {
      int param_size = v.second.get<int>(attr_size,-1); //get parameter size
      if (param_size > 0) { // if param_size is -1 param is not defined correctly and not usable
	float dist = abs(param_size - size);
	if (dist < maxDist) {
	  value = v.second.get<int>(attr_param,-1);
	  maxDist = dist;
	}
      }
    }
  } else {
    value = -1;
    ierr = DKS_ERROR;
  }
  
  return ierr;
}



