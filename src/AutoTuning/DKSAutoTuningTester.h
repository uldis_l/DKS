#ifndef DKS_TESTAUTOTUNING
#define DKS_TESTAUTOTUNING

#include <iostream>
#include <cmath>

/** Tester class for auto-tuning search algorithms. */
class DKSAutoTuningTester {

  friend class DKSBaseMuSR;

private:

  double x;
  double y;

public:

  DKSAutoTuningTester() {
    x = 0.0;
    y = 0.0;
  }

  ~DKSAutoTuningTester();

  double peaksZ() {

    double z = 3 * pow(1-x,2) * exp(-pow(x,2) - pow(y+1,2)) - 10 * (x/5 - pow(x,3) - pow(y,5)) * exp(-pow(x,2) - pow(y,2)) - (1.0/3.0) * exp( - pow(x+1,2) - pow(y,2));
    return z;
  }

};

#endif
