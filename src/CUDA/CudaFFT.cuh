#ifndef H_CUDA_FFT
#define H_CUDA_FFT

#include <iostream>
#include <math.h>
#include <cuda_runtime.h>
#include <cufft.h>
#include "cublas_v2.h"

#include "../Algorithms/FFT.h"
#include "CudaBase.cuh"

/**
 * Cuda FFT class based on BaseFFT interface.
 * Uses cuFFT library to perform FFTs on nvidias GPUs.
 */
class CudaFFT : public BaseFFT {

private:

  bool base_create;
  CudaBase *m_base;
	
  cufftHandle defaultPlanZ2Z;
  cufftHandle defaultPlanD2Z;
  cufftHandle defaultPlanZ2D;
  cublasHandle_t defaultCublasFFT;

public:
	
  /** Constructor with CudaBase as argument */
  CudaFFT(CudaBase *base);

  /** constructor */
  CudaFFT();
		
  /** destructor */
  ~CudaFFT();
		
  /**
   * Init cufftPlans witch can be reused for all FFTs of the same size and type
   * Return: success or error code
   */
  int setupFFT(int ndim, int N[3]);
  int setupFFTRC(int ndim, int N[3], double scale = 1.0) { return DKS_SUCCESS; }
  int setupFFTCR(int ndim, int N[3], double scale = 1.0) { return DKS_SUCCESS; }

  /**
   * Destroy default FFT plans
   * Return: success or error code
   */
  int destroyFFT();

  int executeFFT(void * mem_ptr, int ndim, int N[3], int streamId = -1, bool forward = true);
		
  int executeIFFT(void * mem_ptr, int ndim, int N[3], int streamId = -1);
		
  int normalizeFFT(void * mem_ptr, int ndim, int N[3], int streamId = -1);
		
  int executeRCFFT(void * real_ptr, void * comp_ptr, int ndim, int N[3], int streamId = -1);
		
  int executeCRFFT(void * real_ptr, void * comp_ptr, int ndim, int N[3], int streamId = -1);

  int normalizeCRFFT(void *real_ptr, int ndim, int N[3], int streamId = -1);

};

#endif
