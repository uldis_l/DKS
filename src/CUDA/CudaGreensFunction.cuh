#ifndef H_CUDA_GREENSFUNCTION
#define H_CUDA_GREENSFUNCTION

#include <iostream>
#include <cmath>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuComplex.h>
#include "cublas_v2.h"

#include "../Algorithms/GreensFunction.h"
#include "CudaBase.cuh"

/** CUDA implementation of GreensFunction calculation for OPALs Poisson Solver. */
class CudaGreensFunction : public GreensFunction{

private:
	
  bool base_create;
  CudaBase *m_base;

public:
	
  /** Constructor with CudaBase argument */
  CudaGreensFunction(CudaBase *base);

  /* constructor */
  CudaGreensFunction();
		
  /* destructor */
  ~CudaGreensFunction();
		
  /**
    Info: calc itegral on device memory (taken from OPAL src code)
    Return: success or error code
  */
  int greensIntegral(void *tmpgreen, int I, int J, int K, int NI, int NJ, 
		       double hr_m0, double hr_m1, double hr_m2, 
		       int streamId = -1);
		
  /**
    Info: integration of rho2_m field (taken from OPAL src code)
    Return: success or error code
  */
  int integrationGreensFunction(void *rho2_m, void *tmpgreen, int I, int J, int K,
				  int streamId = -1);
		
  /**
    Info: mirror rho field (taken from OPAL src code)
    Return: succes or error code
  */
  int mirrorRhoField(void *rho2_m, int I, int J, int K, int streamId = -1);

  /**
    Info: multiply complex fields already on the GPU memory, result will be put in ptr1
    Return: success or error code
  */
  int multiplyCompelxFields(void *ptr1, void *ptr2, int size, int streamId = -1);


};

#endif
