#include "CudaImageReconstruction.cuh"

//x_edge, y_edge, z_edge and matrix_distance_factor need to be set as const for the run
//voxel_x, voxel_y and voxel_z also need to be set as const for the run
__device__ float d_x_edge = 30.8;
__device__ float d_y_edge = 30.8;
__device__ float d_z_edge = 16.8;

__device__ float d_matrix_distance_factor = 1.2;

__device__ int d_voxel_x = 90;
__device__ int d_voxel_y = 90;
__device__ int d_voxel_z = 50;

__device__ float d_voxel_size = 0.7;


//phantom_diameter needs to be defined, atten_per_mm as well
__device__ float d_phantom_diameter = 51;
__device__ float d_atten_per_mm = 0.0095;
__device__ float d_ring_diameter = 138;
__device__ float d_minimum_CrystalDistance_InOneRing = 123.489;


__device__ float d_x_edge1 = 29.26;
__device__ float d_y_edge1 = 29.26;
__device__ float d_z_edge1 = 15.96;
__device__ float d_z_edge2 = 14.28;
__device__ float d_minimum_CrystalDistance_InOneRing1 = 127.681;


__device__ inline float distance(VoxelPosition &a, VoxelPosition &b) {
  float dist_x = pow(a.x - b.x, 2);
  float dist_y = pow(a.y - b.y, 2);
  float dist_z = pow(a.z - b.z, 2);
  return sqrt(dist_x + dist_y + dist_z);
}

__global__ void kernelCalculateSource(float *image_space, VoxelPosition *image_position,
				      VoxelPosition *source_position, float *average, 
				      float *stdev, float diameter, int total_voxels,
				      int total_sources, int start)
{

  volatile int tid = threadIdx.x;
  volatile int idx = blockIdx.x * blockDim.x + tid;
  volatile int voxel_id = idx + start;

  if (voxel_id < total_voxels && idx < total_sources) {
    //read source position
    VoxelPosition source = source_position[voxel_id];

    int count = 0;
    float sum = 0;
    float sqsum = 0;
    
    int sx = floor( ((source.x - diameter) / d_voxel_size) + ((d_voxel_x - 1.0) / 2.0) );
    sx = (sx < 0) ? 0 : sx;
    sx = (sx > d_voxel_x - 1) ? d_voxel_x : sx;

    int sy = floor( ((source.y - diameter) / d_voxel_size) + ((d_voxel_y - 1.0) / 2.0) );
    sy = (sy < 0) ? 0 : sy;
    sy = (sy > d_voxel_y - 1) ? d_voxel_y : sy;

    int sz = floor( ((source.z - diameter) / d_voxel_size) + ((d_voxel_z - 1.0) / 2.0) );
    sz = (sz < 0) ? 0 : sz;
    sz = (sz > d_voxel_z - 1) ? d_voxel_z : sz;

    int ex = floor( ((source.x + diameter) / d_voxel_size) + ((d_voxel_x - 1.0) / 2.0) );
    ex = (ex < 0) ? 0 : ex;
    ex = (ex > d_voxel_x - 1) ? d_voxel_x : ex;

    int ey = floor( ((source.y + diameter) / d_voxel_size) + ((d_voxel_y - 1.0) / 2.0) );
    ey = (ey < 0) ? 0 : ey;
    ey = (ey > d_voxel_y - 1) ? d_voxel_y : ey;

    int ez = floor( ((source.z + diameter) / d_voxel_size) + ((d_voxel_z - 1.0) / 2.0) );
    ez = (ez < 0) ? 0 : ez;
    ez = (ez > d_voxel_z - 1) ? d_voxel_z : ez;
    
    VoxelPosition voxel;
    for (int z = sz; z < ez; z++) {
      voxel.z = (z-(d_voxel_z - 1.0) / 2.0) * d_voxel_size;
      for (int y = sy; y < ey; y++) {
	voxel.y = (y-(d_voxel_y - 1.0) / 2.0) * d_voxel_size;
	for (int x = sx; x < ex; x++) {
	  voxel.x = (x-(d_voxel_x - 1.0) / 2.0) * d_voxel_size;

	  float dist = distance(voxel, source);
	  
	  if (dist < diameter * 0.5 ) {
	    //read voxel value
	    int i = z * d_voxel_y * d_voxel_x + y * d_voxel_x + x;
	    float v = image_space[i];
	    sum += v;
	    sqsum += v*v;
	    count++;
	  }
	}
      }
    }

    float avg = sum / count;
    average[idx] = avg;
    stdev[idx] = sqrt( (sqsum + count * avg * avg - 2 * avg * sum) / count / (count - 1) );
  }
}

__global__ void kernelCalculateBackground(float *image_space, VoxelPosition *image_position,
					  VoxelPosition *source_position, float *average, 
					  float *stdev, float diameter, int total_voxels,
					  int total_sources, int start)
{

  volatile int tid = threadIdx.x;
  volatile int idx = blockIdx.x * blockDim.x + tid;
  volatile int voxel_id = idx + start;

  if (voxel_id < total_voxels && idx < total_sources) {
    //read source position
    VoxelPosition source = source_position[voxel_id];

    int count = 0;
    float sum = 0;
    float sqsum = 0;

    int sx = floor( ((source.x - (diameter + 1.0)) / d_voxel_size) + ((d_voxel_x - 1.0) / 2.0) );
    sx = (sx < 0) ? 0 : sx;
    sx = (sx > d_voxel_x - 1) ? d_voxel_x : sx;

    int sy = floor( ((source.y - (diameter + 1.0)) / d_voxel_size) + ((d_voxel_y - 1.0) / 2.0) );
    sy = (sy < 0) ? 0 : sy;
    sy = (sy > d_voxel_y - 1) ? d_voxel_y : sy;

    int sz = floor( ((source.z - (diameter + 1.0)) / d_voxel_size) + ((d_voxel_z - 1.0) / 2.0) );
    sz = (sz < 0) ? 0 : sz;
    sz = (sz > d_voxel_z - 1) ? d_voxel_z : sz;

    int ex = floor( ((source.x + (diameter + 1.0)) / d_voxel_size) + ((d_voxel_x - 1.0) / 2.0) );
    ex = (ex < 0) ? 0 : ex;
    ex = (ex > d_voxel_x - 1) ? d_voxel_x : ex;

    int ey = floor( ((source.y + (diameter + 1.0)) / d_voxel_size) + ((d_voxel_y - 1.0) / 2.0) );
    ey = (ey < 0) ? 0 : ey;
    ey = (ey > d_voxel_y - 1) ? d_voxel_y : ey;

    int ez = floor( ((source.z + (diameter + 1.0)) / d_voxel_size) + ((d_voxel_z - 1.0) / 2.0) );
    ez = (ez < 0) ? 0 : ez;
    ez = (ez > d_voxel_z - 1) ? d_voxel_z : ez;

    VoxelPosition voxel;
    for (int z = sz; z < ez; z++) {
      voxel.z = (z-(d_voxel_z - 1.0) / 2.0) * d_voxel_size;
      for (int y = sy; y < ey; y++) {
	voxel.y = (y-(d_voxel_y - 1.0) / 2.0) * d_voxel_size;
	for (int x = sx; x < ex; x++) {
	  voxel.x = (x-(d_voxel_x - 1.0) / 2.0) * d_voxel_size;

	  float dist = distance(voxel, source);
	  
	  //if ( dist > diameter * 0.5 && dist < (diameter * 0.5 + 1) ) {
	  if ( dist > diameter * 0.5 && dist < (diameter) ) {
	    //read voxel value
	    int i = z * d_voxel_y * d_voxel_x + y * d_voxel_x + x;
	    float v = image_space[i];
	    sum += v;
	    sqsum += v*v;
	    count++;
	  }
	}
      }
    }

    float avg = sum / count;
    average[idx] = avg;
    stdev[idx] = sqrt( (sqsum + count * avg * avg - 2 * avg * sum) / count / (count - 1) );

  }
}


__global__ void kernelCalculateSources(float *image_space, VoxelPosition *image_position,
				       VoxelPosition *source_position, float *average, 
				       float *stdev, float *diameter, int total_voxels,
				       int total_sources, int start)
{

  volatile int tid = threadIdx.x;
  volatile int idx = blockIdx.x * blockDim.x + tid;
  volatile int voxel_id = idx + start;

  if (voxel_id < total_voxels && idx < total_sources) {
    //read source position
    VoxelPosition source = source_position[voxel_id];
    float diam = diameter[voxel_id];

    int count = 0;
    float sum = 0;
    float sqsum = 0;

    int sx = floor( ((source.x - diam) / d_voxel_size) + ((d_voxel_x - 1.0) / 2.0) );
    sx = (sx < 0) ? 0 : sx;
    sx = (sx > d_voxel_x - 1) ? d_voxel_x : sx;

    int sy = floor( ((source.y - diam) / d_voxel_size) + ((d_voxel_y - 1.0) / 2.0) );
    sy = (sy < 0) ? 0 : sy;
    sy = (sy > d_voxel_y - 1) ? d_voxel_y : sy;

    int sz = floor( ((source.z - diam) / d_voxel_size) + ((d_voxel_z - 1.0) / 2.0) );
    sz = (sz < 0) ? 0 : sz;
    sz = (sz > d_voxel_z - 1) ? d_voxel_z : sz;

    int ex = floor( ((source.x + diam) / d_voxel_size) + ((d_voxel_x - 1.0) / 2.0) );
    ex = (ex < 0) ? 0 : ex;
    ex = (ex > d_voxel_x - 1) ? d_voxel_x : ex;

    int ey = floor( ((source.y + diam) / d_voxel_size) + ((d_voxel_y - 1.0) / 2.0) );
    ey = (ey < 0) ? 0 : ey;
    ey = (ey > d_voxel_y - 1) ? d_voxel_y : ey;

    int ez = floor( ((source.z + diam) / d_voxel_size) + ((d_voxel_z - 1.0) / 2.0) );
    ez = (ez < 0) ? 0 : ez;
    ez = (ez > d_voxel_z - 1) ? d_voxel_z : ez;
    
    VoxelPosition voxel;
    for (int z = sz; z < ez; z++) {
      voxel.z = (z-(d_voxel_z - 1.0) / 2.0) * d_voxel_size;
      for (int y = sy; y < ey; y++) {
	voxel.y = (y-(d_voxel_y - 1.0) / 2.0) * d_voxel_size;
	for (int x = sx; x < ex; x++) {
	  voxel.x = (x-(d_voxel_x - 1.0) / 2.0) * d_voxel_size;

	  float dist = distance(voxel, source);
	  
	  if (dist < diam * 0.5 ) {
	    //read voxel value
	    int i = z * d_voxel_y * d_voxel_x + y * d_voxel_x + x;
	    float v = image_space[i];
	    sum += v;
	    sqsum += v*v;
	    count++;
	  }
	}
      }
    }

    float avg = sum / count;
    average[idx] = avg;
    stdev[idx] = sqrt( (sqsum + count * avg * avg - 2 * avg * sum) / count / (count - 1) );

  }
}

__global__ void kernelCalculateBackgrounds(float *image_space, VoxelPosition *image_position,
					   VoxelPosition *source_position, float *average, 
					   float *stdev, float *diameter, int total_voxels,
					   int total_sources, int start)
{

  volatile int tid = threadIdx.x;
  volatile int idx = blockIdx.x * blockDim.x + tid;
  volatile int voxel_id = idx + start;

  if (voxel_id < total_voxels && idx < total_sources) {
    //read source position
    VoxelPosition source = source_position[voxel_id];
    float diam = diameter[voxel_id];

    int count = 0;
    float sum = 0;
    float sqsum = 0;

    int sx = floor( ((source.x - (diam + 1.0)) / d_voxel_size) + ((d_voxel_x - 1.0) / 2.0) );
    sx = (sx < 0) ? 0 : sx;
    sx = (sx > d_voxel_x - 1) ? d_voxel_x : sx;

    int sy = floor( ((source.y - (diam + 1.0)) / d_voxel_size) + ((d_voxel_y - 1.0) / 2.0) );
    sy = (sy < 0) ? 0 : sy;
    sy = (sy > d_voxel_y - 1) ? d_voxel_y : sy;

    int sz = floor( ((source.z - (diam + 1.0)) / d_voxel_size) + ((d_voxel_z - 1.0) / 2.0) );
    sz = (sz < 0) ? 0 : sz;
    sz = (sz > d_voxel_z - 1) ? d_voxel_z : sz;

    int ex = floor( ((source.x + (diam + 1.0)) / d_voxel_size) + ((d_voxel_x - 1.0) / 2.0) );
    ex = (ex < 0) ? 0 : ex;
    ex = (ex > d_voxel_x - 1) ? d_voxel_x : ex;

    int ey = floor( ((source.y + (diam + 1.0)) / d_voxel_size) + ((d_voxel_y - 1.0) / 2.0) );
    ey = (ey < 0) ? 0 : ey;
    ey = (ey > d_voxel_y - 1) ? d_voxel_y : ey;

    int ez = floor( ((source.z + (diam + 1.0)) / d_voxel_size) + ((d_voxel_z - 1.0) / 2.0) );
    ez = (ez < 0) ? 0 : ez;
    ez = (ez > d_voxel_z - 1) ? d_voxel_z : ez;

    VoxelPosition voxel;
    for (int z = sz; z < ez; z++) {
      voxel.z = (z-(d_voxel_z - 1.0) / 2.0) * d_voxel_size;
      for (int y = sy; y < ey; y++) {
	voxel.y = (y-(d_voxel_y - 1.0) / 2.0) * d_voxel_size;
	for (int x = sx; x < ex; x++) {
	  voxel.x = (x-(d_voxel_x - 1.0) / 2.0) * d_voxel_size;
	  
	  float dist = distance(voxel, source);
	  
	  //if ( dist > diam * 0.5 && dist < (diam * 0.5 + 1) ) {
	  if ( dist > diam * 0.5 && dist < diam ) {
	    //read voxel value
	    int i = z * d_voxel_y * d_voxel_x + y * d_voxel_x + x;
	    float v = image_space[i];
	    sum += v;
	    sqsum += v*v;
	    count++;
	  }
	}
      }
    }

    float avg = sum / count;
    average[idx] = avg;
    stdev[idx] = sqrt( (sqsum + count * avg * avg - 2 * avg * sum) / count / (count - 1) );

  }
}

__device__ void localRaytracingX(float *recon, VoxelPosition *image_position, 
				 float &atten_factor, float &slope_y, float &slope_z, 
				 float &a_x, float &a_y, float &a_z)
{

  for (int x = 0; x < d_voxel_x; x++) {
    float lor_x = image_position[x].x;
    float lor_y =  slope_y * ( lor_x - a_x ) + a_y;
    float lor_z =  slope_z * ( lor_x - a_x ) + a_z;

    if ( pow(lor_x / d_x_edge,2) + pow( lor_y/d_y_edge, 2) < 1.0 && abs(lor_z) < d_z_edge ) {

      int y = floor( (lor_y+d_y_edge) / d_voxel_size);
      int z = floor( (lor_z+d_z_edge) / d_voxel_size);
   
      int voxel_id = z * d_voxel_y * d_voxel_x + y * d_voxel_x + x;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_y-image_position[voxel_id].y,2) + 
					  pow(lor_z-image_position[voxel_id].z,2) ) 
				    ) * atten_factor);

      voxel_id = z * d_voxel_y * d_voxel_x + (y+1) * d_voxel_x + x;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_y-image_position[voxel_id].y,2) + 
					  pow(lor_z-image_position[voxel_id].z,2) ) 
				    ) * atten_factor);

      voxel_id = (z+1) * d_voxel_y * d_voxel_x + y * d_voxel_x + x;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_y-image_position[voxel_id].y,2) + 
					  pow(lor_z-image_position[voxel_id].z,2) ) 
				    ) * atten_factor);

      voxel_id = (z+1) * d_voxel_y * d_voxel_x + (y+1) * d_voxel_x + x;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_y-image_position[voxel_id].y,2) + 
					  pow(lor_z-image_position[voxel_id].z,2) ) 
				    ) * atten_factor);
    }
  }

}
				  

__device__ void localRaytracingY(float *recon, VoxelPosition *image_position, 
				 float &atten_factor, float &slope_x, float &slope_z, 
				 float &a_x, float &a_y, float &a_z)
{

  for (int y=0;y<d_voxel_y;y++) {
    int voxel = y * d_voxel_x;
    float lor_y = image_position[voxel].y;
    float lor_x =  slope_x * ( lor_y - a_y ) + a_x;
    float lor_z =  slope_z * ( lor_y - a_y ) + a_z;

    if ( pow(lor_x/d_x_edge,2)+pow(lor_y/d_y_edge,2)<1.0 && abs(lor_z)<d_z_edge ) {

      int x = floor((lor_x+d_x_edge)/d_voxel_size);
      int z = floor((lor_z+d_z_edge)/d_voxel_size);
	
      int voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_x-image_position[voxel_id].x,2) + 
					  pow(lor_z-image_position[voxel_id].z,2) ) 
				    ) * atten_factor);
      
      voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_x-image_position[voxel_id].x,2) + 
					  pow(lor_z-image_position[voxel_id].z,2) )
				    ) * atten_factor);

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_x-image_position[voxel_id].x,2) + 
					  pow(lor_z-image_position[voxel_id].z,2) )
				    ) * atten_factor);

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_x-image_position[voxel_id].x,2) + 
					  pow(lor_z-image_position[voxel_id].z,2) ) 
				    ) * atten_factor);
    }
  }

}

__device__ void localRaytracingZ(float *recon, VoxelPosition *image_position, 
				 float &atten_factor, float &slope_x, float &slope_y, 
				 float &a_x, float a_y, float &a_z)
{

  for (int z=0; z<d_voxel_z; z++) {
    int voxel = z * d_voxel_y * d_voxel_x;
    float lor_z = image_position[voxel].z;
    float lor_x =  slope_x * ( lor_z - a_z ) + a_x;
    float lor_y =  slope_y * ( lor_z - a_z ) + a_y;

    if ( pow(lor_x/d_x_edge,2)+pow(lor_y/d_y_edge,2)<1.0 && abs(lor_z)<d_z_edge ) {

      int x = floor((lor_x+d_x_edge)/d_voxel_size);
      int y = floor((lor_y+d_y_edge)/d_voxel_size);

      int voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_x-image_position[voxel_id].x,2) + 
					  pow(lor_y-image_position[voxel_id].y,2) )
				    ) * atten_factor);

      voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_x-image_position[voxel_id].x,2) + 
					  pow(lor_y-image_position[voxel_id].y,2) )
				    ) * atten_factor);
      
      voxel_id = z*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_x-image_position[voxel_id].x,2) + 
					  pow(lor_y-image_position[voxel_id].y,2) ) 
				    ) * atten_factor);

      voxel_id = z*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x+1;
      atomicAdd(&recon[voxel_id], ( d_matrix_distance_factor - 
				    sqrt( pow(lor_x-image_position[voxel_id].x,2) + 
					  pow(lor_y-image_position[voxel_id].y,2) )
				    ) * atten_factor);
    }
  }

}

__device__ float atten_factor_calcu(float a_x, float a_y, float a_z, 
				    float b_x, float b_y, float b_z) 
{

  float distance_lor_xy = sqrt(pow( (a_x - b_x),2 ) + pow( (a_y - b_y),2 ));
  float distance_tocenter = pow( d_phantom_diameter/2,2 ) + pow( (a_x-b_x)/2,2 ) 
    + pow( (a_y - b_y)/2,2 ) - pow( d_ring_diameter/2,2 );

  float distance_xy;
  if (distance_tocenter>0.001) {
    distance_xy = 2.0 * sqrt( distance_tocenter ) ;
  }
  else
    distance_xy = 0.0;

  float distance_z = abs( a_z - b_z ) * distance_xy / distance_lor_xy;
  float distance = sqrt( pow(distance_xy,2) + pow(distance_z,2) );

  return exp(-distance*d_atten_per_mm);

}

__global__ void kernelNormalization(float *recon, VoxelPosition *image_position, 
				    VoxelPosition *det_position, int total_det)
{

  int tidx = threadIdx.x;
  int tidy = threadIdx.y;

  int detA = blockIdx.x * blockDim.x + tidx;
  int detB = blockIdx.y * blockDim.y + tidy;

  if (detA != detB && detA < total_det && detB < total_det) {

    VoxelPosition pA = det_position[detA];
    VoxelPosition pB = det_position[detB];

    float distance_x = abs( pA.x - pB.x);
    float distance_y = abs( pA.y - pB.y);
    float distance_z = abs( pA.z - pB.z);

    if( sqrt(pow(distance_x,2) + pow(distance_y,2)) > d_minimum_CrystalDistance_InOneRing) {
      float atten_factor;
      atten_factor = atten_factor_calcu(pA.x,pA.y,pA.z,pB.x,pB.y,pB.z);

      if (distance_x > distance_y && distance_x > distance_z) {

	float slope_y = ( pB.y - pA.y ) / ( pB.x - pA.x );
	float slope_z = ( pB.z - pA.z ) / ( pB.x - pA.x );

	localRaytracingX(recon, image_position, atten_factor, slope_y, slope_z, pA.x, pA.y, pA.z);
			  
      }
      else if (distance_y > distance_z) {

	float slope_x = ( pB.x - pA.x ) / ( pB.y - pA.y );
	float slope_z = ( pB.z - pA.z ) / ( pB.y - pA.y );	

	localRaytracingY(recon, image_position, atten_factor, slope_x, slope_z, pA.x, pA.y, pA.z);
      }
      else {

	float slope_x = ( pB.x - pA.x ) / ( pB.z - pA.z );
	float slope_y = ( pB.y - pA.y ) / ( pB.z - pA.z );	

	localRaytracingZ(recon, image_position, atten_factor, slope_x, slope_y, pA.x, pA.y, pA.z);
      } 

    }
  }
}

__device__ float localRaytracingForwardX(float*recon, VoxelPosition &pos,
					 float &a_x, float &a_y, float &a_z, 
					 float &b_x, float &b_y, float &b_z)
{
  
  float result = 0.000001;
  float slope_y = ( b_y - a_y ) / ( b_x - a_x);
  float slope_z = ( b_z - a_z ) / ( b_x - a_x);

  for (int x=0; x<d_voxel_x; x++) {
    float lor_x = pos.x + x * d_voxel_size;
    float lor_y =  slope_y * ( lor_x - a_x ) + a_y;
    float lor_z =  slope_z * ( lor_x - a_x ) + a_z;

    if ( pow(lor_x/d_x_edge1, 2) + pow(lor_y/d_y_edge1, 2) < 1.0 && abs(lor_z) < d_z_edge1 ) {

      int y = floor((lor_y+d_y_edge)/d_voxel_size);
      int z = floor((lor_z+d_z_edge)/d_voxel_size);

      int voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      result += (d_matrix_distance_factor - 
		 sqrt( pow(lor_y-pos.y-y*d_voxel_size,2) + 
		       pow(lor_z-pos.z-z*d_voxel_size,2) ) 
		 ) * recon[voxel_id];

      voxel_id = z*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_y-pos.y-(y+1)*d_voxel_size,2) + 
			pow(lor_z-pos.z-z*d_voxel_size,2) ) 
		  ) * recon[voxel_id];

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_y-pos.y-y*d_voxel_size,2) + 
			pow(lor_z-pos.z-(z+1)*d_voxel_size,2) ) 
		  ) * recon[voxel_id];

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_y-pos.y-(y+1)*d_voxel_size,2) + 
			pow(lor_z-pos.z-(z+1)*d_voxel_size,2) ) 
		  ) * recon[voxel_id];
    }
  }

  return result;
}

__device__ float localRaytracingForwardY(float*recon, VoxelPosition &pos,
					 float &a_x, float &a_y, float &a_z, 
					 float &b_x, float &b_y, float &b_z)
{

  float result = 0.000001;
  float slope_x = ( b_x - a_x ) / ( b_y - a_y);
  float slope_z = ( b_z - a_z ) / ( b_y - a_y);

  for (int y=0;y<d_voxel_y;y++) {
    float lor_y = pos.y + y * d_voxel_size;
    float lor_x =  slope_x * ( lor_y - a_y ) + a_x;
    float lor_z =  slope_z * ( lor_y - a_y ) + a_z;
    
    if ( pow(lor_x/d_x_edge1,2)+pow(lor_y/d_y_edge1,2)<1.0 && abs(lor_z)<d_z_edge1 ) {
      int x = floor((lor_x+d_x_edge)/d_voxel_size);
      int z = floor((lor_z+d_z_edge)/d_voxel_size);

      int voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_x-pos.x-x*d_voxel_size,2) + 
			pow(lor_z-pos.z-z*d_voxel_size,2) )
		  ) * recon[voxel_id];

      voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_x-pos.x-(x+1)*d_voxel_size,2) + 
			pow(lor_z-pos.z-z*d_voxel_size,2) )
		  ) * recon[voxel_id];
      
      voxel_id = (z+1)*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_x-pos.x-x*d_voxel_size,2) + 
			pow(lor_z-pos.z-(z+1)*d_voxel_size,2) )
		  ) * recon[voxel_id];

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_x-pos.x-(x+1)*d_voxel_size,2) + 
			pow(lor_z-pos.z-(z+1)*d_voxel_size,2) )
		  ) * recon[voxel_id];
    }
  }
  
  return result;
}

__device__ float localRaytracingForwardZ(float*recon, VoxelPosition &pos,
					 float &a_x, float &a_y, float &a_z, 
					 float &b_x, float &b_y, float &b_z)
{

  float result = 0.000001;
  float slope_x = ( b_x - a_x ) / ( b_z - a_z);
  float slope_y = ( b_y - a_y ) / ( b_z - a_z);

  for (int z=0;z<d_voxel_z;z++) {    
    float lor_z = pos.z + z * d_voxel_size;
    float lor_x =  slope_x * ( lor_z - a_z ) + a_x;
    float lor_y =  slope_y * ( lor_z - a_z ) + a_y;
   
    if ( pow(lor_x/d_x_edge1,2)+pow(lor_y/d_y_edge1,2)<1.0 && abs(lor_z)<d_z_edge1 ) {
      int x = floor((lor_x+d_x_edge)/d_voxel_size);
      int y = floor((lor_y+d_y_edge)/d_voxel_size);

      int voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_x-pos.x-x*d_voxel_size,2) + 
			pow(lor_y-pos.y-y*d_voxel_size,2) )
		  ) * recon[voxel_id];

      voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_x-pos.x-(x+1)*d_voxel_size,2) + 
			pow(lor_y-pos.y-y*d_voxel_size,2) )
		  ) * recon[voxel_id];

      voxel_id = z*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_x-pos.x-x*d_voxel_size,2) + 
			pow(lor_y-pos.y-(y+1)*d_voxel_size,2) )
		  ) * recon[voxel_id];

      voxel_id = z*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x+1;
      result += ( d_matrix_distance_factor - 
		  sqrt( pow(lor_x-pos.x-(x+1)*d_voxel_size,2) + 
			pow(lor_y-pos.y-(y+1)*d_voxel_size,2) )
		  ) * recon[voxel_id];
    }
  }

  return result;

}

__global__ void kernelZeroForward(float *correction, int size) {

  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx < size)
    correction[idx] = 1 / 0.000001;

}

__global__ void kernelForwardProjection(float *correction, float *recon, ListEvent *list_data, 
					VoxelPosition *det_position, 
					VoxelPosition *image_position, 
					int *event_branch,
					int num_events) 
{

  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  float result = 0.000001;

  if (idx < num_events) {

    int branch = event_branch[idx];
    ListEvent id = list_data[idx];
    VoxelPosition pA = det_position[id.detA];
    VoxelPosition pB = det_position[id.detB];
    
    VoxelPosition pos = image_position[0];

    if (branch == 1)
      result = localRaytracingForwardX(recon, pos, pA.x, pA.y, pA.z, pB.x, pB.y, pB.z);
    else if (branch == 2)
      result = localRaytracingForwardY(recon, pos, pA.x, pA.y, pA.z, pB.x, pB.y, pB.z);
    else if (branch == 3)
      result = localRaytracingForwardZ(recon, pos, pA.x, pA.y, pA.z, pB.x, pB.y, pB.z);
        
    correction[idx] = 1 / result;

  }

}

__global__ void kernelCheckEvents(ListEvent *list_data, VoxelPosition *det_position, 
				  int *event_branch, int num_events)
{

  int idx = blockIdx.x * blockDim.x + threadIdx.x;

  if (idx < num_events) {

    ListEvent id = list_data[idx];
    VoxelPosition pA = det_position[id.detA];
    VoxelPosition pB = det_position[id.detB];

    //distance between two detectors
    float distance_x = abs( pA.x - pB.x );
    float distance_y = abs( pA.y - pB.y );
    float distance_z = 0;
    float distance_z1 = pA.z*0.6 + pB.z*0.4;
    float distance_z2 = pA.z*0.4 + pB.z*0.6;

    int branch = 0;

    if( sqrt(pow(distance_x,2) + pow(distance_y,2)) > d_minimum_CrystalDistance_InOneRing1 && 
	(abs(distance_z1)<d_z_edge2 || abs(distance_z2)<d_z_edge2 || distance_z1*distance_z2<0 ) ) {

      if (distance_x > distance_y && distance_x > distance_z)
	branch = 1;
      else if (distance_y > distance_z)
	branch = 2;
      else
	branch = 3;

    }

    event_branch[idx] = branch;
  }

}

__global__ void kernelZeroBackward(float *recon_corrector, int size) {

  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx < size)
    recon_corrector[idx] = 0;

}

__device__ void localRaytracingBackwardX(float &correction, float *recon_corrector, 
					 VoxelPosition &pos,
					 float &a_x, float &a_y, float &a_z, 
					 float &b_x, float &b_y, float &b_z)
{

  float slope_y = ( b_y - a_y ) / ( b_x - a_x);
  float slope_z = ( b_z - a_z ) / ( b_x - a_x);

  for (int x=0;x<d_voxel_x;x++) {
    float lor_x = pos.x + x * d_voxel_size;
    float lor_y =  slope_y * ( lor_x - a_x ) + a_y;
    float lor_z =  slope_z * ( lor_x - a_x ) + a_z;

    if ( pow(lor_x/d_x_edge1,2)+pow(lor_y/d_y_edge1,2)<1.0 && abs(lor_z)<d_z_edge1 ) {
      int y = floor((lor_y+d_y_edge)/d_voxel_size);
      int z = floor((lor_z+d_z_edge)/d_voxel_size);
      
      int voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_y-pos.y-y*d_voxel_size,2) + 
						    pow(lor_z-pos.z-z*d_voxel_size,2) ) 
					      ) * correction);
      
      voxel_id = z*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_y-pos.y-(y+1)*d_voxel_size,2) + 
						    pow(lor_z-pos.z-z*d_voxel_size,2) ) 
					      ) * correction);

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_y-pos.y-y*d_voxel_size,2) + 
						    pow(lor_z-pos.z-(z+1)*d_voxel_size,2) ) 
					      ) * correction);

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_y-pos.y-(y+1)*d_voxel_size,2) + 
						    pow(lor_z-pos.z-(z+1)*d_voxel_size,2) ) 
					      ) * correction);
    }
  }

}

__device__ void localRaytracingBackwardY(float &correction, float*recon_corrector, 
					 VoxelPosition &pos,
					 float &a_x, float &a_y, float &a_z, 
					 float &b_x, float &b_y, float &b_z)
{

  float slope_x = ( b_x - a_x ) / ( b_y - a_y);
  float slope_z = ( b_z - a_z ) / ( b_y - a_y);

  for (int y=0;y<d_voxel_y;y++) {
    float lor_y = pos.y + y * d_voxel_size;
    float lor_x =  slope_x * ( lor_y - a_y ) + a_x;
    float lor_z =  slope_z * ( lor_y - a_y ) + a_z;

    if ( pow(lor_x/d_x_edge1,2)+pow(lor_y/d_y_edge1,2)<1.0 && abs(lor_z)<d_z_edge1 ) {
      int x = floor((lor_x+d_x_edge)/d_voxel_size);
      int z = floor((lor_z+d_z_edge)/d_voxel_size);

      int voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_x-pos.x-x*d_voxel_size,2) + 
						    pow(lor_z-pos.z-z*d_voxel_size,2) )
					      ) * correction);

      voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_x-pos.x-(x+1)*d_voxel_size,2) + 
						    pow(lor_z-pos.z-z*d_voxel_size,2) )
					      ) * correction);

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_x-pos.x-x*d_voxel_size,2) + 
						    pow(lor_z-pos.z-(z+1)*d_voxel_size,2) )
					      ) * correction);

      voxel_id = (z+1)*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_x-pos.x-(x+1)*d_voxel_size,2) + 
						    pow(lor_z-pos.z-(z+1)*d_voxel_size,2) )
					      ) * correction);
    }
  }

}

__device__ void localRaytracingBackwardZ(float &correction, float*recon_corrector, 
					 VoxelPosition &pos,
					 float &a_x, float &a_y, float &a_z, 
					 float &b_x, float &b_y, float &b_z)
{

  float slope_x = ( b_x - a_x ) / ( b_z - a_z);
  float slope_y = ( b_y - a_y ) / ( b_z - a_z);

  for (int z=0;z<d_voxel_z;z++) {
    float lor_z = pos.z + z * d_voxel_size;
    float lor_x =  slope_x * ( lor_z - a_z ) + a_x;
    float lor_y =  slope_y * ( lor_z - a_z ) + a_y;

    if ( pow(lor_x/d_x_edge1,2)+pow(lor_y/d_y_edge1,2)<1.0 && abs(lor_z)<d_z_edge1 ) {
      int x = floor((lor_x+d_x_edge)/d_voxel_size);
      int y = floor((lor_y+d_y_edge)/d_voxel_size);

      int voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_x-pos.x-x*d_voxel_size,2) + 
						    pow(lor_y-pos.y-y*d_voxel_size,2) )
					       ) * correction);

      voxel_id = z*d_voxel_y*d_voxel_x + y*d_voxel_x + x+1;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_x-pos.x-(x+1)*d_voxel_size,2) + 
						    pow(lor_y-pos.y-y*d_voxel_size,2) )
					      ) * correction);

      voxel_id = z*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_x-pos.x-x*d_voxel_size,2) + 
						    pow(lor_y-pos.y-(y+1)*d_voxel_size,2) )
					     ) * correction);
      
      voxel_id = z*d_voxel_y*d_voxel_x + (y+1)*d_voxel_x + x+1;
      atomicAdd(&recon_corrector[voxel_id], ( d_matrix_distance_factor - 
					      sqrt( pow(lor_x-pos.x-(x+1)*d_voxel_size,2) + 
						    pow(lor_y-pos.y-(y+1)*d_voxel_size,2) )
					     ) * correction);
    }
  }

}


__global__ void kernelBackwardProjection(float *correction, float *recon_corrector, 
					 ListEvent *list_data, VoxelPosition *det_position,
					 VoxelPosition *image_position, int *event_branch,
					 int num_events) 
{

  int idx = blockIdx.x * blockDim.x + threadIdx.x;

  if (idx < num_events) {

    ListEvent id = list_data[idx];
    VoxelPosition pA = det_position[id.detA];
    VoxelPosition pB = det_position[id.detB];
    int branch = event_branch[idx];

    float corr;
    VoxelPosition pos;

    if (branch > 0) {
      corr = correction[idx];
      pos = image_position[0];
    }

    if (branch == 1)
      localRaytracingBackwardX(corr, recon_corrector, pos, pA.x, pA.y, pA.z, pB.x, pB.y, pB.z);
    else if (branch == 2)
      localRaytracingBackwardY(corr, recon_corrector, pos, pA.x, pA.y, pA.z, pB.x, pB.y, pB.z);
    else if (branch == 3)
      localRaytracingBackwardZ(corr, recon_corrector, pos, pA.x, pA.y, pA.z, pB.x, pB.y, pB.z);

  }

}


int CudaImageReconstruction::calculateSource(void *image_space, void *image_position, 
					     void *source_position, void *avg, void *std, 
					     float diameter, int total_voxels, 
					     int total_sources, int start)
{

  int threads = BLOCK_SIZE;
  int blocks = total_sources / threads + 1;

  //call kernel
  kernelCalculateSource<<<blocks, threads>>>( (float*) image_space,
					      (VoxelPosition*) image_position,
					      (VoxelPosition*) source_position,
					      (float*) avg,
					      (float*) std,
					      diameter,
					      total_voxels,
					      total_sources,
					      start);
  
  return DKS_SUCCESS;
}

int CudaImageReconstruction::calculateBackground(void *image_space, void *image_position, 
						 void *source_position, void *avg, void *std, 
						 float diameter, int total_voxels, 
						 int total_sources, int start)
{

  int threads = BLOCK_SIZE;
  int blocks = total_sources / threads + 1;


  //call kernel
  kernelCalculateBackground<<<blocks, threads>>>( (float*) image_space,
						  (VoxelPosition*) image_position,
						  (VoxelPosition*) source_position,
						  (float*) avg,
						  (float*) std,
						  diameter,
						  total_voxels,
						  total_sources,
						  start);

  return DKS_SUCCESS;
}

int CudaImageReconstruction::calculateSources(void *image_space, void *image_position, 
					      void *source_position, void *avg, void *std, 
					      void *diameter, int total_voxels, 
					      int total_sources, int start)
{

  int threads = BLOCK_SIZE;
  int blocks = total_sources / threads + 1;

  //call kernel
  kernelCalculateSources<<<blocks, threads>>>( (float*) image_space,
					       (VoxelPosition*) image_position,
					       (VoxelPosition*) source_position,
					       (float*) avg,
					       (float*) std,
					       (float*) diameter,
					       total_voxels,
					       total_sources,
					       start);

  return DKS_SUCCESS;
}

int CudaImageReconstruction::calculateBackgrounds(void *image_space, void *image_position, 
						  void *source_position, void *avg, void *std, 
						  void *diameter, int total_voxels, 
						  int total_sources, int start)
{

  int threads = BLOCK_SIZE;
  int blocks = total_sources / threads + 1;


  //call kernel
  kernelCalculateBackgrounds<<<blocks, threads>>>( (float*) image_space,
						   (VoxelPosition*) image_position,
						   (VoxelPosition*) source_position,
						   (float*) avg,
						   (float*) std,
						   (float*) diameter,
						   total_voxels,
						   total_sources,
						   start);

  return DKS_SUCCESS;
}

int CudaImageReconstruction::generateNormalization(void *recon, void *image_position,
						   void *det_position, int total_det)
{

  int blocksize = 32;
  dim3 threads(blocksize, blocksize, 1);

  dim3 blocks(total_det / blocksize + 1, total_det / blocksize + 1);

  kernelNormalization<<<blocks, threads>>>( (float*) recon, 
					    (VoxelPosition*) image_position,
					    (VoxelPosition*) det_position,
					    total_det);
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess) {
    DEBUG_MSG("Error launching normalization kernel!");
    std::cout << cudaGetErrorString(err);
    return DKS_ERROR;
  }
  return DKS_SUCCESS;

}

int CudaImageReconstruction::forwardProjection(void *correction, void *recon, 
					       void *list_data, void *det_position, 
					       void *image_position, int num_events) 
{

  int threads = BLOCK_SIZE;
  int blocks = num_events / threads + 1;
  
  int ierr;
  m_event_branch = m_base->cuda_allocateMemory(sizeof(int)*num_events, ierr);

  kernelCheckEvents<<<blocks, threads >>>((ListEvent*)list_data, 
					  (VoxelPosition*)det_position, 
					  (int*)m_event_branch,
					  num_events);

  //warp mem pointers with thrust device ptr
  thrust::device_ptr<int> t_event_branch( (int*)m_event_branch );
  thrust::device_ptr<ListEvent> t_list_data( (ListEvent*)list_data );
  
  thrust::sort_by_key( t_event_branch, t_event_branch + num_events, t_list_data );
  
  kernelForwardProjection<<<blocks, threads>>>( (float*)correction,
						(float*)recon,
						(ListEvent*)list_data,
						(VoxelPosition*)det_position,
						(VoxelPosition*)image_position,
						(int*)m_event_branch,
						num_events);
  
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess) {
    std::cout << "Error launching kernel!" << std::endl;
    std::cout << cudaGetErrorString(err) << std::endl;
      
  }

  return DKS_SUCCESS;

}


int CudaImageReconstruction::backwardProjection(void *correction, void *recon_corrector, 
						void *list_data, 
						void *det_position, void *image_position, 
						int num_events, int num_voxels) 
{

  int threads = BLOCK_SIZE;
  int blocks1 = num_voxels / threads + 1;
  int blocks2 = num_events / threads + 1;

  kernelZeroBackward<<<blocks1, threads>>>((float*)recon_corrector, num_voxels);


  kernelBackwardProjection<<<blocks2, threads>>>( (float*)correction,
						  (float*)recon_corrector,
						  (ListEvent*)list_data,
						  (VoxelPosition*)det_position,
						  (VoxelPosition*)image_position,
						  (int*)m_event_branch,
						  num_events);

  m_base->cuda_freeMemory( m_event_branch );

  return DKS_SUCCESS;

}

int CudaImageReconstruction::setDimensions(int voxel_x, int voxel_y, int voxel_z, 
					   float voxel_size)
{

  //copy from host to __device__ variables
  cudaMemcpyToSymbol(d_voxel_x, &voxel_x, sizeof(int));
  cudaMemcpyToSymbol(d_voxel_y, &voxel_y, sizeof(int));
  cudaMemcpyToSymbol(d_voxel_z, &voxel_z, sizeof(int));
  cudaMemcpyToSymbol(d_voxel_size, &voxel_size, sizeof(float));

  //check for error
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess) {
    DEBUG_MSG("Error copying to device memory!");
    return DKS_ERROR;
  }

  return DKS_SUCCESS;
  
}

int CudaImageReconstruction::setEdge(float x_edge, float y_edge, float z_edge) 
{

  //copy from host to __device__ variables
  cudaMemcpyToSymbol(d_x_edge, &x_edge, sizeof(float));
  cudaMemcpyToSymbol(d_y_edge, &y_edge, sizeof(float));
  cudaMemcpyToSymbol(d_z_edge, &z_edge, sizeof(float));

  //check for error
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess) {
    DEBUG_MSG("Error copying to device memory!");
    return DKS_ERROR;
  }

  return DKS_SUCCESS;

}

int CudaImageReconstruction::setEdge1(float x_edge1, float y_edge1, float z_edge1, float z_edge2)
{

  //copy from host to __device__ variables
  cudaMemcpyToSymbol(d_x_edge1, &x_edge1, sizeof(float));
  cudaMemcpyToSymbol(d_y_edge1, &y_edge1, sizeof(float));
  cudaMemcpyToSymbol(d_z_edge1, &z_edge1, sizeof(float));
  cudaMemcpyToSymbol(d_z_edge2, &z_edge2, sizeof(float));

  //check for error
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess) {
    DEBUG_MSG("Error copying to device memory!");
    return DKS_ERROR;
  }

  return DKS_SUCCESS;

}

int CudaImageReconstruction::setMinCrystalInRing(float min_CrystalDist_InOneRing, 
						 float min_CrystalDist_InOneRing1)
{

  //copy from host to __device__ variables
  cudaMemcpyToSymbol(d_minimum_CrystalDistance_InOneRing, 
		     &min_CrystalDist_InOneRing, sizeof(float));

  cudaMemcpyToSymbol(d_minimum_CrystalDistance_InOneRing1, 
		     &min_CrystalDist_InOneRing1, sizeof(float));  

  //check for error
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess) {
    DEBUG_MSG("Error copying to device memory!");
    return DKS_ERROR;
  }

  return DKS_SUCCESS;

}

int CudaImageReconstruction::setParams(float matrix_distance_factor, float phantom_diameter, 
				       float atten_per_mm, float ring_diameter)
{

  //copy from host to __device__ variables
  cudaMemcpyToSymbol(d_matrix_distance_factor, &matrix_distance_factor, sizeof(float));
  cudaMemcpyToSymbol(d_phantom_diameter, &phantom_diameter, sizeof(float));
  cudaMemcpyToSymbol(d_atten_per_mm, &atten_per_mm, sizeof(float));
  cudaMemcpyToSymbol(d_ring_diameter, &ring_diameter, sizeof(float));

  //check for error
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess) {
    DEBUG_MSG("Error copying to device memory!");
    return DKS_ERROR;
  }

  return DKS_SUCCESS;

}
