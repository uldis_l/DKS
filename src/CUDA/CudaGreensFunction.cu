#include "CudaGreensFunction.cuh"

__global__ void kernelTmpgreen(double *tmpgreen, double hr_m0, double hr_m1, double hr_m2, int NI, int NJ) {

  
  int i = blockIdx.x;
  int j = blockIdx.y;
  int k = blockIdx.z;
  
  double cellVolume = hr_m0 * hr_m1 * hr_m2;
  
  double vv0 = i * hr_m0 - hr_m0 / 2;
  double vv1 = j * hr_m1 - hr_m1 / 2;
  double vv2 = k * hr_m2 - hr_m2 / 2;
  
  double r = sqrt(vv0 * vv0 + vv1 * vv1 + vv2 * vv2);

  double tmpgrn  = -vv2*vv2 * atan(vv0 * vv1 / (vv2 * r) );
  tmpgrn += -vv1*vv1 * atan(vv0 * vv2 / (vv1 * r) );
  tmpgrn += -vv0*vv0 * atan(vv1 * vv2 / (vv0 * r) );
  
  tmpgrn = tmpgrn / 2;

  tmpgrn += vv1 * vv2 * log(vv0 + r);
  tmpgrn += vv0 * vv2 * log(vv1 + r);
  tmpgrn += vv0 * vv1 * log(vv2 + r);

  tmpgreen[i + j * NI + k * NI * NJ] = tmpgrn / cellVolume;
  
}

__global__ void kernelTmpgreen_2(double *tmpgreen, double hr_m0, double hr_m1, double hr_m2, int NI, int NJ, int NK) {

  int tid = threadIdx.x;
  int id = blockIdx.x * blockDim.x + tid;

  if (id < NI * NJ * NK) {
    int i = id % NI;
    int k = id / (NI * NJ);
    int j = (id - k * NI * NJ) / NI;
  
    
    double cellVolume = hr_m0 * hr_m1 * hr_m2;
    
    double vv0 = i * hr_m0 - hr_m0 / 2;
    double vv1 = j * hr_m1 - hr_m1 / 2;
    double vv2 = k * hr_m2 - hr_m2 / 2;
  
    double r = sqrt(vv0 * vv0 + vv1 * vv1 + vv2 * vv2);
    
    double tmpgrn  = -vv2*vv2 * atan(vv0 * vv1 / (vv2 * r) );
    tmpgrn += -vv1*vv1 * atan(vv0 * vv2 / (vv1 * r) );
    tmpgrn += -vv0*vv0 * atan(vv1 * vv2 / (vv0 * r) );
  
    tmpgrn = tmpgrn / 2;

    tmpgrn += vv1 * vv2 * log(vv0 + r);
    tmpgrn += vv0 * vv2 * log(vv1 + r);
    tmpgrn += vv0 * vv1 * log(vv2 + r);

    tmpgreen[id] = tmpgrn / cellVolume;
    
  }
  
}

//calculate greens integral on cpu and transfer to gpu
void kernelTmpgreenCPU(double *tmpgreen, double hr_m0, double hr_m1, double hr_m2,
		       int NI, int NJ, int NK)
{

  double cellVolume = hr_m0 * hr_m1 * hr_m2;
  
  for (int k = 0; k < NK; k++) {
    for (int j = 0; j < NJ; j++) {
      for (int i = 0; i < NI; i++) {
	
	double vv0 = i * hr_m0 - hr_m0 / 2;
	double vv1 = j * hr_m1 - hr_m1 / 2;
	double vv2 = k * hr_m2 - hr_m2 / 2;
  
	double r = sqrt(vv0 * vv0 + vv1 * vv1 + vv2 * vv2);
	
	double tmpgrn = 0;
	tmpgrn += -vv2*vv2 * atan(vv0 * vv1 / (vv2 * r) );
	tmpgrn += -vv1*vv1 * atan(vv0 * vv2 / (vv1 * r) );
	tmpgrn += -vv0*vv0 * atan(vv1 * vv2 / (vv0 * r) );
	
	tmpgrn = tmpgrn / 2;
	
	tmpgrn += vv1 * vv2 * log(vv0 + r);	
	tmpgrn += vv0 * vv2 * log(vv1 + r);
	tmpgrn += vv0 * vv1 * log(vv2 + r);
	
	tmpgrn = tmpgrn / cellVolume;
	
	tmpgreen[k*NJ*NI + j*NJ + i] = tmpgrn;
      }
    }
  }

}


__global__ void kernelIngration(double *rho2_m, double *tmpgreen, int NI, int NJ, int NI_tmp, int NJ_tmp, int NK_tmp) {

  int i = blockIdx.x;
  int j = blockIdx.y;
  int k = blockIdx.z;

  int ni = NI;
  int nj = NJ;
  
  double tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7;
  
  tmp0 = 0; tmp1 = 0; tmp2 = 0; tmp3 = 0;
  tmp4 = 0; tmp5 = 0; tmp6 = 0; tmp7 = 0;
  
  
  if (i+1 < NI_tmp && j+1 < NJ_tmp && k+1 < NK_tmp)
    tmp0 = tmpgreen[(i+1) + (j+1) * NI_tmp + (k+1) * NI_tmp * NJ_tmp];
  
  if (i+1 < NI_tmp)
    tmp1 = tmpgreen[(i+1) +  j    * NI_tmp +  k * NI_tmp * NJ_tmp];
  
  if (j+1 < NJ_tmp)
    tmp2 = tmpgreen[ i    + (j+1) * NI_tmp +  k * NI_tmp * NJ_tmp];
  
  if (k+1 < NK_tmp)
    tmp3 = tmpgreen[ i    +  j    * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
  if (i+1 < NI_tmp && j+1 < NJ_tmp)
    tmp4 = tmpgreen[(i+1) + (j+1) * NI_tmp +  k * NI_tmp * NJ_tmp];  
  
  if (i+1 < NI_tmp && k+1 < NK_tmp)
    tmp5 = tmpgreen[(i+1) +  j    * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
  if (j+1 < NJ_tmp && k+1 < NK_tmp)
    tmp6 = tmpgreen[ i    + (j+1) * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
  tmp7 = tmpgreen[ i    +  j    * NI_tmp +  k * NI_tmp * NJ_tmp];
  
  
  double tmp_rho = tmp0 + tmp1 + tmp2 + tmp3 - tmp4 - tmp5 - tmp6 - tmp7;
  
  rho2_m[i + j*ni +  k*ni*nj] = tmp_rho;

}

__global__ void kernelIngration_2(double *rho2_m, double *tmpgreen, 
				  int NI, int NJ,
				  int NI_tmp, int NJ_tmp, int NK_tmp) {

  int tid = threadIdx.x;
  int id = blockIdx.x * blockDim.x + tid;

  int ni = NI;
  int nj = NJ;
  
  double tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7;

  if (id < NI_tmp * NJ_tmp * NK_tmp) {
    int i = id % NI_tmp;
    int k = id / (NI_tmp * NJ_tmp);
    int j = (id - k * NI_tmp * NJ_tmp) / NI_tmp;

    tmp0 = 0; tmp1 = 0; tmp2 = 0; tmp3 = 0;
    tmp4 = 0; tmp5 = 0; tmp6 = 0; tmp7 = 0;
    
    if (i+1 < NI_tmp && j+1 < NJ_tmp && k+1 < NK_tmp)
      tmp0 = tmpgreen[(i+1) + (j+1) * NI_tmp + (k+1) * NI_tmp * NJ_tmp];
  
    if (i+1 < NI_tmp)
      tmp1 = tmpgreen[(i+1) +  j    * NI_tmp +  k * NI_tmp * NJ_tmp];
  
    if (j+1 < NJ_tmp)
      tmp2 = tmpgreen[ i    + (j+1) * NI_tmp +  k * NI_tmp * NJ_tmp];
  
    if (k+1 < NK_tmp)
      tmp3 = tmpgreen[ i    +  j    * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
    if (i+1 < NI_tmp && j+1 < NJ_tmp)
      tmp4 = tmpgreen[(i+1) + (j+1) * NI_tmp +  k * NI_tmp * NJ_tmp];  
  
    if (i+1 < NI_tmp && k+1 < NK_tmp)
      tmp5 = tmpgreen[(i+1) +  j    * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
    if (j+1 < NJ_tmp && k+1 < NK_tmp)
      tmp6 = tmpgreen[ i    + (j+1) * NI_tmp + (k+1) * NI_tmp * NJ_tmp];  
  
    tmp7 = tmpgreen[ i    +  j    * NI_tmp +  k * NI_tmp * NJ_tmp];
    
    double tmp_rho = tmp0 + tmp1 + tmp2 + tmp3 - tmp4 - tmp5 - tmp6 - tmp7;

    rho2_m[i + j*ni +  k*ni*nj] = tmp_rho;
  }
}


//just one kernel will be executed
__global__ void mirroredRhoField0(double *rho2_m, int NI, int NJ) {
  rho2_m[0] = rho2_m[NI*NJ];
}

__global__ void mirroredRhoFieldI(double *rho2_m, int NI, int NJ) {

  int i = blockIdx.x;
  int j = blockIdx.y;
  int k = blockIdx.z;

  int idx1 = i + j*NI + k*NI*NJ;
  int idx2 = (NI-i) + j*NI + k*NI*NJ;
 
  if (NI-i < NI)
    rho2_m[idx2] = rho2_m[idx1];

}

__global__ void mirroredRhoFieldJ(double *rho2_m, int NI, int NJ) {

  int i = blockIdx.x;
  int j = blockIdx.y;
  int k = blockIdx.z;
  
  int idx1 = i + j*NI + k*NI*NJ;
  int idx2 = i + (NJ-j)*NI + k*NI*NJ;
  
  if (NJ-j < NJ)
    rho2_m[idx2] = rho2_m[idx1];

}

__global__ void mirroredRhoFieldK(double *rho2_m, int NI, int NJ, int NK) {

  int i = blockIdx.x;
  int j = blockIdx.y;
  int k = blockIdx.z;
  
  int idx1 = i + j*NI + k*NI*NJ;
  int idx2 = i + j*NI + (NK-k)*NI*NJ;
  
  if (NK-k < NK)
    rho2_m[idx2] = rho2_m[idx1];
  
}

__global__ void mirroredRhoField(double *rho2_m, 
				 int NI, int NJ, int NK, 
				 int NI_tmp, int NJ_tmp, int NK_tmp) {

  int tid = threadIdx.x;
  int id = blockIdx.x * blockDim.x + tid;

  int id1, id2, id3, id4, id5, id6, id7, id8;

  if (id < NI_tmp * NJ_tmp * NK_tmp) {
    int i = id % NI_tmp;
    int k = id / (NI_tmp * NJ_tmp);
    int j = (id - k * NI_tmp * NJ_tmp) / NI_tmp;

    int ri = NI - i;
    int rj = NJ - j;
    int rk = NK - k;

    id1 = k * NI * NJ + j * NI + i;
    id2 = k * NI * NJ + j * NI + ri;
    id3 = k * NI * NJ + rj * NI + i;
    id4 = k * NI * NJ + rj * NI + ri;

    id5 = rk * NI * NJ + j * NI + i;
    id6 = rk * NI * NJ + j * NI + ri;
    id7 = rk * NI * NJ + rj * NI + i;
    id8 = rk * NI * NJ + rj * NI + ri;
    
    double data = rho2_m[id1];
    if (i != 0) rho2_m[id2] = data;
    
    if (j != 0) rho2_m[id3] = data;

    if (i != 0 && j != 0) rho2_m[id4] = data;
    
    if (k != 0) rho2_m[id5] = data;
    
    if (k !=  0 && i != 0) rho2_m[id6] = data;
    
    if (k!= 0 && j != 0) rho2_m[id7] = data;
    
    if (k != 0 && j != 0 & i != 0) rho2_m[id8] = data;
      
  }

}

__device__ inline cuDoubleComplex ComplexMul(cuDoubleComplex a, cuDoubleComplex b) {

  cuDoubleComplex c;
  c.x = a.x * b.x - a.y * b.y;
  c.y = a.x * b.y + a.y * b.x;
  
  return c;

}

__global__ void multiplyComplexFields(cuDoubleComplex *ptr1, cuDoubleComplex *ptr2) {

  int idx = blockIdx.x;
  
  ptr1[idx] = ComplexMul(ptr1[idx], ptr2[idx]);
}


/*
copy data in shared memory first to improve memory access (few global memory accesses, maybo no improvements) 
use more threads per block to improve occupancy of hardware (test for best block and thread sizes)
*/
__global__ void multiplyComplexFields_2(cuDoubleComplex *ptr1, cuDoubleComplex *ptr2, 
					int size) 
{

  int tid = threadIdx.x;
  int idx = blockIdx.x * blockDim.x + threadIdx.x;

  
  extern __shared__ cuDoubleComplex data[];

  if (idx < size) {
    data[2*tid] = ptr1[idx];
    data[2*tid + 1] = ptr2[idx];
  }

  __syncthreads();

  if (idx < size)
    ptr1[idx] = ComplexMul(data[2*tid], data[2*tid+1]);
  

}


CudaGreensFunction::CudaGreensFunction(CudaBase *base) { 
  m_base = base;
  base_create = false;
}

/* constructor */
CudaGreensFunction::CudaGreensFunction() { 
  m_base = new CudaBase();
  base_create = true;
}
	
/* destructor */
CudaGreensFunction::~CudaGreensFunction() { 
  if (base_create)
    delete m_base;
}

int CudaGreensFunction::greensIntegral(void *tmpgreen, int I, int J, int K, int NI, int NJ, 
				       double hr_m0, double hr_m1, double hr_m2,
				       int streamId)
{
  
  int thread = 128;
  int block = (I * J * K / thread) + 1;

  //if no stream specified use default stream
  if (streamId == -1) {
    kernelTmpgreen_2<<< block, thread >>>((double*)tmpgreen, hr_m0, hr_m1, hr_m2, I, J, K);

    return DKS_SUCCESS;
  }

  
  if (streamId < m_base->cuda_numberOfStreams()) {
    cudaStream_t cs = m_base->cuda_getStream(streamId);
    kernelTmpgreen_2<<< block, thread, 0,  cs>>>((double*)tmpgreen, hr_m0, hr_m1, hr_m2, I, J, K);
    return DKS_SUCCESS;
  }
  
  return DKS_ERROR;
  
}

int CudaGreensFunction::integrationGreensFunction(void *rho2_m, void *tmpgreen, 
						  int I, int J, int K,
						  int streamId) 
{
  
  int thread = 128;
  int block = (I * J * K / thread) + 1;
  int sizerho = 2*(I - 1) * 2*(J - 1) * 2*(K - 1);

  if (streamId == -1) {
    m_base->cuda_zeroMemory( (double*)rho2_m, sizerho, 0 );
    kernelIngration_2<<< block, thread >>>( (double*)rho2_m, (double*)tmpgreen, 
					    2*(I - 1), 2*(J - 1), I, J, K);
    return DKS_SUCCESS;
  }

  
  if (streamId < m_base->cuda_numberOfStreams()) {
    cudaStream_t cs = m_base->cuda_getStream(streamId);
    m_base->cuda_zeroMemoryAsync( (double*)rho2_m, sizerho, 0, streamId);
    kernelIngration_2<<< block, thread, 0, cs>>>( (double*)rho2_m, (double*)tmpgreen, 
						  2*(I - 1), 2*(J - 1), I, J, K);
    return DKS_SUCCESS;
  }
  
  
  return DKS_ERROR;
}

int CudaGreensFunction::mirrorRhoField(void *rho2_m, int I, int J, int K, int streamId) {
  
  int thread = 128;
  int block = ( (I + 1) * (J + 1) * (K + 1) / thread) + 1;
  
  if (streamId == -1) {
    mirroredRhoField0<<< 1, 1>>>( (double *)rho2_m, 2*I,  2*J);
    mirroredRhoField<<< block, thread >>>( (double *) rho2_m,  2*I, 2*J, 2*K, I + 1, J + 1, K + 1);
    return DKS_SUCCESS;
  }
  
  
  if (streamId < m_base->cuda_numberOfStreams()) {
    cudaStream_t cs = m_base->cuda_getStream(streamId);
    mirroredRhoField0<<< 1, 1, 0, cs>>>( (double *)rho2_m, 2*I,  2*J);
    mirroredRhoField<<< block, thread, 0, cs>>>( (double *) rho2_m, 2*I, 2*J, 2*K, I+1, J+1, K+1);
    
    return DKS_SUCCESS;
  }
  
  
  
  return DKS_ERROR;
}

int CudaGreensFunction::multiplyCompelxFields(void *ptr1, void *ptr2, 
					      int size, int streamId) {
  
  int threads = 128;
  int blocks = size / threads + 1;
  int datasize = 2 * threads * sizeof(cuDoubleComplex);
  
  if (streamId == -1) {
    multiplyComplexFields_2<<<blocks, threads, datasize>>> ( (cuDoubleComplex*)ptr1, 
							     (cuDoubleComplex*)ptr2, 
							     size);
    return DKS_SUCCESS;
  }

  if (streamId < m_base->cuda_numberOfStreams()) {
    cudaStream_t cs = m_base->cuda_getStream(streamId);
    multiplyComplexFields_2<<<blocks, threads, datasize, cs >>> ( (cuDoubleComplex*)ptr1, 
								  (cuDoubleComplex*) ptr2, size);
    return DKS_SUCCESS;
  }
  
  return DKS_ERROR;
 
}
		


