#ifndef H_CUDA_CHISQUARE
#define H_CUDA_CHISQUARE

#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>

#include "CudaBase.cuh"

/** Deprecated, CUDA simpleFit implementation of ChiSquare. */
class CudaChiSquare {

private:

  bool base_create;
  CudaBase *m_base;

public:

  /**
   * Constructor which gets CudaBase as argument
   */
  CudaChiSquare(CudaBase *base) {
    m_base = base;
    base_create = false;
  }

  /* constructor */
  CudaChiSquare() {
    m_base = new CudaBase();
    base_create = true;
  }
  
  /* destructor */
  ~CudaChiSquare() {
    if (base_create)
      delete m_base;
  }

  /* PHistoTFFcn calculation */
  int cuda_PHistoTFFcn(void * mem_data, void * mem_par, void * mem_chisq, 
		       double fTimeResolution, double fRebin,
		       int sensors, int length, int numpar,
		       double &result);

  int cuda_singleGaussTF(void *mem_data, void *mem_t0, void *mem_par, void *mem_result,
			 double fTimeResolution, double fRebin, double fGoodBinOffset,
			 int sensors, int length, int numpar,
			 double &result);

  int cuda_doubleLorentzTF(void *mem_data, void *mem_t0, void *mem_par, void *mem_result,
			   double fTimeResolution, double fRebin, double fGoodBinOffset,
			   int sensors, int length, int numpar,
			   double &result);


};

#endif
