#include "CudaBase.cuh"

//=====================================//
//============Cuda kernels=============//
//=====================================//

__global__ void initcuRandState(curandState *state, int size, int seed = 0) {

  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx < size) {
    curand_init(seed, idx, 0, &state[idx]);
  }

}

__global__ void kernelCreateRandNumbers(curandState *state, double *data, int size) {

  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx < size)
    data[idx] = curand_uniform_double(&state[idx]);
}


//=====================================//
//==========Private functions==========//
//=====================================//	


//====================================//
//==========Public functions==========//
//====================================//	

CudaBase::CudaBase() { 

  currentStream = -1;
  cudaStreams.reserve(10);
  defaultRndSet = -1;
 
}
		
CudaBase::~CudaBase() { 
  
  cuda_deleteStreams();
  cuda_deleteCurandStates();
 
}

/*
  create curandStates
*/
int CudaBase::cuda_createCurandStates(int size, int seed) {

  if (defaultRndSet == 1)
    cuda_deleteCurandStates();

  int threads = 128;
  int blocks = size / threads + 1;
  if (seed == -1) 
    seed = time(NULL);

  //std::cout << "sizeof: " << sizeof(curandState) << std::endl;
  cudaMalloc(&defaultRndState, sizeof(curandState)*size);
  initcuRandState<<<blocks, threads>>>(defaultRndState, size, seed);

  defaultRndSet = 1;

  return DKS_SUCCESS;
}

int CudaBase::cuda_deleteCurandStates() {
  if (defaultRndSet == 1) {
    cudaFree(defaultRndState);
    defaultRndSet = -1;
  }

  return DKS_SUCCESS;
}

int CudaBase::cuda_createRandomNumbers(void *mem_ptr, int size) {
  int threads = BLOCK_SIZE;
  int blocks = size / threads + 1;
  
  kernelCreateRandNumbers<<<blocks, threads>>>(defaultRndState, (double *)mem_ptr, size);

  return DKS_SUCCESS;
}

curandState* CudaBase::cuda_getCurandStates() {
  return defaultRndState;
}

/*
  add cuda stream
*/
int CudaBase::cuda_createStream(int &streamId) {

  cudaStream_t tmpStream;
  cudaError_t cerror;

  cerror = cudaStreamCreate(&tmpStream);
  if (cerror != cudaSuccess) {
    DEBUG_MSG("Failed to create new CUDA stream, cuda error: " << cerror);
    return DKS_ERROR;
  }

  cudaStreams.push_back(tmpStream);
  streamId = cudaStreams.size() - 1;

  return DKS_SUCCESS;
}

/*
  add existing stream to list
*/
int CudaBase::cuda_addStream(cudaStream_t tmpStream, int &streamId) {
  cudaStreams.push_back(tmpStream);
  streamId = cudaStreams.size() - 1;

  return DKS_SUCCESS;
}


/*
  delete stream
*/
int CudaBase::cuda_deleteStream(int id) {
  //TODO: lets see if this is necessary, currently do nothing
  return DKS_ERROR;
}

/*
  delete all streams
*/
int CudaBase::cuda_deleteStreams() {

  //delete all cuda streams
  for (unsigned int i = 0; i < cudaStreams.size(); i++) {
    cudaStreamDestroy(cudaStreams[i]);
  }
  cudaStreams.clear();
  currentStream = -1;

  return DKS_SUCCESS;
}


/*
  set stream id
*/
int CudaBase::cuda_setStream(int id) {
  currentStream = id;
  return DKS_SUCCESS;
}

/*
  return stream id
*/
int CudaBase::cuda_getStreamId() {
  return currentStream;
}

/*
  set default stream as the stream to use
*/
int CudaBase::cuda_defaultStream() {
  currentStream  = -1;
  return DKS_SUCCESS;
}

int CudaBase::cuda_numberOfStreams() {
  return cudaStreams.size();
}

cudaStream_t CudaBase::cuda_getStream(int id) {
  return cudaStreams[id];
}

cublasHandle_t CudaBase::cuda_getCublas() {
  return defaultCublas;
}
	
/*
  get all available cuda devices
*/
int CudaBase::cuda_getDevices() {

  std::cout << std::endl;
  std::cout << "==============================" << std::endl;
  std::cout << "=============CUDA=============" << std::endl;
  std::cout << "==============================" << std::endl;
	
  int ndev;
  cudaGetDeviceCount(&ndev);

  std::cout << ndev << std::endl;
	
	
  for (int i = 0; i < ndev; i++) {
			
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
		
    std::cout << "Device " << i+1 << ":" << std::endl;
    std::cout << "Name: " << prop.name << std::endl;
    std::cout << "PCI bus id: " << prop.pciBusID << std::endl;
    std::cout << "PCI device id: " << prop.pciDeviceID << std::endl;
    std::cout << "PCI domain id: " << prop.pciDomainID << std::endl; 
    std::cout << "==============================" << std::endl;
  }
	
  return DKS_SUCCESS;

}
	

int CudaBase::cuda_getDeviceCount(int &ndev) {
  cudaGetDeviceCount(&ndev);
  return DKS_SUCCESS;
}

int CudaBase::cuda_getDeviceName(std::string &device_name) {
  
  int ierr = DKS_SUCCESS;

  int ndev = 0;
  cudaGetDeviceCount(&ndev);
  if (ndev > 0) {
    int device = 0;
    cudaDeviceProp prop;
    cudaGetDevice(&device);
    cudaGetDeviceProperties(&prop, device);

    device_name = prop.name;
  } else {
    ierr = DKS_ERROR;
  }
  return ierr;
}

int CudaBase::cuda_setDevice(int device) {
  int ierr = DKS_SUCCESS;
  int ndev = 0;
  cudaGetDeviceCount(&ndev);

  std::cout << "Init: " << device << "\t" << ndev << std::endl;

  if (device < ndev) {
    std::cout << "set device to: " << device << std::endl;
    cudaSetDevice(device);
  } else {
    if (ndev > 0)
      cudaSetDevice(0);
    else
      ierr = DKS_ERROR;
  }
  return ierr;
}

int CudaBase::cuda_getUniqueDevices(std::vector<int> &devices) {

  std::vector< std::string > names;

  int ndev;
  cudaGetDeviceCount(&ndev);

  for (int i = 0; i < ndev; i++) {
			
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);

    //add first device to the list, for other devices check if the name is already in the list
    if (i == 0) {
      devices.push_back(i);
      names.push_back(prop.name);
    } else {
      std::string target = prop.name;
      bool isPresent = (std::find(names.begin(), names.end(), target) != names.end());
      if (!isPresent) {
	devices.push_back(i);
	names.push_back(prop.name);
      }
    }
  }

  return DKS_SUCCESS;
}


/*
  set up cuda device
*/
int CudaBase::cuda_setUp() {

  std::cout << "set up" << std::endl;
  return DKS_SUCCESS;
}
	
/*
  allocate memory on cuda device
*/
void * CudaBase::cuda_allocateMemory(size_t size, int &ierr) {
	
  cudaError cerror;
  void * mem_ptr = NULL;
	
  cerror = cudaMalloc((void **) &mem_ptr, size);
  if (cerror != cudaSuccess) {
    DEBUG_MSG("Failed to allocate memory, cuda error: " << cerror);
    std::cout << "Error: " << cudaGetErrorString(cerror) << std::endl;
    ierr = DKS_ERROR;
  } else {
    ierr = DKS_SUCCESS;
  }
	
  return mem_ptr;
}
		
/*
  Info: free memory on device
  Return: success or error code
*/
int CudaBase::cuda_freeMemory(void * mem_ptr) {
  cudaError cerror;
	
  cerror = cudaFree(mem_ptr);
  if (cerror != cudaSuccess) {
    DEBUG_MSG("Error freeing memory, cuda error: " << cerror);
    return DKS_ERROR;
  }
	
  return DKS_SUCCESS;
}

int CudaBase::cuda_freeHostMemory(void * mem_ptr) {
  cudaError cerror;
	
  cerror = cudaFreeHost(mem_ptr);
  if (cerror != cudaSuccess) {
    DEBUG_MSG("Error freeing host memory, cuda error: " << cerror);
    return DKS_ERROR;
  }
	
  return DKS_SUCCESS;
}
