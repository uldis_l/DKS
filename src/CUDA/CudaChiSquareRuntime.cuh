#ifndef H_CUDA_CHISQUARE_RUNTIME
#define H_CUDA_CHISQUARE_RUNTIME

#include <iostream>
#include <string>

#include <cuda.h>
#include <cuda_runtime.h>
#include <nvrtc.h>

#include "../Algorithms/ChiSquareRuntime.h"
#include "CudaBase.cuh"

const std::string cudaFunctHeader = "__device__ double fTheory(double t, double *p, double *f, int *m) {";

const std::string cudaFunctFooter = "}\n";

/**
 * CUDA implementation of ChiSquareRuntime class.
 * Implements ChiSquareRuntime interface to allow musrfit to use CUDA to target Nvidia GPU.
 */
class CudaChiSquareRuntime : public ChiSquareRuntime{

private:

  bool base_create;
  CudaBase *m_base;

  CUdevice cuDevice_m;
  CUcontext context_m;
  CUmodule module_m;
  CUfunction kernel_m;

  cublasHandle_t defaultCublasRT;

  /** 
   * Setup to init device.
   * Create context and init device for RT compilation
   */
  void setUpContext();

  /** 
   * Private function to add function to kernel string.
   */
  std::string buildProgram(std::string function);

public:

  /** 
   * Constructor with CudaBase argument
   */
  CudaChiSquareRuntime(CudaBase *base);

  /** 
   * Default constructor init cuda device
   */
  CudaChiSquareRuntime();
  
  /** 
   * Default destructor.
   */
  ~CudaChiSquareRuntime();

  /** 
   * Compile program and save ptx.
   * Add function string to the calcFunction kernel and compile the program
   * Function must be valid C math expression. Parameters can be addressed in
   * a form par[map[idx]]
   */
  int compileProgram(std::string function, bool mlh = false);

  /** 
   * Launch selected kernel.
   * Launched the selected kernel from the compiled code.
   * Result is put in &result variable.
   */
  int launchChiSquare(int fitType, void *mem_data, void *mem_err, int length,
		      int numpar, int numfunc, int nummap,
		      double timeStart, double timeStep,
		      double &result);

  /** 
   * Write params to device.
   * Write params from double array to mem_param_m memory on the device.
   */
  int writeParams(const double *params, int numparams); 

  /** 
   * Write functions to device.
   * Write function values from double array to mem_func_m memory on the device.
   */
  int writeFunc(const double *func, int numfunc);

  /** 
   * Write maps to device.
   * Write map values from int array to mem_map_m memory on the device.
   */
  int writeMap(const int *map, int nummap);

  /** 
   * Allocate temporary memory needed for chi square.
   * Initializes the necessary temporary memory for the chi square calculations. Size_data needs to
   * the maximum number of elements in any datasets that will be used for calculations. Size_param,
   * size_func and size_map are the maximum number of parameters, functions and maps used in 
   * calculations.
   */
  int initChiSquare(int size_data, int size_param, int size_func, int size_map);


  /** 
   * Free temporary memory allocated for chi square.
   * Frees the chisq temporary memory and memory for params, functions and maps
   */
  int freeChiSquare();

  /** 
   * Check if CUDA device is able to run the chi square kernel.
   * Redundant - all new CUDA devices that support RT compilation will also support 
   * double precision, there are no other requirements to run chi square on GPU
   */
  int checkChiSquareKernels(int fitType, int &threadsPerBlock) {
    return DKS_SUCCESS;
  }

};

#endif
