#include "CudaFFT.cuh"

__global__ void normalize(cufftDoubleComplex *in, int N) {
	
  int id = blockIdx.x; //*blockDim.x + threadIdx.x;
  if (id < N) {
    in[id].x = in[id].x / N;
    in[id].y = in[id].y / N;
  }
	
}

CudaFFT::CudaFFT(CudaBase *base) {
  m_base = base;
  base_create = false;
}

/* constructor */
CudaFFT::CudaFFT() { 
  m_base = new CudaBase();
  base_create = true;
}
	
/* destructor */
CudaFFT::~CudaFFT() { 
  if (base_create)
    delete m_base;
}
		
/*
  Info: execute fft using cufft library
  Return: success or error code
*/
int CudaFFT::executeFFT(void * mem_ptr, int ndim, int N[3], int streamId, bool forward) {

  //create fft plan
  cufftResult cresult;
  cufftHandle plan;

  if (useDefaultPlan(ndim, N)) {
    plan = defaultPlanZ2Z;
  } else { 
    switch (ndim) {
    case 1:
      cresult = cufftPlan1d(&plan, N[0], CUFFT_Z2Z, 1);
      break;
    case 2:
      cresult = cufftPlan2d(&plan, N[1], N[0], CUFFT_Z2Z);
      break;
    case 3:
      cresult = cufftPlan3d(&plan, N[2], N[1], N[0], CUFFT_Z2Z);
      break;
    default:
      cresult = CUFFT_SUCCESS;
      break;
    }
    if (cresult != CUFFT_SUCCESS) {
      DEBUG_MSG("Error creating plan, cuda error: " << cresult);
      if (cresult == CUFFT_SETUP_FAILED)
	DEBUG_MSG("Setup failed");
		
      if (cresult == CUFFT_INVALID_SIZE)
	DEBUG_MSG("Invalid size");
		
      if (cresult == CUFFT_INVALID_TYPE)
	DEBUG_MSG("Invalid type");
		
      if (cresult == CUFFT_ALLOC_FAILED)
	DEBUG_MSG("Alloc failed");
				
      return DKS_ERROR;
    }
  }
	
  if (streamId != -1 && streamId < m_base->cuda_numberOfStreams())
    cufftSetStream(plan, m_base->cuda_getStream(streamId));
  else
    cufftSetStream(plan, 0);

  //execute perform in place FFT on created plan 
  if (forward) {
    cresult = cufftExecZ2Z(plan, (cufftDoubleComplex*)mem_ptr, 
			   (cufftDoubleComplex*)mem_ptr, CUFFT_FORWARD);
    if (cresult != CUFFT_SUCCESS) {
      DEBUG_MSG("Error executing fft, cuda error: " << cresult);
      cufftDestroy(plan);
      return DKS_ERROR;
    }		
  } else {
    cresult = cufftExecZ2Z(plan, (cufftDoubleComplex*)mem_ptr, 
			   (cufftDoubleComplex*)mem_ptr, CUFFT_INVERSE);
    if (cresult != CUFFT_SUCCESS) {
      DEBUG_MSG("Error executing ifft, cuda error: " << cresult);
      cufftDestroy(plan);
      return DKS_ERROR;
    }
  }

  //clean up resources
  if (!useDefaultPlan(ndim, N))
      cufftDestroy(plan);
  return DKS_SUCCESS;
}
		
/*
  Info: execute ifft 
  Return: success or error code
*/
int CudaFFT::executeIFFT(void * mem_ptr, int ndim, int N[3], int streamId) {
  return executeFFT(mem_ptr, ndim, N, streamId, false);
}
		
/*
  Info: execute normalize using cuda kernel
  Return: success or error code
*/
int CudaFFT::normalizeFFT(void * mem_ptr, int ndim, int N[3], int streamId) {

  cublasStatus_t status;
  unsigned int size = N[0]*N[1]*N[2];
  cuDoubleComplex alpha = make_cuDoubleComplex(1.0/size, 0);
		
  if (streamId != -1 && streamId < m_base->cuda_numberOfStreams())
    cublasSetStream(defaultCublasFFT, m_base->cuda_getStream(streamId));

  status = cublasZscal(defaultCublasFFT, size, &alpha, (cuDoubleComplex*)mem_ptr, 1);
  if (status != CUBLAS_STATUS_SUCCESS) {
    DEBUG_MSG("CUBLAS exec Zscal failed!");
    return DKS_ERROR;
  }
	
  return DKS_SUCCESS;
}

/*
  Info: execute real to complex double precision FFT
  Return: success or error code
*/
int CudaFFT::executeRCFFT(void * real_ptr, void * comp_ptr, int ndim, int N[3], int streamId) {

  //create fft plan
  cufftResult cresult;
  cufftHandle plan;
  if (useDefaultPlan(ndim, N)) {
    plan = defaultPlanD2Z;
  } else {
    switch (ndim) {
    case 1:
      cresult = cufftPlan1d(&plan, N[0], CUFFT_D2Z, 1);
      break;
    case 2:
      cresult = cufftPlan2d(&plan, N[1], N[0], CUFFT_D2Z);
      break;
    case 3:
      cresult = cufftPlan3d(&plan, N[2], N[1], N[0], CUFFT_D2Z);
      break;
    default:
      cresult = CUFFT_SUCCESS;
      break;
    }
    if (cresult != CUFFT_SUCCESS) {
      DEBUG_MSG("Error creating plan, cuda error: " << cresult);
      return DKS_ERROR;
    }
  }
	
  if (streamId != -1 && streamId < m_base->cuda_numberOfStreams())
    cresult = cufftSetStream(plan, m_base->cuda_getStream(streamId));
  else
    cufftSetStream(plan, 0);

  //execute perform in place FFT on created plan 
  cresult = cufftExecD2Z(plan, (cufftDoubleReal*)real_ptr, (cufftDoubleComplex*)comp_ptr);

  if (cresult != CUFFT_SUCCESS) {
    DEBUG_MSG("Error executing fft, cuda error: " << cresult);
    if (cresult == CUFFT_INVALID_PLAN)
      DEBUG_MSG("invalid plan");
    if (cresult == CUFFT_INVALID_VALUE)
      DEBUG_MSG("invalid value");
    if (cresult == CUFFT_INTERNAL_ERROR)
      DEBUG_MSG("internal error");
    if (cresult == CUFFT_EXEC_FAILED)
      DEBUG_MSG("exec failed");
    if (cresult == CUFFT_SETUP_FAILED)
      DEBUG_MSG("setup failed");

    return DKS_ERROR;
  }
			
  //clean up resources
  if (!useDefaultPlan(ndim, N)) {
    cresult = cufftDestroy(plan);
    if (cresult != CUFFT_SUCCESS) {
      DEBUG_MSG("Error destroying cufft plan, cuda error: " << cresult);
      return DKS_ERROR;
    }
  }
  return DKS_SUCCESS;
}
	
/*
  Info: exectue complex to real double precision FFT
  Return: success or error code
*/
int CudaFFT::executeCRFFT(void * real_ptr, void * comp_ptr, int ndim, int N[3], int streamId) {

  //create fft plan
  cufftResult cresult;
  cufftHandle plan;

  if (useDefaultPlan(ndim, N)) {
    plan = defaultPlanZ2D;
  } else {
    switch (ndim) {
    case 1:
      cresult = cufftPlan1d(&plan, N[0], CUFFT_Z2D, 1);
      break;
    case 2:
      cresult = cufftPlan2d(&plan, N[1], N[0], CUFFT_Z2D);
      break;
    case 3:
      cresult = cufftPlan3d(&plan, N[2], N[1], N[0], CUFFT_Z2D);
      break;
    default:
      cresult = CUFFT_SUCCESS;
      break;
    }
    if (cresult != CUFFT_SUCCESS) {
      DEBUG_MSG("Error creating plan, cuda error: " << cresult);
      return DKS_ERROR;
    }
  }
  
  if (streamId != -1 && streamId < m_base->cuda_numberOfStreams())
    cufftSetStream(plan, m_base->cuda_getStream(streamId));
  else
    cufftSetStream(plan, 0);
	
  //execute perform in place FFT on created plan 
  cresult = cufftExecZ2D(plan, (cufftDoubleComplex*)comp_ptr, (cufftDoubleReal*)real_ptr);
  
  if (cresult != CUFFT_SUCCESS) {
    DEBUG_MSG("Error executing fft, cuda error: " << cresult);
    cufftDestroy(plan);
    return DKS_ERROR;
  }
			
  //clean up resources
  if (!useDefaultPlan(ndim, N)) {
    cresult = cufftDestroy(plan);
    if (cresult != CUFFT_SUCCESS) {
      DEBUG_MSG("Error destroying cufft plan, cuda error: " << cresult);
      return DKS_ERROR;
    }
  }
  return DKS_SUCCESS;
}

/*
  Info: execute normalize for complex to real iFFT
  Return: success or error code
*/
int CudaFFT::normalizeCRFFT(void *real_ptr, int ndim, int N[3], int streamId) {
  cublasStatus_t status;
  unsigned int size = N[0]*N[1]*N[2];
  double alpha = 1.0/size;
		
  if (streamId != -1 && streamId < m_base->cuda_numberOfStreams())
    cublasSetStream(defaultCublasFFT, m_base->cuda_getStream(streamId));

  status = cublasDscal(defaultCublasFFT, size, &alpha, (double*)real_ptr, 1);
  if (status != CUBLAS_STATUS_SUCCESS) {
    DEBUG_MSG("CUBLAS exec Zscal failed!");
    return DKS_ERROR;
  }
	
  return DKS_SUCCESS;
}

/*
  Info: init cufftPlans witch can be reused for all FFTs of the same size and type
  Return: success or error code
*/
int CudaFFT::setupFFT(int ndim, int N[3]) {
 
  cufftResult cr1 = CUFFT_SUCCESS;
  cufftResult cr2 = CUFFT_SUCCESS;
  cufftResult cr3 = CUFFT_SUCCESS;

  //create default fft plans
  if (ndim == 1) {
    cr1 = cufftPlan1d(&defaultPlanZ2Z, N[0], CUFFT_Z2Z, 1);
    cr2 = cufftPlan1d(&defaultPlanD2Z, N[0], CUFFT_D2Z, 1);
    cr3 = cufftPlan1d(&defaultPlanZ2D, N[0], CUFFT_Z2D, 1);    
  }

  if (ndim == 2) {
    cr1 = cufftPlan2d(&defaultPlanZ2Z, N[1], N[0], CUFFT_Z2Z);
    cr2 = cufftPlan2d(&defaultPlanD2Z, N[1], N[0], CUFFT_D2Z);
    cr3 = cufftPlan2d(&defaultPlanZ2D, N[1], N[0], CUFFT_Z2D);
  }

  if (ndim == 3) {
    cr1 = cufftPlan3d(&defaultPlanZ2Z, N[2], N[1], N[0], CUFFT_Z2Z);
    cr2 = cufftPlan3d(&defaultPlanD2Z, N[2], N[1], N[0], CUFFT_D2Z);
    cr3 = cufftPlan3d(&defaultPlanZ2D, N[2], N[1], N[0], CUFFT_Z2D);
  }

  if (cr1 != CUFFT_SUCCESS || cr2 != CUFFT_SUCCESS || cr3 != CUFFT_SUCCESS) {
    DEBUG_MSG("Error creating default plan");
    return DKS_ERROR;
  }

  //create cublas plan
  cublasStatus_t status;
  status = cublasCreate(&defaultCublasFFT);
  if (status != CUBLAS_STATUS_SUCCESS) {
    DEBUG_MSG("CUBLAS create default handle failed!");
    return DKS_ERROR;
  }
  //std::cout << "cublas created" << std::endl;

  defaultNdim = ndim;
  if (ndim > 0) {
    defaultN[0] = N[0]; 
    defaultN[1] = N[1]; 
    defaultN[2] = N[2];
  }

  return DKS_SUCCESS;

}

/*
  Info: destroy default FFT plans
  Return: success or error code
*/
int CudaFFT::destroyFFT() {
  
  cufftResult cr1 = CUFFT_SUCCESS;
  cufftResult cr2 = CUFFT_SUCCESS;
  cufftResult cr3 = CUFFT_SUCCESS;
  cublasStatus_t status = CUBLAS_STATUS_SUCCESS;

  if (defaultNdim > 0) {
    //clean up resources
    cr1 = cufftDestroy(defaultPlanZ2Z);
    cr2 = cufftDestroy(defaultPlanD2Z);
    cr3 = cufftDestroy(defaultPlanZ2D);

    if (cr1 != CUFFT_SUCCESS || cr2 != CUFFT_SUCCESS || cr3 != CUFFT_SUCCESS) {
      DEBUG_MSG("Error destroying default cufft plans");
      return DKS_ERROR;
    }
  
  }

  if (defaultNdim > -1) {
    status = cublasDestroy(defaultCublasFFT);
    if (status != CUBLAS_STATUS_SUCCESS) {
      DEBUG_MSG("CUBLAS delete default handle failed!");
      return DKS_ERROR;
    }
  }

  defaultN[0] = -1;
  defaultN[1] = -1;
  defaultN[2] = -1;
  defaultNdim = -1;
  return DKS_SUCCESS;

}



