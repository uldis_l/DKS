#include "CudaChiSquare.cuh"

//simple kernel version
__global__ void kernelPHistoTFFcn(double *data, double *par, double *chisq, 
				  double fTimeResolution, double fRebin, int n) {

  int j = blockIdx.x;
  int i = blockIdx.y;

  int idx = i * n + j;

  const double tau = 2.197019;
  double dt0 = fTimeResolution * 0.5 * (fRebin - 1);
  double time = dt0 + fTimeResolution * fRebin * j;  

  double w = par[0]*0.08516155035269027;

  double ldata = data[idx];
  
  double theo =  par[2 + i*4] * exp(-time/tau) * (1.0 + par[3 + i*4] * exp(-0.5 * pow(par[1]*time,2.0) ) * cos(w * time+par[4+i*4] * 1.74532925199432955e-2) ) + par[5+i*4]; 

  
  if (ldata != 0.0)
    chisq[idx] = (theo - ldata) * (theo - ldata) / ldata;
  else
    chisq[idx] = theo * theo;
  
}

__global__ void kernelPHistoTFFcn_2(double *data, double *par, double *chisq, 
				    double fTimeResolution, double fRebin, int n, int s) {

  int j = blockIdx.x;

  const double tau = 2.197019;
  double dt0 = fTimeResolution * 0.5 * (fRebin - 1);
  double time = dt0 + fTimeResolution * fRebin * j;  
  double w = par[0]*0.08516155035269027;
  double tt = exp(-time/tau);
  double pp = exp(-0.5 * par[1] * time * par[1] * time);
  double wt = w * time;

  int idx;
  double ldata, theo;
  for (int i = 0; i < s; i++) {
    idx = i * n + j;
    ldata = data[idx];
  
    theo = par[2 + i*4] * tt * (1.0 + par[3 + i*4] * pp * cos(wt + par[4+i*4] * 1.74532925199432955e-2) ) + par[5+i*4]; 
  
    if (ldata != 0.0)
      chisq[idx] = (theo - ldata) * (theo - ldata) / ldata;
    else
      chisq[idx] = theo * theo;
  }
  
}

#define TAU 2.197019

__global__ void kernelPHistoTFFcn_3(double *data, double *par, double *chisq, 
				    double fTimeResolution, double fRebin, 
				    int length, int sensors, int numpar) {

  
  //define shared variable for parameters
  extern __shared__ double p[];

  //get thread id and calc global id
  int tid = threadIdx.x;
  int j = blockIdx.x * blockDim.x + threadIdx.x;

  //load parameters from global to shared memory
  if (tid < numpar)
    p[tid] = par[tid];

  //sync threads
  __syncthreads();

  if (j < length) {

    double dt0 = fTimeResolution * 0.5 * (fRebin - 1);
    double time = dt0 + fTimeResolution * fRebin * j;  
    double w = p[0]*0.08516155035269027;
    double tt = exp(-time/TAU);
    double pp = exp(-0.5 * pow(p[1]*time, 2.0));
    double wt = w * time;

    int idx;
    double ldata, theo;
    for (int i = 0; i < sensors; i++) {
      idx = i * length + j;
      ldata = data[idx];
  
      theo = p[2+i*4]*tt*(1.0+p[3+i*4]*pp*cos(wt+p[4+i*4]*1.74532925199432955e-2))+p[5+i*4]; 
  
      if (ldata != 0.0)
	chisq[idx] = (theo - ldata) * (theo - ldata) / ldata;
      else
	chisq[idx] = theo * theo;
    }
  }
  
  
}

__global__ void kernelSingleGaussTF(double *data, unsigned int *t0, double *par, double *result,
				    double fTimeResolution, double fRebin, double fGoodBinOffset,
				    int length, int sensors, int numpar)
{

  //define shared variable for parameters
  extern __shared__ double p[];

  //get thread id and calc global id
  int tid = threadIdx.x;
  int j = blockIdx.x * blockDim.x + threadIdx.x;

  //load parameters from global to shared memory
  if (tid < numpar)
    p[tid] = par[tid];

  //sync threads
  __syncthreads();

  if (j < length) {
    double dt0 = fTimeResolution*0.5*(fRebin - 1);
    double w1 = par[0]*0.08516155035269027;

    int idx;
    double ldata, lft0, theo, time;
    for (int  i = 0; i < sensors; i++) {
      idx = i * length + j;
      lft0 = t0[i];
      if (j >= lft0 + fGoodBinOffset/fRebin) {
	ldata = data[idx];
	time = dt0 + fTimeResolution * fRebin* (j - lft0);
	theo = p[2+i*4]*exp(-time/TAU)*(1.0+p[3+i*4]*exp(-0.5*pow(p[1]*time,2.0))
					*cos(w1*time+p[4+i*4]*1.74532925199432955e-2))+p[5+i*4]; 
	// 1.74532925199432955e-2 = pi/180

	if ( (ldata > 1.0e-9) && (fabs(theo) > 1.0e-9) )
	  result[idx] = (theo - ldata) + ldata*log(ldata/theo);
	else
	  result[idx] = theo - ldata;
      } else {
	result[idx] = 0;
      }
    }
  }

}

__global__ void kernelDoubleLorentzTF(double *data, unsigned int *t0, double *par, double *result,
				      double fTimeResolution, double fRebin, double fGoodBinOffset,
				      int length, int sensors, int numpar)
{

  //define shared variable for parameters
  extern __shared__ double p[];

  //get thread id and calc global id
  int tid = threadIdx.x;
  int j = blockIdx.x * blockDim.x + threadIdx.x;

  //load parameters from global to shared memory
  if (tid < numpar)
    p[tid] = par[tid];

  //sync threads
  __syncthreads();

  if (j < length) {
    double dt0 = fTimeResolution*0.5*(fRebin - 1);
    double w1 = p[0]*0.08516155035269027;
    double w2 = p[2]*0.08516155035269027;

    int idx;
    double ldata, lft0, theo, time;
    for (int  i = 0; i < sensors; i++) {
      
      idx = i * length + j;
      lft0 = t0[i];
      if (j >= lft0 + fGoodBinOffset/fRebin) {
	ldata = data[idx];
	time = dt0+fTimeResolution*fRebin*(j-lft0);

	theo = p[4+i*5]*exp(-time/TAU)*
	  (1.0+p[8+i*5]*p[5+i*5]*exp(-p[1]*time)*
	   cos(w1*time+p[6+i*5]*1.74532925199432955e-2)+
	   (1.0-p[8+i*5])*p[5+i*5]*exp(-p[3]*time)*
	   cos(w2*time+p[6+i*5]*1.74532925199432955e-2))+p[7+i*5]; 
	// 1.74532925199432955e-2 = pi/180
	if ((ldata > 1.0e-9) && (fabs(theo) > 1.0e-9))
	  result[idx] = (theo - ldata) + ldata*log(ldata/theo);
	else
	  result[idx] = theo - ldata;
      } else {
	result[idx] = 0;
      }
    }
  }
}



int CudaChiSquare::cuda_PHistoTFFcn(void *mem_data, void *mem_ptr, void *mem_chisq,
				    double fTimeResolution, double fRebin,
				    int sensors, int length, int numpar,
				    double &result)
{
  
  int threads = 128;
  int blocks = length / threads + 1;

  kernelPHistoTFFcn_3<<<blocks, threads, numpar*sizeof(double) >>>((double*)mem_data, 
								   (double*)mem_ptr, 
								   (double*)mem_chisq, 
								   fTimeResolution, 
								   fRebin, length, 
								   sensors, numpar);


  cublasStatus_t status;
  status = cublasDasum(m_base->cuda_getCublas(), sensors*length, (double*)mem_chisq, 1, &result);
  if (status != CUBLAS_STATUS_SUCCESS) {
    DEBUG_MSG("cublas asum failed");
    return DKS_ERROR;
  }
  

  return DKS_SUCCESS;
}


int CudaChiSquare::cuda_singleGaussTF(void *mem_data, void *mem_t0, void *mem_par, void *mem_result,
				      double fTimeResolution, double fRebin, double fGoodBinOffset,
				      int sensors, int length, int numpar,
				      double &result)
{

  int threads = 128;
  int blocks = length / threads + 1;

  kernelSingleGaussTF<<<blocks, threads, numpar*sizeof(double) >>>( (double*)mem_data,
								    (unsigned int*)mem_t0,
								    (double*)mem_par,
								    (double*)mem_result,
								    fTimeResolution,
								    fRebin,
								    fGoodBinOffset,
								    length, sensors, numpar);

  cublasDasum(m_base->cuda_getCublas(), sensors*length, (double*)mem_result, 1, &result);
  result = 2.0 * result;
			

  return DKS_SUCCESS;

}


int CudaChiSquare::cuda_doubleLorentzTF(void *mem_data, void *mem_t0, void *mem_par, void *mem_result,
					double fTimeResolution, double fRebin, double fGoodBinOffset,
					int sensors, int length, int numpar,
					double &result)
{

  int threads = 128;
  int blocks = length / threads + 1;

  kernelDoubleLorentzTF<<<blocks, threads, numpar*sizeof(double) >>>( (double*)mem_data,
								      (unsigned int*)mem_t0,
								      (double*)mem_par,
								      (double*)mem_result,
								      fTimeResolution,
								      fRebin,
								      fGoodBinOffset,
								      length, sensors, numpar);

  cublasDasum(m_base->cuda_getCublas(), sensors*length, (double*)mem_result, 1, &result);
  result = 2.0 * result;
			

  return DKS_SUCCESS;

}
