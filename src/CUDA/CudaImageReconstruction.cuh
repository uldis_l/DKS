#ifndef H_CUDA_IMAGERECONSTRUCTION
#define H_CUDA_IMAGERECONSTRUCTION

#include <cuda.h>
#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/count.h>

#include "../Algorithms/ImageReconstruction.h"
#include "CudaBase.cuh"

/** CUDA implementation of ImageReconstruction interface. */
class CudaImageReconstruction : public ImageReconstruction {

private:

  bool base_create;
  CudaBase *m_base;
  
public:
 
  /** Constructor */
  CudaImageReconstruction() { 
    m_base = new CudaBase();
    base_create = true;
  };

  /** Constructor with base **/
  CudaImageReconstruction(CudaBase *base) {
    m_base = base;
    base_create = false;
  }

  /** Destructor */
  ~CudaImageReconstruction() { 
    if (base_create)
      delete m_base;
  };

  /** CUDA implementation of caluclate source
   */
  int calculateSource(void *image_space, void *image_position, void *source_position, 
		      void *avg, void *std, float diameter, int total_voxels, 
		      int total_sources, int start = 0);

  /** Cuda implementation of calculate background
   */
  int calculateBackground(void *image_space, void *image_position, void *source_position, 
			  void *avg, void *std, float diameter, int total_voxels, 
			  int total_sources, int start = 0);

  /**
   * Caluclate source for differente sources
   */
  int calculateSources(void *image_space, void *image_position, void *source_position, 
		       void *avg, void *std, void *diameter, int total_voxels, 
		       int total_sources, int start = 0);

  /**
   * Calculate background for differente sources
   */
  int calculateBackgrounds(void *image_space, void *image_position, void *source_position, 
			   void *avg, void *std, void *diameter, int total_voxels, 
			   int total_sources, int start = 0);

  /** Generate normalization.
   * Goes trough detectors pairs and if detector pair crosses image launches seperate kernel
   * that updates voxel values in the image on the slope between these two detectors.
   */
  int generateNormalization(void *recon, void *image_position, 
			    void *det_position, int total_det);


  /** Calculate forward projection.
   * For image reconstruction calculates forward projections.
   * see recon.cpp for details
   */
  int forwardProjection(void *correction, void *recon, void *list_data, void *det_position, 
			void *image_position, int num_events);

  /** Calculate backward projection.
   * For image reconstruction calculates backward projections.
   * see recon.cpp for details
   */
  int backwardProjection(void *correction, void *recon_corrector, void *list_data, 
			 void *det_position, void *image_position, 
			 int num_events, int num_voxels);

  /** Set the voxel dimensins on device.
   * 
   */
  int setDimensions(int voxel_x, int voxel_y, int voxel_z, float voxel_size);

  /** Set the image edge.
   * 
   */
  int setEdge(float x_edge, float y_edge, float z_edge);

  /** Set the image edge1.
   * 
   */
  int setEdge1(float x_edge1, float y_edge1, float z_edge1, float z_edge2);

  /** Set the minimum crystan in one ring values.
   * 
   */
  int setMinCrystalInRing(float min_CrystalDist_InOneRing, float min_CrystalDist_InOneRing1);

  /** Set all other required parameters for reconstruction.
   * 
   */
  int setParams(float matrix_distance_factor, float phantom_diameter,
		float atten_per_mm, float ring_diameter);


};

#endif
