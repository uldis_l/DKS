#define PI     3.141592653589793115998
#define TWO_PI 6.283185307179586231996
#define DEG_TO_RAD 1.7453292519943295474371681e-2

/** Theory function declaration.
 * Definition of the theory function will be build during runtime before compilation.
 */
__device__ double fTheory(double t, double *p, double *f, int *m);

/** MusrFit predefined functions.
 * Predefined functions from MusrFit that can be used to define the theory function.
 * First parameter in all the functions is alwats time - t, rest of the parameters depend
 * on the function.
 */
__device__ double se(double t, double lamda) {
  return exp( -lamda*t );
}

__device__ double ge(double t, double lamda, double beta) {
  return exp( -pow(lamda*t, beta) );
}

__device__ double sg(double t, double sigma) {
  return exp( -0.5*pow(sigma*t, 2.0) );
}

__device__ double stg(double t, double sigma) {
  double sigmatsq = pow(sigma*t, 2.0);

  return (1.0/3.0) + (2.0/3.0)*(1.0 - sigmatsq) * exp(-0.5*sigmatsq);
}

__device__ double sekt(double t, double lambda) {
  double lambdat = lambda*t;

  return (1.0/3.0) + (2.0/3.0)*(1.0 - lambdat) * exp(-lambdat);
}

__device__ double lgkt(double t, double lambda, double sigma) {
  double lambdat = lambda*t;
  double sigmatsq = pow(sigma*t, 2.0);

  return (1.0/3.0) + (2.0/3.0)*(1.0 - lambdat - sigmatsq) * exp(-lambdat - 0.5*sigmatsq);
}

__device__ double skt(double t, double sigma, double beta) {
  if (beta < 1.0e-3)
    return 0.0;
  double sigmatb = pow(sigma*t, beta);

  return (1.0/3.0) + (2.0/3.0)*(1.0 - sigmatb) * exp(-sigmatb/beta);
}

__device__ double spg(double t, double lambda, double gamma, double q) {
  double lam2 = lambda*lambda;
  double lamt2q = t*t*lam2*q;
  double rate2 = 4.0*lam2*(1.0-q)*t/gamma;
  double rateL = sqrt(fabs(rate2));
  double rateT = sqrt(fabs(rate2)+lamt2q);

  return (1.0/3.0)*exp(-rateL) + (2.0/3.0)*(1.0 - lamt2q / rateT)*exp(-rateT);
}

__device__ double rahf(double t, double nu, double lambda) {
  double nut  = nu*t;
  double nuth = nu*t/2.0;
  double lamt = lambda*t;

  return (1.0/6.0)*(1.0-nuth)*exp(-nuth) + (1.0/3.0)*(1.0-nut/4.0)*exp(-0.25*(nut+2.44949*lamt));
}

__device__ double tf(double t, double phi, double nu) {
  double tmp_nu = TWO_PI*nu*t;
  double tmp_phi = DEG_TO_RAD*phi;

  return cos(tmp_nu + tmp_phi);
}

__device__ double ifld(double t, double alpha, double phi, double nu, double lambdaT, double lambdaL) {
  double wt = TWO_PI*nu*t;
  double ph = DEG_TO_RAD*phi;

  return alpha*cos(wt+ph)*exp(-lambdaT*t) + (1.0-alpha)*exp(-lambdaL*t);
}

__device__ double ifgk(double t, double alpha, double nu, double sigma, double lambda, double beta) {
  double wt = TWO_PI*nu*t;
  double rate2 = sigma*sigma*t*t;
  double rateL = 0.0;
  double result = 0.0;

  // make sure lambda > 0
  if (lambda < 0.0)
    return 0.0;

  if (beta < 0.001) {
    rateL = 1.0;
  } else {
    rateL = pow(lambda*t, beta);
  }

  if (nu < 0.01) {
    result = (1.0-alpha)*exp(-rateL) + alpha*(1.0-rate2)*exp(-0.5*rate2);
  } else {
    result = (1.0-alpha)*exp(-rateL) + alpha*(cos(wt)-sigma*sigma*t*t/(wt)*sin(wt))*exp(-0.5*rate2);
  }

  return result;
}

__device__ double ifll(double t, double alpha, double nu, double a, double lambda, double beta) {
  double wt = TWO_PI*nu*t;
  double at = a*t;
  double rateL = 0.0;
  double result = 0.0;

  // make sure lambda > 0
  if (lambda < 0.0)
    return 0.0;

  if (beta < 0.001) {
    rateL = 1.0;
  } else {
    rateL = pow(lambda*t, beta);
  }

  if (nu < 0.01) {
    result = (1.0-alpha)*exp(-rateL) + alpha*(1.0-at)*exp(-at);
  } else {
    result = (1.0-alpha)*exp(-rateL) + alpha*(cos(wt)-a/(TWO_PI*nu)*sin(wt))*exp(-at);
  }

  return result;
}

__device__ double b(double t, double phi, double nu) {
  return j0(TWO_PI*nu*t + DEG_TO_RAD*phi);
}

__device__ double ib(double t, double alpha, double phi, double nu, double lambdaT, double lambdaL) {
  double wt = TWO_PI * nu * t;
  double ph = DEG_TO_RAD * phi;

  return alpha*j0(wt+ph)*exp(-lambdaT*t) + (1.0-alpha)*exp(-lambdaL*t);
}

__device__ double ab(double t, double sigma, double gamma) {
  double gt = gamma*t;

  return exp(-pow(sigma/gamma,2.0)*(exp(-gt) - 1.0 + gt));
}

__device__ double snkzf(double t, double Delta0, double Rb) {
  double D0t2 = pow(Delta0*t, 2.0);
  double aa = 1.0/(1.0+pow(Rb,2.0)*D0t2);

  return (1.0/3.0) + (2.0/3.0)*pow(aa,1.5)*(1.0-D0t2*aa)*exp(-0.5*D0t2*aa);
}

__device__ double snktf(double t, double phi, double nu, double Delta0, double Rb) {
  double wt = TWO_PI*nu*t;
  double ph = DEG_TO_RAD*phi;
  double D0t2 = pow(Delta0*t, 2.0);
  double aa = 1.0/(1.0+pow(Rb,2.0)*D0t2);

  return sqrt(aa)*exp(-0.5*D0t2*aa)*cos(wt+ph);
}

__device__ double dnkzf(double t, double Delta0, double Rb, double nuc) {
  double nuct = nuc*t;
  double theta = (exp(-nuct) - 1.0 -nuct)/pow(nuc, 2.0);
  double aa = 1.0/(1.0+4.0*pow(Rb*Delta0,2.0)*theta);

  return sqrt(aa)*exp(-2.0*Delta0*Delta0*theta*aa);
}

__device__ double dnktf(double t, double phi, double nu, double Delta0, double Rb, double nuc) {
  double wt = TWO_PI*nu*t;
  double ph = DEG_TO_RAD*phi;
  double nuct = nuc*t;
  double theta = (exp(-nuct) - 1.0 -nuct)/pow(nuc, 2.0);
  double aa = 1.0/(1.0+2.0*pow(Rb*Delta0,2.0)*theta);

  return sqrt(aa)*exp(-Delta0*Delta0*theta*aa)*cos(wt+ph);
}

/** Theory and chisquare functions.
 * Based on the compiler flags set theory is calculated either in single hist mode or asymetric.
 * Based on the compiler flags calculate either chisq or MLE
 */

__device__ inline double singleHist(double &N0, double &tau, double &bkg, double &f, double &t) {
  return N0 * exp (-t/tau ) * (1.0 + f) + bkg;
}

__device__ inline double asymetry(double &a, double &b, double &f) {
  return (f * (a * b) - (a - 1.0)) / ((a + 1.0) - f * (a * b - 1.0));
}

__device__ inline double getTheory(double &c1, double &c2, double &c3, double &f, double &t) {
#ifndef ASYMETRY
  return singleHist(c1, c2, c3, f, t);
#elif
  return asymetry(c1, c2, f);
#endif
}

__device__ inline double chiSq(double &data, double &theo, double &err) {
  double res = (theo - data) * (theo - data);
  if (err != 0.0)
    res /= err;
 
  return res;
}

__device__ inline double mle(double &data, double &theo, double &err) {
  double res = (theo - data);
  if ( data > 1.0e-9 && fabs(theo) > 1.0e-9 )
    res += data * log(data / theo);

  return res;
}

__device__ inline double getChiSq(double &data, double &theo, double &err) {
#ifndef MLE
  return chiSq(data, theo, err);
#elif
  return mle(data, theo, err);
#endif
}

//-----------------------------------------------------------------------------------------------
/**
 * Kernel to calculate theory function and chisquare/mle values for single histogram fits.
 */
extern "C" __global__ void kernelChiSquareSingleHisto(double *data, double *err, double *par,
             double *chisq, int *map, double *funcv, int length,
					   int numpar, int numfunc, int nummap,
					   double timeStart, double timeStep,         
					   double tau, double N0, double bkg) {
  //define shared variable for parameters                                  
  extern __shared__ double smem[];                                         
  double *p = (double*)smem;                                                    
  double *f = (double*)&smem[numpar];
  int *m = (int*)&smem[numpar + numfunc];
                                                                           
  //get thread id and calc global id                                       
  int tid;
  int j = blockIdx.x * blockDim.x + threadIdx.x;
                                                                           
  //load parameters from global to shared memory                           
  tid = threadIdx.x;  
  while (tid < numpar) {
    p[tid] = par[tid];  
    tid += blockDim.x;
  }                                                   

  //load functions from global to shared memory
  tid = threadIdx.x;
  while (tid < numfunc) {
    f[tid] = funcv[tid];
    tid += blockDim.x;
  }

  //load maps from global memory
  tid = threadIdx.x;
  while (tid < nummap) {
    m[tid] = map[tid];
    tid += blockDim.x;
  }
                                                                 
  //sync threads                                                           
  __syncthreads();
                                                                           
  while (j < length) {
     
    double t = timeStart + j*timeStep;                                  
    double ldata = data[j];
    double lerr = err[j];
   
    double theo = N0 * exp (-t/tau ) * (1.0 + fTheory(t, p, f, m)) + bkg;

    #ifdef MLH
    if ((ldata > 1.0e-9) && (fabs(theo) > 1.0e-9))
      chisq[j] = 2.0 * ((theo - ldata) + ldata * log(ldata / theo));
    else
      chisq[j] = 2.0 * (theo - ldata);
    #else
    if (lerr != 0.0)
      chisq[j] = (theo - ldata) * (theo - ldata) / (lerr * lerr);
    else
      chisq[j] = theo * theo;
    #endif
    
    j += gridDim.x * blockDim.x;

  }           
}

//-----------------------------------------------------------------------------------------------
/**
 * Kernel to calculate theory function and chisquare/mle values for asymmetry fits.
 */
extern "C" __global__ void kernelChiSquareAsymmetry(double *data, double *err, double *par,
             double *chisq, int *map, double *funcv, int length,
             int numpar, int numfunc, int nummap,
             double timeStart, double timeStep,
             double alpha, double beta) {
  //define shared variable for parameters
  extern __shared__ double smem[];
  double *p = (double*)smem;
  double *f = (double*)&smem[numpar];
  int *m = (int*)&smem[numpar + numfunc];

  //get thread id and calc global id
  int tid;
  int j = blockIdx.x * blockDim.x + threadIdx.x;

  //load parameters from global to shared memory
  tid = threadIdx.x;
  while (tid < numpar) {
    p[tid] = par[tid];
    tid += blockDim.x;
  }

  //load functions from global to shared memory
  tid = threadIdx.x;
  while (tid < numfunc) {
    f[tid] = funcv[tid];
    tid += blockDim.x;
  }

  //load maps from global memory
  tid = threadIdx.x;
  while (tid < nummap) {
    m[tid] = map[tid];
    tid += blockDim.x;
  }

  //sync threads
  __syncthreads();

  while (j < length) {

    double t = timeStart + j*timeStep;
    double ldata = data[j];
    double lerr = err[j];

    double theoVal = fTheory(t, p, f, m);
    double ab = alpha*beta;

    double theo = ((ab+1.0)*theoVal - (alpha-1.0))/((alpha+1.0) - (ab-1.0)*theoVal);

    #ifdef MLH
    chisq[j] = 0.0; // log max likelihood not defined here
    #else
    if (lerr != 0.0)
      chisq[j] = (theo - ldata) * (theo - ldata) / (lerr * lerr);
    else
      chisq[j] = theo * theo;
    #endif

    j += gridDim.x * blockDim.x;
  }
}

