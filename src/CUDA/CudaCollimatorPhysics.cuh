#ifndef H_CUDA_COLLIMATORPHYSICS
#define H_CUDA_COLLIMATORPHYSICS

#include <iostream>
#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include <vector_types.h>
#include <curand_kernel.h>

#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/count.h>

#include <cublas_v2.h>

#include "../Algorithms/CollimatorPhysics.h"
#include "CudaBase.cuh"

/**
 * Structure for storing particle on GPU or MIC as AoS.
 * Structure for OPAL particle, can be used to store particles on the GPU in array of structures.
 */
typedef struct __align__(16) {
  int label;
  unsigned localID;
  double3 Rincol;
  double3 Pincol;
  long IDincol;
  int Binincol;
  double DTincol;
  double Qincol;
  long LastSecincol;
  double3 Bfincol;
  double3 Efincol;
} CUDA_PART;

/**
 * Structure for storing particle on GPU as AoS
 * Structure for OPAL particle, can be used to store particles on the GPU in array of structures,
 * contains only data that are used by the GPU kernels, the rest of the particle data must be kept
 * on the host side.
 */
typedef struct {
  int label;
  unsigned localID;
  double3 Rincol;
  double3 Pincol;
} CUDA_PART_SMALL;

/**
 * Structure for storing particle on GPU as SoA.
 * Structure for OPAL particle, can be used to store particles on the GPU in structure of arrays.
 */
typedef struct {
  int *label;
  unsigned *localID;
  double3 *Rincol;
  double3 *Pincol;
  long *IDincol;
  int *Binincol;
  double *DTincol;
  double *Qincol;
  long *LastSecincol;
  double3 *Bfincol;
  double3 *Efincol;
} CUDA_PART2;

/**
 * Structure for storing particle on GPU
 * Structure for OPAL particle, can be used to store particles on the GPU in structure of arrays,
 * contains only data that are used by the GPU kernels, the rest of the particle data must be kept
 * on the host side.
 */
typedef struct {
  int *label;
  unsigned *localID;
  double3 *Rincol;
  double3 *Pincol;
} CUDA_PART2_SMALL;

/** 
 * Operator used in thrust sort to compare particles by label.
 * Used to move dead particles to the end of array, since they have label -1 or -2.
 */
struct compare_particle_small
{
  int threshold;

  compare_particle_small() {
    threshold = 0;
  }

  void set_threshold(int t) {
    threshold = t;
  }

  __host__ __device__
  bool operator()(CUDA_PART_SMALL p1, CUDA_PART_SMALL p2) {
    return p1.label > p2.label;
  }

  __host__  __device__
  bool operator()(CUDA_PART_SMALL p1) {
    return p1.label < threshold;
  }
};

/** 
 * CudaCollimatorPhysics class based on DKSCollimatorPhysics interface.
 * Contains kerenls that execute CollimatorPhysics functions form OPAL.
 * For detailed documentation on CollimatorPhysics functions see OPAL documentation.
 */
class CudaCollimatorPhysics : public DKSCollimatorPhysics {

private:

  bool base_create;
  CudaBase *m_base;

public:

  /** 
   * Constructor with CudaBase as argument.
   * Create a new instace of the CudaCollimatorPhysics using existing CudaBase object.
   */
  CudaCollimatorPhysics(CudaBase *base) {
    m_base = base;
    base_create = false;
  }

  /** 
   * Empty constructor.
   * Create a new instance of CudaCollimatorPhysics with its own CudaBase. 
   */
  CudaCollimatorPhysics() { 
    m_base = new CudaBase();
    base_create = true;
  }

  /** 
   * Destructor.
   * Destroy CudaBase object if it was created by CudaCollimatorPhysics constructor.
   */
  ~CudaCollimatorPhysics() { 
    if (base_create)
      delete m_base;
  };

  /** 
   * Execute collimator physics kernel.
   *
   */
  int CollimatorPhysics(void *mem_ptr, void *par_ptr, 
			int numpartices, bool enableRutherforScattering = true);

  /** 
   * Special calse CollimatorPhysics kernel that uses SoA instead of AoS.
   * Used only on the MIC side, was not implemented on the GPU.
   */
  int CollimatorPhysicsSoA(void *label_ptr, void *localID_ptr, 
			   void *rx_ptr, void *ry_ptr, void *rz_ptr, 
			   void *px_ptr, void *py_ptr, void *pz_ptr,
			   void *par_ptr, int numparticles)
    {
      return DKS_ERROR;
    }

  /** 
   * Sort particle array on GPU.
   * Count particles that are dead (label -1) or leaving material (label -2) and sort particle
   * array so these particles are at the end of array
   */
  int CollimatorPhysicsSort(void *mem_ptr, int numparticles, int &numaddback);
  
  /** 
   * Special calse CollimatorPhysicsSort kernel that uses SoA instead of AoS.
   * Used only on the MIC side, was not implemented on the GPU.
   */
  int CollimatorPhysicsSortSoA(void *label_ptr, void *localID_ptr, 
			       void *rx_ptr, void *ry_ptr, void *rz_ptr, 
			       void *px_ptr, void *py_ptr, void *pz_ptr,
			       void *par_ptr, int numparticles, int &numaddback) 
    {
      return DKS_ERROR;
    }

  /** 
   * BorisPusher push function for integration from OPAL.
   * ParallelTTracker integration from OPAL implemented in cuda.
   * For more details see ParallelTTracler docomentation in opal
   */
  int ParallelTTrackerPush(void *r_ptr, void *p_ptr, int npart, void *dt_ptr, 
			   double dt, double c, bool usedt = false, int streamId = -1);

  /** 
   * BorisPusher kick function for integration from OPAL.
   * ParallelTTracker integration from OPAL implemented in cuda.
   * For more details see ParallelTTracler docomentation in opal
   */
  int ParallelTTrackerKick(void *r_ptr, void *p_ptr, void *ef_ptr,
			   void *bf_ptr, void *dt_ptr, double charge, double mass,
			   int npart, double c, int streamId = -1); 

  /** 
   * BorisPusher push function with transformto function form OPAL.
   * ParallelTTracker integration from OPAL implemented in cuda.
   * For more details see ParallelTTracler docomentation in opal
   */
  int ParallelTTrackerPushTransform(void *x_ptr, void *p_ptr, void *lastSec_ptr, 
				    void *orient_ptr, int npart, int nsec, 
				    void *dt_ptr, double dt, double c, 
				    bool usedt = false, int streamId = -1);

};

#endif
