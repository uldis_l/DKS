#ifndef H_CUDA_BASE
#define H_CUDA_BASE

#include "../DKSDefinitions.h"

#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cuda_runtime.h>
#include <cufft.h>
#include <cublas_v2.h>
#include <curand_kernel.h>
#include <time.h>

#define BLOCK_SIZE 128

/**
 * CUDA base class handles device setup and basic communication with the device.
 * Handles devicew setup, memory manegement, data transfers and stream setup for 
 * asynchronous data transfers and kernel executions.
 */
class CudaBase {

private:
	
  int currentStream;
  std::vector<cudaStream_t> cudaStreams;

protected: 

  cublasHandle_t defaultCublas;

  curandState *defaultRndState;
  int defaultRndSet;

public:
	
  CudaBase();
		
  ~CudaBase();

  /**
   * Init cuda random number (cuRand) states.
   * Create an array of type curandState  with "size" elements on the GPU
   * and create a curandState with different seed for each array entry.
   * If no seed is given create a seed based on current time.
   * Return success or error code
   */
  int cuda_createCurandStates(int size, int seed = -1);

  /**
   * Delete curandState.
   * Delete curandState array on the GPU and free memory.
   * Return success or error code
   */
  int cuda_deleteCurandStates();

  /** 
   * Create 'size' random numbers on the device and save in mem_ptr array.
   */
  int cuda_createRandomNumbers(void *mem_ptr, int size);

  /** 
   * Get a pointer to curand states.
   */
  curandState* cuda_getCurandStates();
	
  /**
   * Create a cuda stream and set streamId to index refering to this stream.
   * Return success or error code
   */
  int cuda_createStream(int &streamId);

  /**
   * add existing cuda stream to the list.
   * Return: success or error code.
   */
  int cuda_addStream(cudaStream_t tmpStream, int &streamId);

  /**
   * delete cuda stream.
   * success or error code
   */
  int cuda_deleteStream(int id);

  /**
   * delete all streams.
   * success or error code
   */
  int cuda_deleteStreams();

  /**
   * set stream to use.
   * success or error code
   */
  int cuda_setStream(int id);

  /**
   * get stream that is used.
   * Return: return id of curretn stream
   */
  int cuda_getStreamId();

  /**
   * reset to default stream.
   * Return: success or error code
   */
  int cuda_defaultStream();

  /**
   * get number of streams.
   * Return: success or error code
   */
  int cuda_numberOfStreams();

  /**
   * get stream.
   * Return: stream
   */
  cudaStream_t cuda_getStream(int id);

  /**
   * Get default cublass handle.
   */
  cublasHandle_t cuda_getCublas();

  /**
   * get information on cuda devices.
   * Return: success or error code
   */
  int cuda_getDevices();

  /** 
   * Get CUDA device count.
   * Sets the number of devices on the platform that can use CUDA.
   */
  int cuda_getDeviceCount(int &ndev);

  /** 
   * Get the name of the device.
   * QUery the device properties of the used device and set the string device_name
   */
  int cuda_getDeviceName(std::string &device_name);

  /** 
   * Set CUDA device to use.
   * If device passed in is larger than the number of devices use 
   * the default:0 and return DKS_ERROR 
   */
  int cuda_setDevice(int device);

  /** 
   * Get unique devices.
   * Get array of indeces with the unique CUDA devices available on the paltform
   */
  int cuda_getUniqueDevices(std::vector<int> &devices);
	
  /**
   * Initialize connection to the device.
   * Only needed when runtime compilation is used.
   * Return: success or error code
   */
  int cuda_setUp();
	
  /**
   * Allocate memory on cuda device.
   * Return: pointer to memory object
   */
  void * cuda_allocateMemory(size_t size, int &ierr);

  /**
   * Allocate host memory in pinned memory
   * Return: success or error code
   */
  template<typename T>
  int cuda_allocateHostMemory(T *&ptr, size_t size) {
    cudaError cerror;
    cerror = cudaMallocHost((void**)&ptr, sizeof(T)*size);
    if (cerror != cudaSuccess)
      return DKS_ERROR;
    
    return DKS_SUCCESS;
  }		

  /** 
   * Zero CUDA memory.
   * Set all the elements of the array on the device to zero.
   */
  template<typename T>
  int cuda_zeroMemory(T *mem_ptr, size_t size, int offset = 0) {
    cudaError cerror;
    cerror = cudaMemset(mem_ptr + offset, 0, sizeof(T) * size);
    if (cerror != cudaSuccess) {
      DEBUG_MSG("Error zeroing cuda memory!\n");
      return DKS_ERROR;
    }
    
    return DKS_SUCCESS;
  }

  /** 
   * Zero CUDA memory async.
   * Set all the elements of the array on the device to zero.
   */
  template<typename T>
  int cuda_zeroMemoryAsync(T *mem_ptr, size_t size, int offset = 0, int streamId = -1) {
    int dkserror = DKS_SUCCESS;
    cudaError cerror;
    if (streamId < cuda_numberOfStreams()) {
	cerror = cudaMemsetAsync(mem_ptr + offset, 0, sizeof(T) * size, 
				 cuda_getStream(streamId));
	  
	if (cerror != cudaSuccess)
	  dkserror = DKS_ERROR;
    } else
      dkserror = DKS_ERROR;
    
    return dkserror;
  }

  /** 
   * Write data to memory
   * Retrun: success or error code
   */
  template<typename T>
  int cuda_writeData(T * mem_ptr, const void * in_data, size_t size, int offset = 0) {
    cudaError cerror;
	
    cerror = cudaMemcpy(mem_ptr + offset, in_data, size, cudaMemcpyHostToDevice);
    if (cerror != cudaSuccess) {
      DEBUG_MSG("Error copying data to device, cuda error: " << cerror);
      return DKS_ERROR;
    }
	
    return DKS_SUCCESS;
  }

  /**
   * Write data assynchonuously
   * Return: success or error code
   */
  template<typename T>
  int cuda_writeDataAsync(T *mem_ptr, const void *in_data, size_t size, int streamId = -1, int offset = 0) {
    cudaError cerror;

    //if default stream or no stream specified, use default write method
    if (streamId == -1) {
      cuda_writeData(mem_ptr, in_data, size, offset);
      return DKS_SUCCESS;
    }

    if (streamId < cuda_numberOfStreams()) {
      //call async write
      cerror = cudaMemcpyAsync(mem_ptr + offset, in_data, size, cudaMemcpyHostToDevice,
			      cuda_getStream(streamId));
      
      if (cerror != cudaSuccess) {
	DEBUG_MSG("Error async data copy, cuda error: " << cerror);
	return DKS_ERROR;
      } 
    } else {
      DEBUG_MSG("Error invalid stream id: " << streamId);
      return DKS_ERROR;
    }
  

    return DKS_SUCCESS;
  }
		
  /**
   * Read data from memory
   * Return: success or error code
   */
  template<typename T>
  int cuda_readData(const T * mem_ptr, void * out_data, size_t size, int offset = 0) {
    cudaError cerror;
	
    cerror = cudaMemcpy(out_data, mem_ptr + offset, size, cudaMemcpyDeviceToHost);
    if (cerror != cudaSuccess) {
      DEBUG_MSG("Error reading data from device");
      return DKS_ERROR;
    }
	
    return DKS_SUCCESS;
  }

  /**
   * Read data async from device memory
   * Return: success or error code
   */
  template<typename T>
  int cuda_readDataAsync(const T *mem_ptr, void *out_data, size_t size, int streamId = -1, int offset = 0) {
    cudaError cerror;
    
    if (streamId == -1) {
      cerror = cudaMemcpyAsync(out_data, mem_ptr + offset, size, cudaMemcpyDeviceToHost, 0);
      if (cerror != cudaSuccess) {
	DEBUG_MSG("Error async read from devie default stream");
	return DKS_ERROR;
      }
      return DKS_SUCCESS;
    }

    if (streamId < cuda_numberOfStreams()) {
      cerror = cudaMemcpyAsync(out_data, mem_ptr + offset, size, cudaMemcpyDeviceToHost, 
			       cuda_getStream(streamId));
      if (cerror != cudaSuccess) {
	DEBUG_MSG("Error async read from device, cuda error: " << cerror);
	return DKS_ERROR;
      }
    } else {
      DEBUG_MSG("Error invalid stream id: " << streamId);
      return DKS_ERROR;
    }
    
    return DKS_SUCCESS;
  }
		
  /**
   * Free memory on device
   * Return: success or error code
   */
  int cuda_freeMemory(void * mem_ptr);
	
  /**
   * Free page locked memory on host
   * Return: success or erro code
   */
  int cuda_freeHostMemory(void * mem_ptr);
	
  /**
   * Allcate memory and write data (push)
   * Return: pointer to memory object
   */
  template<typename T>
  void * cuda_pushData(const void * in_data, size_t size, int &ierr) {
		
    void * mem_ptr;
    mem_ptr = cuda_allocateMemory(size, ierr);
	
    if (ierr == DKS_SUCCESS)
      ierr = cuda_writeData((T*)mem_ptr, in_data, size);
		
    return mem_ptr;
  }
		
  /**
   * Read data and free memory (pull)
   * Return: success or error code
   */
  template<typename T>
  int cuda_pullData(T * mem_ptr, void * out_data, size_t size, int &ierr) {

    ierr = cuda_readData(mem_ptr, out_data, size);
    if (ierr == DKS_SUCCESS)
      ierr = cuda_freeMemory(mem_ptr);	
    else
      return DKS_ERROR;
	
	
    if (ierr == DKS_SUCCESS)	
      return DKS_SUCCESS;
    else
      return DKS_ERROR;
  }
  
  /**
   * Sync cuda device.
   * Waits till all the tasks on the GPU are finished.
   * Return: success or error code
   */
  int cuda_syncDevice() {
    cudaDeviceSynchronize();
    return DKS_SUCCESS;
  }

  /**
   * Page-lock host memory.
   */
  template<typename T>
  int cuda_hostRegister(T *ptr, int size) {
    int cerr = cudaHostRegister(ptr, size*sizeof(T), cudaHostRegisterPortable);
    if (cerr == cudaSuccess) {
      return DKS_SUCCESS;
    } else {
      DEBUG_MSG("Host memroy was not page locked");
      return DKS_ERROR;
    }
  }

  /**
   * Release page locked memory.
   */
  template<typename T>
  int cuda_hostUnregister(T *ptr) {
    int cerr = cudaHostUnregister(ptr);
    if (cerr == cudaSuccess)
      return DKS_SUCCESS;
    else
      return DKS_ERROR;

  }

  /**
   * Print device memory info (total, used, avail)
   * Return: success or error code
   */
  int cuda_memInfo() {
    int ierr;
    size_t avail;
    size_t total;
    double mb = 1000000.0;
    
    ierr = cudaMemGetInfo( &avail, &total);
    
    if (ierr != cudaSuccess) {
      DEBUG_MSG("Device mem info could not be obtained!");
      return DKS_ERROR;
    }

    std::cout << "Device memory info, total: " << total / mb << "MB,\t";
    std::cout << "used: " << (total - avail) / mb << "MB,\t";
    std::cout << "avail: " << avail / mb << "MB" << std::endl;

    return DKS_SUCCESS;
  }

};

#endif
