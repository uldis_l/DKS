#ifndef H_DKS_IMAGERECONSTRUCTION
#define H_DKS_IMAGERECONSTRUCTION

#include <iostream>
#include "DKSBase.h"

#include "Algorithms/ImageReconstruction.h"

#ifdef DKS_CUDA
#include "CUDA/CudaImageReconstruction.cuh"
#endif

/**
 * API to handle PET image reconstruction calls.
 */
class DKSImageRecon : public DKSBase {

private:
  
  ImageReconstruction *imageRecon;

public:

  DKSImageRecon();

  ~DKSImageRecon();

  /** 
   * Image reconstruction analaysis calculate source.
   */
  int callCalculateSource(void *image_space, void *image_position, void *source_position, 
			  void *avg, void *std, float diameter, int total_voxels, 
			  int total_sources, int start = 0);

  /** 
   * Image reconstruction analaysis calculate source.
   */
  int callCalculateBackground(void *image_space, void *image_position, void *source_position, 
			      void *avg, void *std, float diameter, int total_voxels, 
			      int total_sources, int start = 0);


  /** 
   * Image reconstruction analaysis calculate source.
   */
  int callCalculateSources(void *image_space, void *image_position, void *source_position, 
			   void *avg, void *std, void *diameter, int total_voxels, 
			   int total_sources, int start = 0);

  /** 
   * Image reconstruction analaysis calculate source.
   */
  int callCalculateBackgrounds(void *image_space, void *image_position, void *source_position, 
			       void *avg, void *std, void *diameter, int total_voxels, 
			       int total_sources, int start = 0);

  /** 
   * Image reconstruction - generate normalization.
   */
  int callGenerateNormalization(void *recon, void *image_position, 
				void *det_position, int total_det);

  /** 
   * Image reconstruction - forward correction.
   */
  int callForwardProjection(void *correction, void *recon, void *list_data, void *det_position, 
			    void *image_position, int num_events);

  /** 
   * Image reconstruction - backward projection.
   */
  int callBackwardProjection(void *correction, void *recon_corrector, void *list_data, 
			     void *det_position, void *image_position, 
			     int num_events, int num_voxels);

  /** 
   * Set the voxel dimensins on device.
   * Values are stored in GPU memory and used in forward and backward projection calculations.
   * Call set function once to transfer the values from host side to GPU.
   * If value changes on the host side set functions needs to be called again to update GPU values.
   */
  int setDimensions(int voxel_x, int voxel_y, int voxel_z, float voxel_size);

  /** 
   * Set the image edge.
   * Values are stored in GPU memory and used in forward and backward projection calculations.
   * Call set function once to transfer the values from host side to GPU.
   * If value changes on the host side set functions needs to be called again to update GPU values.
   */
  int setEdge(float x_edge, float y_edge, float z_edge);

  /** 
   * Set the image edge1.
   * Values are stored in GPU memory and used in forward and backward projection calculations.
   * Call set function once to transfer the values from host side to GPU.
   * If value changes on the host side set functions needs to be called again to update GPU values.
   */
  int setEdge1(float x_edge1, float y_edge1, float z_edge1, float z_edge2);

  /** 
   * Set the minimum crystal in one ring values.
   * Values are stored in GPU memory and used in forward and backward projection calculations.
   * Call set function once to transfer the values from host side to GPU.
   * If value changes on the host side set functions needs to be called again to update GPU values.
   */
  int setMinCrystalInRing(float min_CrystalDist_InOneRing, float min_CrystalDist_InOneRing1);

  /** 
   * Set all other required parameters for reconstruction.
   * Values are stored in GPU memory and used in forward and backward projection calculations.
   * Call set function once to transfer the values from host side to GPU.
   * If value changes on the host side set functions needs to be called again to update GPU values.
   */
  int setParams(float matrix_distance_factor, float phantom_diameter,
		float atten_per_mm, float ring_diameter);





};


#endif
