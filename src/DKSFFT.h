#ifndef H_DKSBASE_FFT
#define H_DKSBASE_FFT

#include <iostream>
#include "AutoTuning/DKSAutoTuning.h"

#include "DKSBase.h"

#include "DKSDefinitions.h"

#include "Algorithms/GreensFunction.h"
#include "Algorithms/CollimatorPhysics.h"
#include "Algorithms/FFT.h"

#ifdef DKS_AMD
#include "OpenCL/OpenCLFFT.h"
#endif

#ifdef DKS_CUDA
#include "CUDA/CudaFFT.cuh"
#endif

#ifdef DKS_MIC
#include "MIC/MICFFT.h"
#endif

/**
 * API to handel calls to DKSFFT.
 * Using DKSFFT interface executes FFT on GPUs, CPUs and MICs using cuFFT, clFFT or MKL libraries.
 */
class DKSFFT : public DKSBase {

private:

  BaseFFT *dksfft;

  int initFFT();
  
public:
  
  DKSFFT();
  ~DKSFFT();

  /** 
   * Setup FFT function.
   * Initializes parameters for fft executuin. If ndim > 0 initializes handles for fft calls.
   * If ffts of various sizes are needed setupFFT should be called with ndim 0, in this case 
   * each fft will do its own setup according to fft size and dimensions.
   * TODO: opencl and mic implementations
   */
  int setupFFT(int ndim, int N[3]);
  //BENI:
  int setupFFTRC(int ndim, int N[3], double scale = 1.0);
  //BENI:
  int setupFFTCR(int ndim, int N[3], double scale = 1.0);

  /** 
   * Call complex-to-complex fft.
   * Executes in place complex to compelx fft on the device on data pointed by data_ptr.
   * stream id can be specified to use other streams than default.
   * TODO: mic implementation
   */
  int callFFT(void * data_ptr, int ndim, int dimsize[3], int streamId = -1);

  /** 
   * Call complex-to-complex ifft.
   * Executes in place complex to compelx ifft on the device on data pointed by data_ptr.
   * stream id can be specified to use other streams than default.
   * TODO: mic implementation.
   */
  int callIFFT(void * data_ptr, int ndim, int dimsize[3], int streamId = -1);

  /** 
   * Normalize complex to complex ifft.
   * Cuda, mic and OpenCL implementations return ifft unscaled, this function divides each element by 
   * fft size
   * TODO: mic implementation.
   */
  int callNormalizeFFT(void * data_ptr, int ndim, int dimsize[3], int streamId = -1);

  /** 
   * Call real to complex FFT.
   * Executes out of place real to complex fft, real_ptr points to real data, comp_pt - points
   * to complex data, ndim - dimension of data, dimsize size of each dimension. real_ptr size
   * should be dimsize[0]*dimsize[1]*disize[2], comp_ptr size should be atleast
   * (dimsize[0]/2+1)*dimsize[1]*dimsize[2]
   * TODO: opencl and mic implementations
   */
  int callR2CFFT(void * real_ptr, void * comp_ptr, int ndim, int dimsize[3], int streamId = -1);

  /** 
   * Call complex to real iFFT.
   * Executes out of place complex to real ifft, real_ptr points to real data, comp_pt - points
   * to complex data, ndim - dimension of data, dimsize size of each dimension. real_ptr size
   * should be dimsize[0]*dimsize[1]*disize[2], comp_ptr size should be atleast
   * (dimsize[0]/2+1)*dimsize[1]*dimsize[2]
   * TODO: opencl and mic implementations.
   */
  int callC2RFFT(void * real_ptr, void * comp_ptr, int ndim, int dimsize[3], int streamId = -1);

  /** 
   * Normalize compelx to real ifft.
   * Cuda, mic and OpenCL implementations return ifft unscaled, this function divides each element by 
   * fft size.
   * TODO: opencl and mic implementations.
   */
  int callNormalizeC2RFFT(void * real_ptr, int ndim, int dimsize[3], int streamId = -1);


};

#endif
