#include "DKSOPAL.h"

DKSOPAL::DKSOPAL() {
  dkscol = nullptr;
  dksgreens = nullptr;
}

DKSOPAL::DKSOPAL(const char* api_name, const char* device_name) {
  setAPI(api_name, strlen(api_name));
  setDevice(device_name, strlen(device_name));
}

DKSOPAL::~DKSOPAL() {
  delete dkscol;
  delete dksgreens;
}

int DKSOPAL::setupOPAL() {
  int ierr = DKS_ERROR;
  if (apiOpenCL()) {
    ierr = OPENCL_SAFECALL( DKS_SUCCESS );
    //TODO: only enable if AMD libraries are available
    dkscol = OPENCL_SAFEINIT_AMD( new OpenCLCollimatorPhysics(getOpenCLBase()) );
    dksgreens = OPENCL_SAFEINIT_AMD( new OpenCLGreensFunction(getOpenCLBase()) );
  } else if (apiCuda()) {
    ierr = CUDA_SAFECALL( DKS_SUCCESS );
    dkscol = CUDA_SAFEINIT( new CudaCollimatorPhysics(getCudaBase()) );
    dksgreens = CUDA_SAFEINIT( new CudaGreensFunction(getCudaBase()) );
  } else if (apiOpenMP()) {
    ierr = MIC_SAFECALL( DKS_SUCCESS );
    dkscol = MIC_SAFEINIT( new MICCollimatorPhysics(getMICBase()) );
    dksgreens = MIC_SAFEINIT( new MICGreensFunction(getMICBase()) );
  } else {
    ierr = DKS_ERROR;
  }

  return ierr;
}

int DKSOPAL::initDevice() {
  int ierr = setupDevice();
  if (ierr == DKS_SUCCESS)
    ierr = setupOPAL();
  return ierr;

}

int DKSOPAL::callGreensIntegral(void *tmp_ptr, int I, int J, int K, int NI, int NJ, 
				double hz_m0, double hz_m1, double hz_m2, int streamId) {

    return dksgreens->greensIntegral(tmp_ptr, I, J, K, NI, NJ, 
				     hz_m0, hz_m1, hz_m2, streamId);

}

int DKSOPAL::callGreensIntegration(void *mem_ptr, void *tmp_ptr, 
				   int I, int J, int K, int streamId) {

  return dksgreens->integrationGreensFunction(mem_ptr, tmp_ptr, I, J, K, streamId);
}

int DKSOPAL::callMirrorRhoField(void *mem_ptr, int I, int J, int K, int streamId) {

  return dksgreens->mirrorRhoField(mem_ptr, I, J, K, streamId);  
}

int DKSOPAL::callMultiplyComplexFields(void *mem_ptr1, void *mem_ptr2, int size, int streamId) {
  
  return dksgreens->multiplyCompelxFields(mem_ptr1, mem_ptr2, size, streamId);
}

int DKSOPAL::callCollimatorPhysics(void *mem_ptr, void *par_ptr, 
				   int numparticles, int numparams,
				   int &numaddback, int &numdead, 
				   bool enableRutherforScattering) 
{

  return dkscol->CollimatorPhysics(mem_ptr, par_ptr, numparticles, enableRutherforScattering);

}


int DKSOPAL::callCollimatorPhysics2(void *mem_ptr, void *par_ptr, int numparticles,
				    bool enableRutherforScattering) 
{

  return dkscol->CollimatorPhysics(mem_ptr, par_ptr, numparticles, enableRutherforScattering);
  
}

int DKSOPAL::callCollimatorPhysicsSoA(void *label_ptr, void *localID_ptr, 
				      void *rx_ptr, void *ry_ptr, void *rz_ptr, 
				      void *px_ptr, void *py_ptr, void *pz_ptr,
				      void *par_ptr, int numparticles)
{

  
    return dkscol->CollimatorPhysicsSoA(label_ptr, localID_ptr, 
					rx_ptr, ry_ptr, rz_ptr, 
					px_ptr, py_ptr, pz_ptr,
					par_ptr,  numparticles);

}


int DKSOPAL::callCollimatorPhysicsSort(void *mem_ptr, int numparticles, int &numaddback) 
{

  return dkscol->CollimatorPhysicsSort(mem_ptr, numparticles, numaddback);

}

int DKSOPAL::callCollimatorPhysicsSortSoA(void *label_ptr, void *localID_ptr, 
					  void *rx_ptr, void *ry_ptr, void *rz_ptr, 
					  void *px_ptr, void *py_ptr, void *pz_ptr,
					  void *par_ptr, int numparticles, int &numaddback) 
{

  return MIC_SAFECALL(dkscol->CollimatorPhysicsSortSoA(label_ptr, localID_ptr, 
						       rx_ptr, ry_ptr, rz_ptr, 
						       px_ptr, py_ptr, pz_ptr,
						       par_ptr,  numparticles, numaddback));

}


int DKSOPAL::callParallelTTrackerPush(void *r_ptr, void *p_ptr, int npart, 
				      void *dt_ptr, double dt, double c, 
				      bool usedt, int streamId) 
{

  return dkscol->ParallelTTrackerPush(r_ptr, p_ptr, npart, dt_ptr, dt, c, usedt, streamId);

}

int DKSOPAL::callParallelTTrackerPush(void *r_ptr, void *p_ptr, void *dt_ptr, 
				      int npart, double c, int streamId) {
  
  return dkscol->ParallelTTrackerPush(r_ptr, p_ptr, npart, dt_ptr, 0, c, true, streamId);

}

int DKSOPAL::callParallelTTrackerKick(void *r_ptr, void *p_ptr, void *ef_ptr,
				      void *bf_ptr, void *dt_ptr, double charge, double mass,
				      int npart, double c, int streamId) 
{
  
  return dkscol->ParallelTTrackerKick(r_ptr, p_ptr, ef_ptr, bf_ptr, dt_ptr, 
				      charge, mass, npart, c, streamId);

}

int DKSOPAL::callParallelTTrackerPushTransform(void *x_ptr, void *p_ptr, 
					       void *lastSec_ptr, void *orient_ptr, 
					       int npart, int nsec, void *dt_ptr, double dt, 
					       double c, bool usedt, int streamId)
{

  return dkscol->ParallelTTrackerPushTransform(x_ptr, p_ptr, lastSec_ptr, orient_ptr,
					       npart, nsec, dt_ptr, dt, c, usedt, streamId);
  
}
