#include "DKSImageReconstruction.h"

DKSImageRecon::DKSImageRecon() {

  //set up base. since reconstruction is always using cuda, set up base to CUDA
  setAPI("Cuda");
  setDevice("-gpu");
  initDevice();

  imageRecon = CUDA_SAFEINIT( new CudaImageReconstruction(getCudaBase()) );
}

DKSImageRecon::~DKSImageRecon() { 
  delete[] imageRecon;
}

int DKSImageRecon::callCalculateSource(void *image_space, void *image_position, 
				       void *source_position, void *avg, void *std, 
				       float diameter, int total_voxels, 
				       int total_sources, int start)
{
  int ierr;
  ierr = imageRecon->calculateSource(image_space, image_position, source_position, 
				     avg, std, diameter, total_voxels, 
				     total_sources, start);
  return ierr;
}

int DKSImageRecon::callCalculateBackground(void *image_space, void *image_position, 
					   void *source_position, void *avg, void *std, 
					   float diameter, int total_voxels, 
					   int total_sources, int start)
{

  int ierr;
  ierr = imageRecon->calculateBackground(image_space, image_position, 
					 source_position, avg, std, diameter, 
					 total_voxels, total_sources, start);
  return ierr;
}

int DKSImageRecon::callCalculateSources(void *image_space, void *image_position, 
					void *source_position, void *avg, void *std, 
					void *diameter, int total_voxels, 
					int total_sources, int start)
{
  int ierr;
  ierr = imageRecon->calculateSources(image_space, image_position, 
				      source_position, avg, std, diameter, 
				      total_voxels, total_sources, start);
  return ierr;
}

int DKSImageRecon::callCalculateBackgrounds(void *image_space, void *image_position, 
					    void *source_position, void *avg, void *std, 
					    void *diameter, int total_voxels, 
					    int total_sources, int start)
{
  
  int ierr;
  ierr = imageRecon->calculateBackgrounds(image_space, image_position, 
					  source_position, avg, std, diameter, 
					  total_voxels, total_sources, start);

return ierr;
}


int DKSImageRecon::callGenerateNormalization(void *recon, void *image_position, 
					     void *det_position, int total_det)
{
  
  int ierr = imageRecon->generateNormalization(recon, image_position, 
					       det_position, total_det);
  return ierr;
}


int DKSImageRecon::callForwardProjection(void *correction, void *recon, void *list_data, 
					 void *det_position, void *image_position, int num_events)
{

  int ierr;
  ierr = imageRecon->forwardProjection(correction, recon, list_data, det_position, 
				       image_position, num_events);
  return ierr;
}

int DKSImageRecon::callBackwardProjection(void *correction, void *recon_corrector, void *list_data, 
					  void *det_position, void *image_position, 
					  int num_events, int num_voxels)
{

  int ierr;
  ierr = imageRecon->backwardProjection(correction, recon_corrector, list_data, 
					det_position, image_position, num_events, 
					num_voxels);
  return ierr;
}

int DKSImageRecon::setDimensions(int voxel_x, int voxel_y, int voxel_z, float voxel_size) {
  int ierr = imageRecon->setDimensions(voxel_x, voxel_y, voxel_z, voxel_size);
  return ierr;
}

int DKSImageRecon::setEdge(float x_edge, float y_edge, float z_edge) {
  int ierr = imageRecon->setEdge(x_edge, y_edge, z_edge);
  return ierr;
}

int DKSImageRecon::setEdge1(float x_edge1, float y_edge1, float z_edge1, float z_edge2) {
  int ierr = imageRecon->setEdge1(x_edge1, y_edge1, z_edge1, z_edge2);
  return ierr;
}

int DKSImageRecon::setMinCrystalInRing(float min_CrystalDist_InOneRing, 
				       float min_CrystalDist_InOneRing1) 
{
  int ierr = imageRecon->setMinCrystalInRing(min_CrystalDist_InOneRing, 
					     min_CrystalDist_InOneRing1);
  return ierr;
}

int DKSImageRecon::setParams(float matrix_distance_factor, float phantom_diameter,
			     float atten_per_mm, float ring_diameter)
{
  int ierr = imageRecon->setParams(matrix_distance_factor, phantom_diameter,
				   atten_per_mm, ring_diameter);
  return ierr;
}
