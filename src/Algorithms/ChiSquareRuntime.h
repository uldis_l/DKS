#ifndef H_CHISQUARE_RUNTIME
#define H_CHISQUARE_RUNTIME

#include <iostream>
#include <string>
#include <sstream>
#include "../DKSDefinitions.h"

#define BLOCK_SIZE 128

#define FITTYPE_UNDEFINED    0
#define FITTYPE_SINGLE_HISTO 1
#define FITTYPE_ASYMMETRY    2
#define FITTYPE_MU_MINUS     3

class DKSBaseMuSR;

/** 
 * Interface to implement ChiSquareRuntime class for musrfit.
 */
class ChiSquareRuntime {
  friend class DKSBaseMuSR;

protected:
  // single histo fit parameter
  double N0_m;
  double tau_m;
  double bkg_m;
  // asymmetry fit parameter
  double alpha_m;
  double beta_m;

  bool initDone_m;
  void *mem_chisq_m;
  void *mem_param_m;
  void *mem_func_m;
  void *mem_map_m;

  int numBlocks_m;
  int blockSize_m;

  char *ptx_m;

  void setN0(double value) {
    N0_m = value;
  }

  void setTau(double value) {
    tau_m = value;
  }

  void setBKG(double value) {
    bkg_m = value;
  }

  void setAlpha(double value) {
    alpha_m = value;
  }

  void setBeta(double value) {
    beta_m = value;
  }

public:

  /** Default constructor */
  //ChiSquareRuntime();

  /** Default destructor. */
  virtual ~ChiSquareRuntime() { };

  /**
   * Compile GPU programm generated at runtime.
   */
  virtual int compileProgram(std::string function, bool mlh = false) = 0;

  /**
   * Launche the compiled chiSquare kernel.
   */
  virtual int launchChiSquare(int fitType, void *mem_data, void *mem_err, int length, 
			      int numpar, int numfunc, int nummap,
			      double timeStart, double timeStep,
			      double &result) = 0;

  /** 
   * Write the parameter values to the GPU.
   */
  virtual int writeParams(const double *params, int numparams) = 0;

  /**
   * Write the function values to the GPU.
   */
  virtual int writeFunc(const double *func, int numfunc) = 0;

  /**
   * Write map values to the GPU.
   */
  virtual int writeMap(const int *map, int nummap) = 0;

  /**
   * Allocate temporary memory needed for the chi square calucaltios on the device.
   */
  virtual int initChiSquare(int size_data, int size_param, int size_func, int size_map) = 0;

  /**
   * Free device memory allocated for chi square calculations.
   */
  virtual int freeChiSquare() = 0;

  /**
   * Check if available device can run the chi square GPU code.
   */
  virtual int checkChiSquareKernels(int fitType, int &threadsPerBlock) = 0;

  /** 
   * Set N0, tau and bgk values to use for the kernel.
   * If values changes between data sets this needs to be called before
   * every kernel call. Returns DKS_SUCCESS.
   */
  int setConsts(double N0, double tau, double bkg) {
    setN0(N0);
    setTau(tau);
    setBKG(bkg);
    
    return DKS_SUCCESS;
  }

  /** 
   * Set alpha and beta values to use for the kernel.
   * If values changes between data sets this needs to be called before
   * every kernel call. Returns DKS_SUCCESS.
   */
  int setConsts(double alpha, double beta) {
    setAlpha(alpha);
    setBeta(beta);
    return DKS_SUCCESS;
  }

  /** 
   * Set number of blocks and threads.
   * Used to set parameters obtained from auto-tuning
   */
  int setKernelParams(int numBlocks, int blockSize) {
    int ierr = DKS_ERROR;
    if (numBlocks > 0) {
      numBlocks_m = numBlocks;
      ierr = DKS_SUCCESS;
    }
    if (blockSize > 0) {
      blockSize_m = blockSize;
      ierr = DKS_SUCCESS;
    }

    return ierr;
  }

  /** 
   * Get the number of operations in compiled kernel.
   * Count the number of operation in the ptx file for the compiled program.
   */
  int getOperations(int &oper) {

    std::string ptx_str(ptx_m);
    std::istringstream is(ptx_str);

    std::string line;
    bool start = false;
    int count = 0;
    while(std::getline(is, line)) {
      
      //when fTheory start enable counting of operations
      size_t f1 = line.find("fTheory");
      size_t f2 = line.find(".visible");
      size_t f3 = line.find(";");
      if (f1 != std::string::npos && f2 != std::string::npos) {
	start = true;
	continue;
      }

      //exit when the new functions begins
      if (start && f2 != std::string::npos)
	break;

      //count opertations
      if (start && f3 != std::string::npos)
	count++;
    }

    oper = count;
    return DKS_SUCCESS;
  }

};

#endif
