#ifndef H_IMAGERECONSTRUCTION
#define H_IMAGERECONSTRUCTION

#include "../DKSDefinitions.h"

#define BLOCK_SIZE 128

/** Struct to hold voxel position for PET image. */
struct VoxelPosition {
  float x;
  float y;
  float z;
};

/** Struct that holds pair of detectors that registered an envent. */
struct ListEvent {
  unsigned detA : 16;
  unsigned detB : 16;
};

/**
 * Interface to implement PET image reconstruction.
 */
class ImageReconstruction {

protected:
  void *m_event_branch;

public:

  virtual ~ImageReconstruction() { }
  
  /** 
   * Caluclate source.
   *  Places a sphere at each voxel position and calculate the avg value and std value of pixels 
   *  that are inside this sphere. All the sphere used have the same diameter.
   */
  virtual int calculateSource(void *image_space, void *image_position, void *source_position, 
			      void *avg, void *std, float diameter, int total_voxels, 
			      int total_sources, int start = 0) = 0;

  /** 
   * Calculate background.
   * Places two sphere at each voxel position, calculates the avg value and std value of pixels
   * that are inside the larger sphere, but are outside of the smaller sphere. The diameter of the
   * smaller speher is given by parameter diameter, diameter of the larger sphere is 2*diameter.
   */
  virtual int calculateBackground(void *image_space, void *image_position, void *source_position, 
				  void *avg, void *std, float diameter, int total_voxels, 
				  int total_sources, int start = 0) = 0;

  /** 
   * Caluclate source using differente sources.
   * Places two sphere at each voxel position, calculates the avg value and std value of pixels
   * that are inside the larger sphere, but are outside of the smaller sphere. The diameter of the
   * each sphere is given by *diameter array.
   */
  virtual int calculateSources(void *image_space, void *image_position, void *source_position, 
			       void *avg, void *std, void *diameter, int total_voxels, 
			       int total_sources, int start = 0) = 0;

  /**
   * Places two sphere at each voxel position, calculates the avg value and std value of pixels.
   * that are inside the larger sphere, but are outside of the smaller sphere. The diameter of the
   * smaller sphere is given by *diameter array, diameter of the larger sphere is 2*diameter of the
   * smaller sphere.
   */
  virtual int calculateBackgrounds(void *image_space, void *image_position, void *source_position, 
				   void *avg, void *std, void *diameter, int total_voxels, 
				   int total_sources, int start = 0) = 0;

  /** 
   * Generate normalization.
   * Goes trough detectors pairs and if detector pair crosses image launches seperate kernel
   * that updates voxel values in the image on the slope between these two detectors.
   */
  virtual int generateNormalization(void *recon, void *image_position, 
				 void *det_position, int total_det) = 0; 


  /** 
   * Calculate forward projection.
   * For image reconstruction calculates forward projections.
   * see recon.cpp for details
   */
  virtual int forwardProjection(void *correction, void *recon, void *list_data, void *det_position, 
				void *image_position, int num_events) = 0;

  /** 
   * Calculate backward projection.
   * For image reconstruction calculates backward projections.
   * see recon.cpp for details
   */
  virtual int backwardProjection(void *correction, void *recon_corrector, void *list_data, 
				 void *det_position, void *image_position, 
				 int num_events, int num_voxels) = 0;

  /** 
   *Set the voxel dimensins on device. 
   */
  virtual int setDimensions(int voxel_x, int voxel_y, int voxel_z, float voxel_size) = 0;

  /** 
   * Set the image edge variables on the device.
   */
  virtual int setEdge(float x_edge, float y_edge, float z_edge) = 0;

  /** 
   * Set the image edge1 on the device.
   */
  virtual int setEdge1(float x_edge1, float y_edge1, float z_edge1, float z_edge2) = 0;

  /** 
   * Set the minimum crystan in one ring values on the device.
   */
  virtual int setMinCrystalInRing(float min_CrystalDist_InOneRing, 
				  float min_CrystalDist_InOneRing1) = 0;

  /** 
   * Set all other required parameters for reconstruction.
   */
  virtual int setParams(float matrix_distance_factor, float phantom_diameter,
			float atten_per_mm, float ring_diameter) = 0;
  
  
};

#endif
