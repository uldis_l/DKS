#ifndef H_DKS_FFT
#define H_DKS_FFT

#include <iostream>
#include <math.h>

#include "../DKSDefinitions.h"

/**
 * Abstract class defining methods for DKS FFT class.
 * Used by CudaFFT, OpenCLFFT and MICFFT to create device specific FFT classes.
 */
class BaseFFT {

protected:
  int defaultN[3];
  int defaultNdim;

  /**
   * Check if FFT plan is created for the needed dimension and FFT size.
   * Returns true if the plan has been created and false if no plan for specified dimension
   * and size exists.
   */
  bool useDefaultPlan(int ndim, int N[3]) {
    if (ndim != defaultNdim)
      return false;
    if (N[0] != defaultN[0] && N[1] != defaultN[1] && N[2] != defaultN[2])
      return false;
    return true;
  }

public:

  virtual ~BaseFFT() { }

  /** Setup FFT - init FFT library used by chosen device. */
  virtual int setupFFT(int ndim, int N[3]) = 0;

  /** Setup real to complex FFT - init FFT library used by chosen device. */
  virtual int setupFFTRC(int ndim, int N[3], double scale = 1.0) = 0;

  /** Setup real to complex complex to real FFT - init FFT library used by chosen device. */
  virtual int setupFFTCR(int ndim, int N[3], double scale = 1.0) = 0;

  /** Clean up. */
  virtual int destroyFFT() = 0;

  /** 
   * Exectute C2C FFT.
   * mem_ptr - memory ptr on the device for complex data.
   * Performs in place FFT.
   */
  virtual int executeFFT(void * mem_ptr, int ndim, int N[3], 
			 int streamId = -1, bool forward = true) = 0;

  /** 
   * Exectute inverse C2C FFT.
   * mem_ptr - memory ptr on the device for complex data.
   * Performs in place FFT.
   */
  virtual int executeIFFT(void * mem_ptr, int ndim, int N[3], int streamId = -1) = 0;

  /**
   * Normalize the FFT or IFFT.
   * mem_ptr - memory to complex data.
   */
  virtual int normalizeFFT(void * mem_ptr, int ndim, int N[3], int streamId = -1) = 0;

  /** 
   * Exectute R2C FFT.
   * real_ptr - real input data for FFT, comp_ptr - memory on the device where
   * results for the FFT are stored as complex numbers.
   */
  virtual int executeRCFFT(void * real_ptr, void * comp_ptr, int ndim, int N[3], 
				int streamId = -1) = 0;

  /** 
   * Exectute C2R FFT.
   * real_ptr - real output data from the C2R FFT, comp_ptr - complex input data for the FFT.
   */
  virtual int executeCRFFT(void * real_ptr, void * comp_ptr, int ndim, int N[3], 
				int streamId = -1) = 0;

  /**
   * Normalize CR FFT.
   */
  virtual int normalizeCRFFT(void *real_ptr, int ndim, int N[3], int streamId = -1) = 0;

};

#endif
