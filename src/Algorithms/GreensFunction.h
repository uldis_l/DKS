#ifndef H_GREENSFUNCTION
#define H_GREENSFUNCTION

#include <iostream>
#include <cmath>

/**
 * Interface to implement Greens function calculations for OPAL.
 */
class GreensFunction {

public:
  
  virtual ~GreensFunction() { }

  /** calc greens integral, as defined in OPAL. */
  virtual int greensIntegral(void *tmpgreen, int I, int J, int K, int NI, int NJ,
			     double hr_m0, double hr_m1, double hr_m2, int streamId = -1) = 0;

  /** integration if rho2_m, see OPAL for more details. */
  virtual int integrationGreensFunction(void * rho2_m, void *tmpgreen, int I, int J, int K, 
					int streamId = -1) = 0;

  /** mirror rho2_m field. */
  virtual int mirrorRhoField(void *rho2_m, int I, int J, int K, int streamId = -1) = 0;

  /** multiply two complex fields from device memory. */
  virtual int multiplyCompelxFields(void *ptr1, void *ptr2, int size, int streamId = -1) = 0;

};

#endif
