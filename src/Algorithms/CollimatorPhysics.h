#ifndef H_COLLIMATOR_PHYSICS
#define H_COLLIMATOR_PHYSICS

#include <iostream>
#include <string>
#include "../DKSDefinitions.h"

/**
 * Interface to impelment particle matter interaction for OPAL.
 */
class DKSCollimatorPhysics {

protected:

  int numBlocks_m;
  int blockSize_m;

public:
  
  virtual ~DKSCollimatorPhysics() { }

  /** 
   * Execute collimator physics kernel.
   *
   */  
  virtual int CollimatorPhysics(void *mem_ptr, void *par_ptr, int numpartices, 
				bool enableRutherforScattering = true) = 0;

  /** 
   * Special calse CollimatorPhysics kernel that uses SoA instead of AoS.
   * Used only on the MIC side, was not implemented on the GPU.
   */
  virtual int CollimatorPhysicsSoA(void *label_ptr, void *localID_ptr, 
				   void *rx_ptr, void *ry_ptr, void *rz_ptr, 
				   void *px_ptr, void *py_ptr, void *pz_ptr,
				   void *par_ptr, int numparticles) = 0;
  
  /** 
   * Sort particle array on GPU.
   * Count particles that are dead (label -1) or leaving material (label -2) and sort particle
   * array so these particles are at the end of array
   */
  virtual int CollimatorPhysicsSort(void *mem_ptr, int numparticles, int &numaddback) = 0;

  /** 
   * Special calse CollimatorPhysicsSort kernel that uses SoA instead of AoS.
   * Used only on the MIC side, was not implemented on the GPU.
   */
  virtual int CollimatorPhysicsSortSoA(void *label_ptr, void *localID_ptr, 
				       void *rx_ptr, void *ry_ptr, void *rz_ptr, 
				       void *px_ptr, void *py_ptr, void *pz_ptr,
				       void *par_ptr, int numparticles, int &numaddback) = 0;

  /** 
   * BorisPusher push function for integration from OPAL.
   * ParallelTTracker integration from OPAL implemented in cuda.
   * For more details see ParallelTTracler docomentation in opal
   */
  virtual int ParallelTTrackerPush(void *r_ptr, void *p_ptr, int npart, void *dt_ptr, 
				   double dt, double c, bool usedt = false, int streamId = -1) = 0;

  /** 
   * BorisPusher kick function for integration from OPAL.
   * ParallelTTracker integration from OPAL implemented in cuda.
   * For more details see ParallelTTracler docomentation in opal
   */
  virtual int ParallelTTrackerKick(void *r_ptr, void *p_ptr, void *ef_ptr,
				   void *bf_ptr, void *dt_ptr, double charge,
				   double mass, int npart, double c, int streamId = -1) = 0; 

  /** 
   * BorisPusher push function with transformto function form OPAL.
   * ParallelTTracker integration from OPAL implemented in cuda.
   * For more details see ParallelTTracler docomentation in opal
   */
  virtual int ParallelTTrackerPushTransform(void *x_ptr, void *p_ptr, void *lastSec_ptr, 
					    void *orient_ptr, int npart, int nsec, void *dt_ptr, 
					    double dt, double c, bool usedt = false, 
					    int streamId = -1) = 0;


};

#endif
