//AUTHOR: Benjamin Ulmer

#ifndef H_MIC_GREENS
#define H_MIC_GREENS

#include <iostream>
#include <complex>

#include <offload.h>
#include <mkl_dfti.h>

#include "../Algorithms/GreensFunction.h"
#include "MICBase.h"

#define DKS_SUCCESS 0
#define DKS_ERROR 1

/** OpenMP offload implementation of GreensFunction calculation for OPALs Poisson Solver. */
class MICGreensFunction : public GreensFunction {

private:
  MICBase *m_micbase;

public:

  /* constructor */
  MICGreensFunction(MICBase *base);

  /* destructor */
  ~MICGreensFunction();

  /* compute greens integral analytically */
  int greensIntegral(void * tmpgreen_, int I, int J, int K, int NI, int NJ,
		     double hr_m0, double hr_m1, double hr_m2, int streamId = -1);

  /* perform the actual integration */
  int integrationGreensFunction(void * rho2_m, void * tmpgreen,int I,int J, int K, 
				int stremaId = -1);

  /* Mirror rho-Field */
  int mirrorRhoField(void * rho2_m, int I, int J, int K, int streamId = -1);

  /*multiply complex fields*/
  int multiplyCompelxFields(void * ptr1, void * ptr2, int size, int streamId = -1);

};

#endif
