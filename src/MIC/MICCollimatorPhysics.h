#ifndef H_MIC_COLLIMATORPHYSICS
#define H_MIC_COLLIMATORPHYSICS

#include <iostream>
#include <cstdio>
#include <cmath>
#include <omp.h>
#include <vector>

#include "../Algorithms/CollimatorPhysics.h"
#include "MICBase.h"

__declspec(target(mic))
typedef struct {
  double x;
  double y;
  double z;
} mic_double3;

__declspec(target(mic))
typedef struct {
  int label;
  unsigned localID;
  mic_double3 Rincol;
  mic_double3 Pincol;
} MIC_PART_SMALL;


/**
 * MICCollimatorPhysics class based on DKSCollimatorPhysics interface.
 * Implementes OPALs collimator physics class for particle matter interactions using OpenMP
 * and offload mode targetomg Intel Xeon Phi processors.
 * For detailed documentation on CollimatorPhysics functions see OPAL documentation.
 */
class MICCollimatorPhysics : public DKSCollimatorPhysics {

private:

  MICBase *m_micbase;

public:

  MICCollimatorPhysics(MICBase *base) {
    m_micbase = base;
  };

  ~MICCollimatorPhysics() {  };

  int CollimatorPhysics(void *mem_ptr, void *par_ptr, int numparticles, 
			bool enableRutherforScattering = true);

  int CollimatorPhysicsSoA(void *label_ptr, void *localID_ptr, 
			   void *rx_ptr, void *ry_ptr, void *rz_ptr, 
			   void *px_ptr, void *py_ptr, void *pz_ptr,
			   void *par_ptr, int numparticles);

  int CollimatorPhysicsSort(void *mem_ptr, int numparticles, int &numaddback);

  int CollimatorPhysicsSortSoA(void *label_ptr, void *localID_ptr, 
			       void *rx_ptr, void *ry_ptr, void *rz_ptr, 
			       void *px_ptr, void *py_ptr, void *pz_ptr,
			       void *par_ptr, int numparticles, int &numaddback);

  int ParallelTTrackerPush(void *r_ptr, void *p_ptr, int npart, void *dt_ptr, 
			   double dt, double c, bool usedt = false, int streamId = -1);

  int ParallelTTrackerPushTransform(void *x_ptr, void *p_ptr, void *lastSec_ptr, 
				    void *orient_ptr, int npart, int nsec, 
				    void *dt_ptr, double dt, double c,
				    bool usedt = false, int streamId = -1);

};


#endif
