#include <iostream>
#include <cstdlib>
#include <omp.h>

/* default comparison function */
template<typename T>
inline bool greaterThan(T x, T y) {
  return x > y;
}

/* swap a and b */
template<typename T>
void mergeswap(T &a, T &b) {
  T tmp = a;
  a = b;
  b = tmp;
}

template <typename T>
void split_merge(T *a, int ibegin, int iend, T *b, bool (*comp)(T, T) ) {

  if (iend - ibegin < 500) {
    quick_sort(a + ibegin, 0, iend - ibegin - 1, comp);
    return;
  }

  int imiddle = (iend + ibegin) / 2;

#pragma omp task
  split_merge(a, ibegin, imiddle, b, comp);
  split_merge(a, imiddle, iend, b, comp);
#pragma omp taskwait
  
  merge(a, ibegin, imiddle, iend, b, comp);

}

template <typename T>
void merge(T *a, int ibegin, int imiddle, int iend, T *b, bool (*comp)(T, T)) {

  int i0 = ibegin;
  int i1 = imiddle;

  //merge two halfs of array a to tmp array b
  int i = ibegin;
  while (i < iend) {
    if (i0 < imiddle && ( i1 >= iend || comp(a[i1], a[i0]) ) )
      b[i++] = a[i0++];
    else
      b[i++] = a[i1++];
  }
  
  //copy b back to a
  for (int i = ibegin; i < iend; i++)
    a[i] = b[i];

}

template <typename T>
int partition(T *a, int start, int end, bool (*comp)(T, T) ) {
  int p = start;
  T x = a[start];

  for (int i = start + 1; i <= end; i++) {
    if ( comp(x, a[i]) ) {
      p++;
      mergeswap(a[i], a[p]);
    }
  }
  mergeswap(a[p], a[start]);
  return p;
}

/**
 * Merge sort implementation for intel MIC.
 * Paralellized over all the MIC cores using OpenMP tasks.
 */
template <typename T>
void merge_sort( T *list, int n, bool (*comp)(T, T) = greaterThan) {

#pragma omp parallel
  {
#pragma omp single
    {
      T *b = new T[n];
      split_merge(list, 0, n, b, comp);
    }
  }
}

/**
 * Quicksort algorithm, developed for use on Intel MIC devices.
 */
template <typename T>
void quick_sort( T *list, int start, int end, bool (*comp)(T, T) ) {

  if (start < end) {
    //for small elements move to insertion sort
    if ( (end - start) < 9 ) {
      insertion_sort(list, start, end + 1, comp);
    } else {
      int part = partition(list, start, end, comp);
      quick_sort(list, start, part - 1, comp);
      quick_sort(list, part + 1, end, comp);
    }
  }

}

/** 
 * Insertion sort of @p list, developed for use on Intel MIC.
 * Used by quick_sort to sort small lists.
 */
template <typename T>
void insertion_sort( T *list, int start, int end, bool (*comp)(T, T) ) {

  for (int i = start + 1; i < end; i++) {
    T key = list[i];
    int j = i - 1;
    while ( j >= 0 && comp(list[j], key) ) {
      list[j + 1] = list[j];
      j--;
    }
    list[j + 1] = key;
  }

}
