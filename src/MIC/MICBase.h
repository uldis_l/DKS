/*

  Name: MIC Base
  Author: Uldis Locans
  Info: class to handle set up and data transfer from host to Intel MIC devices
  Date: 29.09.2014

*/
#ifndef H_MIC_BASE
#define H_MIC_BASE

#include <iostream>
#include <omp.h>
#include <offload.h>
#include <mkl_dfti.h>
#include <mkl_vsl.h>
#include <vector>
#include <time.h>

#include "../DKSDefinitions.h"

#define DKS_ALLOC alloc_if(1)
#define DKS_FREE free_if(1)
#define DKS_RETAIN free_if(0)
#define DKS_REUSE alloc_if(0)

#define MIC_WIDTH 128

/** MIC Base class handles device setup and basic communication with the device.
 * Handles devicew setup, memory manegement and  data transfers.
 */
class MICBase {

private:
  std::vector<int> micStreams;
  int maxThreads; 

protected:

  int defaultRndSet;
public:

//#pragma offload_attribute(push,target(mic))
  void *defaultRndStream; //VSLSStreamStatePtr
  void *testPtr;

//#pragma offload_attribute(pop)

  int m_device_id;

  /** constructor */
  MICBase();

  /** destructor */
  ~MICBase();

  /**
   * Create MKL rand streams for each thread
   *  Return: success or error code
   */
  int mic_createRandStreams(int size);

  /**
   * Delete MKL rand streams
   * Return: succes or error code
   */
  int mic_deleteRandStreams();

  /**
   * Create a new signal for the mic.
   * Signals can be used for assynchronous data transfers.
   * Return: success or error code
   */
  int mic_createStream(int & streamId);

  /**
   * Info: get the signal from the vector.
   * Return: mic signal
  */
  int& mic_getStream(int id);

  /**
   * Info: delete streams.
   * Return: success or error code
   */
  int mic_deleteStreams();

  /**
   * Info: set device id.
   * Return: success or error code
   */
  int mic_setDeviceId(int id);

  /**
   * Info: get mic devices.
   * Prints information about mic devices.
   * Return: success or error code
   */
  int mic_getDevices();

  /**
   * Allocate memory on MIC device.
   * Return: success or error code
   */
  template<typename T>
  void * mic_allocateMemory(int size) {

    int padding = size % MIC_WIDTH;
    int totalsize = size + padding;

    T *tmp = (T*)_mm_malloc(sizeof(T)*totalsize, 64); // = new T[size];
#pragma offload_transfer target(mic:m_device_id) nocopy(tmp:length(totalsize) DKS_ALLOC DKS_RETAIN)

    return tmp;
  }

  /**
   * Transfer data to device.
   * Return: success or error code
   */
  template<typename T>
  int mic_writeData(void * data_ptr, const void * data, int size, int offset = 0) {
    T* tmp_ptr = (T*)data_ptr;
    T* tmp_data = (T*)data;

#pragma offload_transfer target(mic:m_device_id) in(tmp_data[0:size] : DKS_REUSE DKS_RETAIN into(tmp_ptr[offset:size]) )

    return DKS_SUCCESS;
  }

  /**
   * Write data to device, non-blocking.
   * Return: success or error code
   */
  template<typename T>
  int mic_writeDataAsync(void * data_ptr, const void * data, int size, int streamId = -1, int offset = 0) 
  {
    T* tmp_ptr = (T*)data_ptr;
    T* tmp_data = (T*)data;

#pragma offload_transfer target(mic:m_device_id) in(tmp_data[0:size] : DKS_REUSE DKS_RETAIN into(tmp_ptr[offset:size]) )

    return DKS_SUCCESS;
  }
  

  /**
   * Read data from device
   * Return: success or error code
   */
  template<typename T>
  int mic_readData(const void * data_ptr, void * result, int size, int offset = 0) {
    T* tmp_ptr = (T*)data_ptr;
    T* tmp_result = (T*)result;
	
	//std::cout << "try to read data with size = " << size << " adn offset = " << offset << std::endl;
#pragma offload_transfer target(mic:m_device_id) out(tmp_ptr[offset:size] : DKS_REUSE DKS_RETAIN into(tmp_result[0:size]) )

    return DKS_SUCCESS;
  }

  /**
   * Read data from device waiting for signal
   * Return: success or error code
   */
  template<typename T>
  int mic_readDataAsync(const void * data_ptr, void * result, int size, 
			int streamId = -1, int offset = 0) {
    T* tmp_ptr = (T*)data_ptr;
    T* tmp_result = (T*)result;

#pragma offload_transfer target(mic:m_device_id) out(tmp_ptr[offset:size] : DKS_REUSE DKS_RETAIN into(tmp_result[0:size]) ) 
      {
      }

    return DKS_SUCCESS;

  }

  /**
   * Wait till all the signals are complete
   * Return siccess or error code
   */
  int mic_syncDevice() {
    
    //empty offload to wait for all the signals to finish and launch a new empy signal
    /*
    for (int i = 0; i < micStreams.size(); i++) {
#pragma offload target(mic:m_device_id) wait(mic_getStream(i)) signal(mic_getStream(i))
      {
      }
    }
    */

	//std::cout << "done read data" << std::endl;

    return DKS_SUCCESS;

  }

  /**
   * Free memory on device
   * Return: success or error code
   */
  template<typename T>
  int mic_freeMemory(void * data_ptr, int size) {

    int padding = size % MIC_WIDTH;
    int totalsize = size + padding;

    T* tmp_ptr = (T*)data_ptr;
#pragma offload_transfer target(mic:m_device_id) nocopy(tmp_ptr:length(totalsize) DKS_REUSE DKS_FREE)
    {
    }
    return DKS_SUCCESS;
  }

  /**
   * Allocate memory and write data to device
   * Return: success or error code
   */
  template<typename T>
  void * mic_pushData(const void * data, int size) {
    T* tmp_ptr = new T[size];
    T* tmp_data = (T*)data;

#pragma offload_transfer target(mic:m_device_id) in(tmp_data[0:size] : DKS_ALLOC DKS_RETAIN 
    into(tmp_ptr[0:size]) )
  {
  }

  return tmp_ptr;
}

  /**
   * Read data and free memory on device
   * Return: success or erro code
   */
  template<typename T>
  int mic_pullData(void * data_ptr, void * result, int size) {
    T* tmp_ptr = (T*)data_ptr;
    T* tmp_data = (T*)result;

#pragma offload_transfer target(mic:m_device_id) out(tmp_ptr[0:size] : DKS_REUSE DKS_FREE into(tmp_data[0:size]) )
    {
    }

    return DKS_SUCCESS;
  }

};

#endif
