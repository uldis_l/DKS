#ifndef H_MIC_FFT
#define H_MIC_FFT

#include <iostream>
#include <complex>

#include <offload.h>
#include <mkl_dfti.h>

#include "../Algorithms/FFT.h"
#include "MICBase.h"

/** 
 * MIC FFT based on BaseFFT interface.
 * uses MKL library to offload FFT on Intel Xeon Phi devices.
 */
class MICFFT : public BaseFFT {

private:

  bool m_fftsetup;
  MICBase *m_micbase;

  /// Internal FFT object for performing serial FFTs.
#pragma offload_attribute(push,target(mic))
  DFTI_DESCRIPTOR_HANDLE FFTHandle_m; //declspec only works for global variables
  DFTI_DESCRIPTOR_HANDLE handle;
  DFTI_DESCRIPTOR_HANDLE rc_handle; //handle for REAL->COMPLEX
  DFTI_DESCRIPTOR_HANDLE cr_handle; //handle for COMPLEX->REAL

#pragma offload_attribute(pop)

  __attribute__((target(mic:0))) DFTI_DESCRIPTOR_HANDLE& getHandle(void) { 
    return FFTHandle_m; 
  }

  __attribute__((target(mic:0))) DFTI_DESCRIPTOR_HANDLE& getHandle1(void) { 
    return handle; 
  }

  __attribute__((target(mic:0))) DFTI_DESCRIPTOR_HANDLE& getHandleRC(void) { 
    return rc_handle; 
  }

  __attribute__((target(mic:0))) DFTI_DESCRIPTOR_HANDLE& getHandleCR(void) { 
    return cr_handle; 
  }

public:

  /* constructor */
  MICFFT(MICBase *base);

  /* destructir */
  ~MICFFT();

  /* 
     Info: setup mkl fft
     Return: success or error code
  */ 
  int setupFFT(int ndim, int N[3]);
  //BENI: 
  int setupFFTRC(int ndim, int N[3], double scale = 1.0);
  //BENI: 
  int setupFFTCR(int ndim, int N[3], double scale = 1.0);

  /* execute FFT on MIC */
  int executeFFT(void *mem_ptr, int ndim, int N[3], int streamId = -1, bool forward = true);

  /* execute IFFT on MIC */
  int executeIFFT(void *mem_ptr, int ndim, int N[3], int streamId = -1);

  /* execute REAL->COMPLEX FFT on MIC */
  int executeRCFFT(void *in_ptr, void *out_ptr, int ndim, int N[3], int streamId = -1);

  /* execute COMPLEX->REAL FFT on MIC */
  int executeCRFFT(void *in_ptr, void *out_ptr, int ndim, int N[3], int streamId = -1);

  /* normalize IFFT on MIC */
  int normalizeFFT(void *mem_ptr, int ndim, int N[3], int streamId = -1);

  /**
   * Info: destroy default FFT plans
   * Return: success or error code
   */
  int destroyFFT() { return DKS_SUCCESS; }

  /*
    Info: execute normalize for complex to real iFFT
    Return: success or error code
  */
  int normalizeCRFFT(void *real_ptr, int ndim, int N[3], int streamId = -1) { return DKS_SUCCESS; }

};

#endif
