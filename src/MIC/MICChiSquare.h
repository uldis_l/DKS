/*

  Name: MICChiSquare
  Info: calculate chi^2 using intel mic coporcessor
  Author: Uldis Locans
  Date: 29.09.2014

*/
#ifndef H_MIC_CHI_SQUARE
#define H_MIC_CHI_SQUARE

#include <math.h>
#include <omp.h>
#include <offload.h>
#include "MICBase.h"

/** Deprecated, OpenMP + offload to Xeon Phi implementation of ChiSquare for MIC devices. 
 * Not complete and untested because of the poor performance of first MIC devices.
 */
class MICChiSquare {

  MICBase *m_micbase;

public:

  /* constructor */
  MICChiSquare(MICBase *base) { 
    m_micbase = base;
  }

  /* destructor */
  ~MICChiSquare() { }

  /*
    Info: calucate chi square
    Return: success or error code
  */
  int mic_chi2(double *O, double *E, double *result, int size);

  /*
    Info: calculate Nt function
    Return: success or error code
  */
  int mic_Nt(double *nt, double *p, int psize, int nsize, int jsize, double deltaT = 1);

  /*
    Info: calculate sum of array
    Return: success or error code
  */
  int mic_sum(double *data, double *result, int size);

};

#endif
