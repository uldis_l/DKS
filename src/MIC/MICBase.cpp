#include "MICBase.h"

//constructor, sets default device id equal to 0
MICBase::MICBase() {
  m_device_id = 0;
  defaultRndSet = -1;

}

//destructor, delete defaultrnd streams if they are set
MICBase::~MICBase() {
  mic_deleteRandStreams();
}


//create default rand streams
int MICBase::mic_createRandStreams(int size) {

  int seed = time(NULL);

  int numThreads = 0;
#pragma offload target(mic:m_device_id) inout(numThreads)
  {
#pragma omp parallel
    numThreads = omp_get_num_threads();
  }

  defaultRndStream =  mic_allocateMemory<VSLStreamStatePtr>(numThreads);
  VSLStreamStatePtr *tmpRndStream = (VSLStreamStatePtr*) defaultRndStream;
  maxThreads = numThreads; 
  
#pragma offload target(mic:m_device_id) \
  in(tmpRndStream:length(0) DKS_REUSE DKS_RETAIN)        \
  in(seed)
  {
    //create stream states for each thread
#pragma omp parallel for
    for (int i = 0; i < omp_get_num_threads(); i++)
      vslNewStream(&tmpRndStream[i], VSL_BRNG_MT2203, seed + i);
  }

  defaultRndSet = 1;
  return DKS_SUCCESS;

}

//delete default rand streams
int MICBase::mic_deleteRandStreams() {

  //mic_freeMemory<VSLStreamStatePtr>(defaultRndStream, 236);
  return DKS_SUCCESS;
}

//create a new signal for the mic
int MICBase::mic_createStream(int & streamId) {

  //use int as signal, create a new int in micStreams vector, return the id
  int tmpStream = micStreams.size();
  micStreams.push_back(tmpStream);
  streamId = micStreams.size() - 1;

  //empty offload to create the signal on the mic
  /*
#pragma offload target(mic:m_device_id) signal(mic_getStream(streamId))
  {
  }
  */
  return DKS_SUCCESS;
}

//get the signal from the vector
int& MICBase::mic_getStream(int id) {
  return micStreams[id];
}

//delete streams
int MICBase::mic_deleteStreams() {
  micStreams.clear();

  return DKS_SUCCESS;
}


//sets device id
int MICBase::mic_setDeviceId(int id) {
  m_device_id = id;

  return DKS_SUCCESS;
}

//get information abaut all available mic devices
//TODO: find a way to check system for avaialbel mic devices

int MICBase::mic_getDevices() {

  int devices = _Offload_number_of_devices();
  int thread_count = 0;

  std::cout << "==============================" << std::endl;
  std::cout << "==========Intel MICs==========" << std::endl;
  std::cout << "==============================" << std::endl;

  std::cout << "Total mic devices: " << devices << std::endl;
  //std::cout << "Total mic devices: currently cant be found, but it's 1 on kraftwerk" << std::endl;

#pragma offload target(mic:m_device_id) inout(thread_count)
  {
    thread_count = omp_get_max_threads();
  }

  std::cout << "Max threads: " << thread_count << std::endl;


  return DKS_SUCCESS;
}
