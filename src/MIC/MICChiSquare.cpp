#include "MICChiSquare.h"

/*
  calculate chi^2 on intel mic, use data already loaded on device
*/
int MICChiSquare::mic_chi2(double *O, double *E, double *result, int size) {

#pragma offload target(mic:m_micbase->m_device_id)		\
  in(O:length(0) DKS_RETAIN DKS_REUSE)			\
  in(E:length(0) DKS_RETAIN DKS_REUSE)			\
  in(result:length(0) DKS_RETAIN DKS_REUSE)		\
  in(size)
  {	
#pragma omp parallel for
    for (int i = 0; i < size; i++) {
      result[i] = pow(O[i] - E[i], 2) / E[i];		
    }
  }

  return DKS_SUCCESS;
}


/*
  calculate function N(t), use data already loaded on device
*/
int MICChiSquare::mic_Nt(double *nt, double *p, int psize, int nsize, int jsize, double deltaT) {

#pragma offload target(mic:m_micbase->m_device_id)		\
  in(nt:length(0) DKS_RETAIN DKS_REUSE)			\
  in(p:length(0) DKS_RETAIN DKS_REUSE)			\
  in(psize) in(nsize) in(jsize) in(deltaT)
  {

    double gamma = 0.01; //???
    double tau = 0.01; //???

    for (int j = 0; j < jsize; j++) {

      int pid = j*psize;
      double N0 = p[pid];
      double Nbkg = p[pid+1];
      double A0 = p[pid+2];
      double phi = p[pid+3];
      double sigma = p[pid+4];
      double B = p[pid+5];

      int idj = j*nsize;

      double a1 = -0.5*sigma*sigma;
      double b1 = gamma*B;

#pragma omp parallel for	
      for (int n = 0; n < nsize; n++) {

	int id = idj + n;
	double t = n*deltaT;

	double a = a1*t*t;
	double b = b1*t + phi;
	double At = A0 * exp2(a) * cos(b);

	double c = -t/tau;
	double Nt = N0 * exp2(c) * (1 + At) + Nbkg;

	nt[id] = Nt;
      }
    }

  }

  return DKS_SUCCESS;
}

/*
  calculate sum of array
*/
int MICChiSquare::mic_sum(double *data, double *result, int size) {
  double sum = 0;
#pragma offload target(mic:m_micbase->m_device_id)		\
  in(data:length(0) DKS_REUSE DKS_RETAIN)		\
  in(result:length(0) DKS_REUSE DKS_RETAIN)		\
  in(size) in(sum)
  {
#pragma omp parallel for reduction(+:sum)
    for (int i = 0; i < size; i++) {
      sum += data[i];
    }
    result[0] = sum;
  }
  return DKS_SUCCESS;
}

