#ifndef H_DKSTIMER
#define H_DKSTIMER

#include <iostream>
#include <string>
#include <sys/time.h>

/**
 * Custom timer class.
 * Allows to insert timers in the code to get function exectution times.
 */
class DKSTimer {

private:

  bool running;
  double timervalue;
  struct timeval timeStart;
  struct timeval timeEnd;
  std::string name;

public:

  /** Init DKSTimer by seting timer to zero. */
  DKSTimer();

  ~DKSTimer();

  /** 
   * Init the timer.
   * Set the name for timer and clear all values
   */
  void init(std::string n);

  /** 
   * Start the timer.
   * Get the curret time with gettimeofday and save in timeStart
   */
  void start();

  /** 
   * Stop the timer.
   * Get the curretn time with gettimeofday and save in timeEnd
   * Calculate elapsed time by timeEnd - timeStart and add to timervalue
   */
  void stop();

  /** 
   * Reset timervalue to zero.
   * Set timervalue, timeStart and timeEnd to zero
   */
  void reset();

  /** 
   * Return elapsed time in seconds.
   * Return the value of timervalue
   */
  double gettime();

  /** 
   * Print timer.
   * Print the elapsed time of the timer
   */
  void print();
     

};

#endif
