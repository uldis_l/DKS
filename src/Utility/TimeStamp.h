#ifndef H_TIMESTAMPE
#define H_TIMESTAMPE

#include <iostream>
#include <time.h>
#include <sys/time.h>

typedef unsigned long long timestamp_t;

timestamp_t get_timestamp();
double get_secs(timestamp_t t_start, timestamp_t t_end);


#endif