#include "DKSTimer.h"

//set initial values - running to false, timervalue to zero and name to empty string
DKSTimer::DKSTimer() {
  running = false;
  timervalue = 0.0;
  name = "";
}

//destructor does nothing
DKSTimer::~DKSTimer() {

}

//init the timer by setting name and clearing timervalue, also sets running to false
void DKSTimer::init(std::string n) {
  running = false;
  timervalue = 0.0;
  name = n;
}

//if timer is not running get the current time and save to timeStart, set the timer as running
void DKSTimer::start() {
  if (!running) {
    gettimeofday(&timeStart, NULL);
    running = true;
  }
}

//if the timer is running get the current time to timeEnd, calculate the elapsed time befor start
//and end, add elapsed time to timervalue, set the timer as not running
void DKSTimer::stop() {
  if (running) {
    gettimeofday(&timeEnd, NULL);
    timervalue += ( (timeEnd.tv_sec - timeStart.tv_sec) * 1000000 + 
		    (timeEnd.tv_usec - timeStart.tv_usec)) * 1e-6;
    running = false;
  }
}

void DKSTimer::reset() {
  running = false;
  timervalue = 0.0;
}

//return the accumulated value of timervalue
double DKSTimer::gettime() {
  return timervalue;
}

void DKSTimer::print() {
  std::cout << "DKSTimer " << name << " elapsed time\t" << timervalue << "s" << std::endl;
}
