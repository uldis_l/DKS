#include "TimeStamp.h"

timestamp_t get_timestamp() {
    struct timeval now;
    gettimeofday (&now, NULL);
    return now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}

double get_secs(timestamp_t t_start, timestamp_t t_end) {
    return (t_end - t_start) / 1000000.0L;
}