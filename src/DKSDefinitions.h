#ifndef H_DKS_DEFINITIONS
#define H_DKS_DEFINITIONS

#define API_OPENCL "OpenCL"
#define API_CUDA "Cuda"
#define API_OPENMP "OpenMP"
#define API_UNKNOWN "Unknown"

#define DEVICE_GPU_NEW "GPU"
#define DEVICE_CPU_NEW "CPU"
#define DEVICE_MIC_NEW "MIC"
#define DEVICE_UNKNOWN_NEW "Unknown"

#define DEVICE_GPU "-gpu"
#define DEVICE_CPU "-cpu"
#define DEVICE_MIC "-mic"

//define macro for printing debug messages if debug flag is set
#ifdef DEBUG
#define DEBUG_MSG(x) (std::cout << x << std::endl)
#else
#define DEBUG_MSG(x)
#endif

//define DKS error codes
#define DKS_SUCCESS 0
#define	DKS_ERROR 1
#define DKS_API_NOT_ENABLED 100

#define OCL_SUCCESS 0
#define OCL_ERROR 1

//define macros to enable or disable calls to specific frameworks
//if framework specific flag is set execute the satement, of not give DKS_API_NOT_ENABLED error
#ifdef DKS_CUDA
#define CUDA_SAFECALL(...) ( __VA_ARGS__ )
#else
#define CUDA_SAFECALL(...) ( DKS_API_NOT_ENABLED )
#endif

#ifdef DKS_OPENCL
#define OPENCL_SAFECALL(...) ( __VA_ARGS__ )
#else
#define OPENCL_SAFECALL(...) ( DKS_API_NOT_ENABLED )
#endif

#ifdef DKS_MIC
#define MIC_SAFECALL(...) ( __VA_ARGS__ )
#else
#define MIC_SAFECALL(...) ( DKS_API_NOT_ENABLED )
#endif

#ifdef DKS_CUDA
#define CUDA_SAFEINIT(x) ( x )
#else
#define CUDA_SAFEINIT(x) ( NULL )
#endif

#ifdef DKS_OPENCL
#define OPENCL_SAFEINIT(x) ( x )
#else
#define OPENCL_SAFEINIT(x) ( NULL )
#endif

#ifdef DKS_AMD
#define OPENCL_SAFEINIT_AMD(x) ( x )
#else
#define OPENCL_SAFEINIT_AMD(x) ( NULL )
#endif

#ifdef DKS_MIC
#define MIC_SAFEINIT(x) ( x )
#else
#define MIC_SAFEINIT(x) ( NULL )
#endif

#endif
