#include <iostream>
#include <string>
#include <vector>
#include <sys/time.h>

#include "DKSBase.h"

using namespace std;

int main(int argc, char *argv[]) {

  int size = 10;
  bool apiSet = false;
  char *api_name = new char[10];
  char *device_name = new char[10];

  for (int i = 1; i < argc; i++) {

    if (argv[i] == string("-cuda")) {
      strcpy(api_name, "Cuda");
      strcpy(device_name, "-gpu");
      apiSet = true;
    }

    if (argv[i] == string("-opencl")) {
      strcpy(api_name, "OpenCL");
      strcpy(device_name, "-gpu");
      apiSet = true;
    }

    if (argv[i] == string("-N")) {
      size = atoi(argv[i+1]);
      i++;
    }

  }

  if (!apiSet) {
    strcpy(api_name, "Cuda");
    strcpy(device_name, "-gpu");
  }
    
  cout << "=========================BEGIN TEST=========================" << endl;
  cout << "Use api: " << api_name << "\t" << device_name << endl;
  cout << "Number of randoms: " << size << endl;

  //init dks
  int ierr;
  DKSBase base;
  base.setAPI(api_name, strlen(api_name));
  base.setDevice(device_name, strlen(api_name));
  base.initDevice();
  base.callInitRandoms(size);

  //create host vector to store results
  double *host_data = new double[size];

  //create device vector
  void *device_data = base.allocateMemory<double>(size, ierr);
  
  for (int i = 0; i < 5; i++) {
    //fill device vector with random values
    base.callCreateRandomNumbers(device_data, size);

    //read device vector
    base.readData<double>(device_data, host_data, size);

    //print host data
    for (int i = 0; i < size; i++)
      cout << host_data[i] << " ";
    cout << endl;
  }

  //free device vector
  base.freeMemory<double>(device_data, size);
  
  //free host data
  delete[] host_data;

  return 0;
}
