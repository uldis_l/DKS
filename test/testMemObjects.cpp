#include <iostream>
#include <cstdlib>

#include "DKSBase.h"

using namespace std;

int main(int argc, char *argv[]) {
	
	int ierr,n, N;

	if (argc > 1)
		n = atoi(argv[1]);
	else
		n = 10;

	N = 2 << n;
	cout << "Elements: " << N << endl;

	double *data = new double[N];
	for (int i = 0; i < N; i++)
		data[i] = (double)i / N;


	DKSBase base = DKSBase();
	base.setAPI("OpenCL", 6);
	base.setDevice("-gpu", 4);
	base.initDevice();
	
	void *ptr1;
	ptr1 = base.allocateMemory<double>(N, ierr);
	ierr = base.writeData<double>(ptr1, data, N);
	
	void *ptr2;
	ptr2 = base.allocateMemory<double>(N, ierr);
	ierr = base.writeData<double>(ptr2, data, N);
	
	void *ptr3;
	ptr3 = base.allocateMemory<double>(N, ierr);
	ierr = base.writeData<double>(ptr3, data, N);
	
	void *ptr4;
	ptr4 = base.allocateMemory<double>(N, ierr);
	ierr = base.writeData<double>(ptr4, data, N);
	
	void *ptr5;
	ptr5 = base.allocateMemory<double>(N, ierr);
	ierr = base.writeData<double>(ptr5, data, N);
	
	void *ptr6;
	ptr6 = base.allocateMemory<double>(N, ierr);
	ierr = base.writeData<double>(ptr6, data, N);
	
	void *ptr7;
	ptr7 = base.allocateMemory<double>(N, ierr);
	ierr = base.writeData<double>(ptr7, data, N);
	
	void *ptr8;
	ptr8 = base.allocateMemory<double>(N, ierr);
	ierr = base.writeData<double>(ptr8, data, N);
	
	base.freeMemory<double>(ptr1, N);
	base.freeMemory<double>(ptr2, N);
	base.freeMemory<double>(ptr3, N);
	base.freeMemory<double>(ptr4, N);
	base.freeMemory<double>(ptr5, N);
	base.freeMemory<double>(ptr6, N);
	base.freeMemory<double>(ptr7, N);
	base.freeMemory<double>(ptr8, N);
	
	
		
	return 0;
}

