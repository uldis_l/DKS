#include <iostream>
#include <cstdlib>
#include <vector>

#include "DKSBase.h"

#include <vector_types.h>
#include "cuda_runtime.h"

using namespace std;


void initData(double3 *data, int N) {
  for (int i = 0; i < N; i++) {
    data[i].x = rand() / RAND_MAX;
    data[i].y = rand() / RAND_MAX;
    data[i].z = rand() / RAND_MAX;
  }
}


int main() {
  
  int ierr;
  int N = 1000000;
  double3 *R = new double3[N];
  double3 *P = new double3[N];

  initData(R, N);
  initData(P, N);

  DKSBase dksbase;
  dksbase.setAPI("Cuda", 4);
  dksbase.setDevice("-gpu", 4);
  dksbase.initDevice();

  void *r_ptr, *p_ptr;
  
  r_ptr = dksbase.allocateMemory<double3>(N, ierr);
  p_ptr = dksbase.allocateMemory<double3>(N, ierr);

  dksbase.writeData<double3>(r_ptr, R, N);
  dksbase.writeData<double3>(p_ptr, P, N);

  for (int i = 0; i < 100; i++)
    dksbase.callParallelTTrackerPush(r_ptr, p_ptr, N, NULL, 0.5, 1, false);


  dksbase.readData<double3>(r_ptr, R, N);
  dksbase.readData<double3>(p_ptr, P, N);

  dksbase.freeMemory<double3>(r_ptr, N);
  dksbase.freeMemory<double3>(p_ptr, N);


  return 0;
}
