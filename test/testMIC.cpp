#include <iostream>
#include "DKSBase.h"

using namespace std;

int main() {

  DKSBase base;
	
  base.setAPI("OpenMP", 6);
  base.initDevice();
	
  //init data
  int ierr;
  int N = 8;
  double *in_data = new double[N];
  double *in_data2 = new double[N];
  double *out_data = new double[N];
  double *out_data2 = new double[N];
	
  for (int i = 0; i < N; i++) {
    in_data[i] = i;
    in_data2[i] = i*i;
  }
		
  //test memory allocation, write and read operations
  void *d_ptr, *d2_ptr;
  
  d_ptr = base.allocateMemory<double>(N, ierr);
  d2_ptr = base.allocateMemory<double>(N, ierr);
 	
  base.writeData<double>(d_ptr, in_data, N);
  base.writeData<double>(d2_ptr, in_data2, N);
	
  base.readData<double>(d_ptr, out_data, N);
  base.readData<double>(d2_ptr, out_data2, N);
  base.freeMemory<double>(d_ptr, N);
  base.freeMemory<double>(d2_ptr, N);
 		
  //print results
  for (int i = 0; i < N; i++)
    cout << out_data[i] << "\t";
  cout << endl;	
	
  for (int i = 0; i < N; i++)
    cout << out_data2[i] << "\t";
  cout << endl;	
	
  return 0;

}
