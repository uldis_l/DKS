#include <iostream>
#include <cstdlib>

#include "DKSBase.h"

using namespace std;

int main(int argc, char *argv[]) {
	
	char *api_name = new char[10];
	char *device_name = new char[10];
	if (argc == 2) {
		strcpy(api_name, argv[1]);
		strcpy(device_name, "-gpu");
	} else if (argc == 3) {
		strcpy(api_name, argv[1]);
		strcpy(device_name, argv[2]);
	} else {
		strcpy(api_name, "OpenCL");
		strcpy(device_name, "-gpu");
	}
	
	
	int ierr,n, N;

	N = 8;
	n = 4;

	double *data_in = new double[N];
	double *data_out_1 = new double[N];
	double *data_out_2 = new double[N];
	for (int i = 0; i < N; i++) {
		data_in[i] = (double)i / N;
		data_out_1[i] = 0.0;
		data_out_2[i] = 0.0;
	}

	cout << "Run example on: " << api_name << " using " << device_name << endl;

	DKSBase base = DKSBase();
	base.setAPI(api_name, strlen(api_name));
	base.setDevice(device_name, strlen(api_name));
	base.initDevice();
	
	void *ptr1;
	ptr1 = base.allocateMemory<double>(N, ierr);
	
	ierr = base.writeData<double>(ptr1, data_in, n, 0);
	ierr = base.writeData<double>(ptr1, data_in, n, 4);
	
	ierr = base.readData<double>(ptr1, data_out_1, N);
	ierr = base.readData<double>(ptr1, data_out_2, n, 2);
	
	base.freeMemory<double>(ptr1, N);
	
	for (int i = 0; i < N; i++)
		cout << data_in[i] << "\t";
	cout << endl;
	
	for (int i = 0; i < N; i++)
		cout << data_out_1[i] << "\t";
	cout << endl;
	
	for (int i = 0; i < N; i++)
		cout << data_out_2[i] << "\t";
	cout << endl;
		
	
	
		
	return 0;
}

