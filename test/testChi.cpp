#include <iostream>
#include <complex>
#include <cstdlib>

#include "DKSBase.h"
#include "Utility/TimeStamp.h"

using namespace std;

int main(int argc, char *argv[]) {

  char *api_name = new char[10];
  char *device_name = new char[4];


  if (argc == 3) {
    strcpy(api_name, argv[1]);
    strcpy(device_name, argv[2]);
  } else if (argc == 2){
    strcpy(api_name, argv[1]);
    strcpy(device_name, "-gpu");
  } else {
    strcpy(api_name, "OpenCL");
    strcpy(device_name, "-gpu");
  }

  cout << "Use api: " << api_name << endl;

  cout << "Begin DKS Base tests" << endl;
	
  /* inti data */
  int ierr;
  int nsize = 4000000;
  int jsize = 16;
  int psize = 6;
  double *data = new double[nsize*jsize];
  double *p = new double[psize*jsize];
  double data_out = 0;
	
  srand(time(NULL));
  for (int i = 0; i < nsize*jsize; i++) {
    //int sign = ((double)rand()/RAND_MAX > 0.5) ? 1 : -1;
    //data[i] = sign*(double)rand()/RAND_MAX;
    data[i] = (double)i / (nsize*jsize);
    //data[i] = 1;
  }
  for (int i = 0; i < psize*jsize; i++) {
    //int sign = ((double)rand()/RAND_MAX > 0.5) ? 1 : -1;
    //p[i] = sign*(double)rand()/RAND_MAX;
    p[i] = (double)i / (nsize*jsize);
    //p[i] = 1;
  }
  /* end init */
	
  timestamp_t tstart, tend;
  //timestamp_t t0, t1;
	
  tstart = get_timestamp();
		
  //init dks base class, set API to opencl and init connection with OpenCL device
  DKSBase base;
  base.setAPI(api_name, strlen(api_name));
  base.setDevice(device_name, strlen(device_name));
  base.initDevice();
	
  //ptrs to hold reference to device memory
  void *dptr, *ntptr, *pptr;
	
  //allocate memory on device
  //t0 = get_timestamp();
  dptr = base.allocateMemory<double>(nsize*jsize, ierr);
  ntptr = base.allocateMemory<double>(nsize*jsize, ierr);
  pptr = base.allocateMemory<double>(psize*jsize, ierr);
  //t1 = get_timestamp();
  //cout << "Allocate memory: " << get_secs(t0, t1) << endl;
	
  //write data to device
  //t0 = get_timestamp();
  base.writeData<double>(dptr, data, nsize*jsize);	
  //t1 = get_timestamp();
  //cout << "Write data set: " << get_secs(t0, t1) << endl << endl;
	
  for (int i = 0; i < 5; i++) {
    //write parameters to device
    //t0 = get_timestamp();
    base.writeData<double>(pptr, p, psize*jsize);
    //t1 = get_timestamp();
    //cout << "Write parameters: " << get_secs(t0, t1) << endl;

    //set function to calcNt and execute it with necessary parameters
    //t0 = get_timestamp();
    base.callNt<double>(ntptr, pptr, psize, nsize, jsize, 0.025);
    //t1 = get_timestamp();
		
    //cout << "Calc N(t): " << get_secs(t0, t1) << endl;
		
    //set function to chi2 and execute it with necessary parameters
    //t0 = get_timestamp();
    base.callChi2<double>(ntptr, dptr, ntptr, nsize*jsize);
    //t1 = get_timestamp();
    //cout << "Calc chi^2: " << get_secs(t0, t1) << endl;
	
    //set function so sum and execute it with necessary parameters
    //t0 = get_timestamp();
    base.callSum<double>(ntptr, ntptr, nsize*jsize);
    //t1 = get_timestamp();
    //cout << "Calc sum: " << get_secs(t0, t1) << endl;
	
    //read calculated sum (one value)
    //t0 = get_timestamp();
    base.readData<double>(ntptr, &data_out, 1);
    //t1 = get_timestamp();
    //cout << "Read sum: " << get_secs(t0, t1) << endl;
    cout << "Sum nt: " << data_out << endl;
		
    /*
      for (int i = 0; i < psize*jsize; i++) {
      int sign = ((double)rand()/RAND_MAX > 0.5) ? 1 : -1;
      p[i] = sign*(double)rand()/RAND_MAX;
      }
    */
		
		
    //cout << endl;
  }
	
  //free device memory
  //t0 = get_timestamp();
  base.freeMemory<double>(dptr, nsize*jsize);
  base.freeMemory<double>(ntptr, nsize*jsize);
  base.freeMemory<double>(pptr, psize*jsize);
  //t1 = get_timestamp();
  //cout << "Free memory: " << get_secs(t0, t1) << endl;
	
  tend = get_timestamp();
	
  cout << endl  << "time: " << get_secs(tstart, tend) << endl;
		
  return 0;
}

