#include <iostream>
#include <cstdlib>
#include "DKSBase.h"
#include "Utility/TimeStamp.h"

using namespace std;

int main(int argc, char *argv[]) {

	char *api_name = new char[10];
	char *device_name = new char[4];

	if (argc == 3) {
		strcpy(api_name, argv[1]);
		strcpy(device_name, argv[2]);
	} else if (argc == 2){
		strcpy(api_name, argv[1]);
		strcpy(device_name, "-gpu");
	} else {
		strcpy(api_name, "OpenCL");
		strcpy(device_name, "-gpu");
	}

	cout << "Use api: " << api_name << endl;
	cout << "Use device: " << device_name << endl;


	int ierr;
	int N = 10000;
	double *data = new double[N];
	double *data_out = new double[N];
	double *data_out2 = new double[N];
	
	for (int i = 0; i < N; i++) {
		data[i] = i;
	}
	
	//init dks base class, set API to opencl and init connection with OpenCL device
	DKSBase base;
	base.setAPI(api_name, strlen(api_name));
	base.setDevice(device_name, strlen(device_name));
	base.initDevice();
	
	//data ptr
	void *data_ptr, *data_ptr2;
	
	//allocate memory
	data_ptr = base.allocateMemory<double>(N, ierr);
	data_ptr2 = base.allocateMemory<double>(N, ierr);
	
	//write data to memory and fill data on device
	base.writeData<double>(data_ptr, data, N);
	base.writeData<double>(data_ptr2, data, N);
	//base.callNt<double>(data_ptr2, data_ptr, 6, N, 1, 0);
	
	//calc sum
	base.callSum<double>(data_ptr2, data_ptr2, N);
	
	//base.callSum<double>(data_ptr, data_ptr, N);
	
	//chi^2
	//base.callChi2<double>(data_ptr, data_ptr, data_ptr, N);
	//base.callChi2<double>(data_ptr2, data_ptr2, data_ptr2, N);
	
	//read data
	base.readData<double>(data_ptr, data_out, N);
	base.readData<double>(data_ptr2, data_out2, N);
	
	//base.oclEventInfo();
	
	//free memory
	base.freeMemory<double>(data_ptr, N);
	base.freeMemory<double>(data_ptr2, N);
	
	
	/*
	for (int i = 0; i < N; i++) {
		cout << data[i] << "\t";
	}
	cout << endl << endl;
	for (int i = 0; i < N; i++) {
		cout << data_out[i] << "\t";
	}
	cout << endl << endl;
	for (int i = 0; i < N; i++) {
		cout << data_out2[i] << "\t";
	}
	cout << endl;
	*/



	return 0;
}