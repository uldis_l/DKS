#include <iostream>
#include <cstdlib>
#include <complex>

#include "Utility/TimeStamp.h"
#include "DKSBase.h"

using namespace std;

int main(int argc, char *argv[]) {

  char *api_name = new char[10];
  char *device_name = new char[10];
  if (argc == 2) {
    strcpy(api_name, argv[1]);
    strcpy(device_name, "-gpu");
  } else if (argc == 3) {
    strcpy(api_name, argv[1]);
    strcpy(device_name, argv[2]);
  } else {
    strcpy(api_name, "OpenCL");
    strcpy(device_name, "-gpu");
  }

  cout << "Use api: " << api_name << "\t" << device_name << endl;

  cout << "Begin DKS Base tests" << endl;
		
  int N = 2;
  int dimsize[3] = {N, N, N};
	
  complex<double> *cdata = new complex<double>[N];
  complex<double> *cfft = new complex<double>[N];
  for (int i = 0; i < N; i++) {
    cdata[i] = complex<double>(0, 0);
    cfft[i] = complex<double>(0, 0);
  }
	
  cdata[0] = complex<double>(1.73205, 1.73205);
	
  timestamp_t t0, t1;
	
  /* init DKSBase */
  cout << "Init device and set function" << endl;
  DKSBase base;
  base.setAPI(api_name, strlen(api_name));
  base.setDevice(device_name, strlen(api_name));
  base.initDevice();
	
  void *mem_ptr;
  int ierr;
	
  /* write data to device */	
  mem_ptr = base.pushData< complex<double> >( (const void*)cdata, N, ierr);

  /* execute fft */
  base.callFFT(mem_ptr, 1, dimsize);
	
  /* execute ifft */	
  base.callIFFT(mem_ptr, 1, dimsize);
	
  /* execute normalize */
  base.callNormalizeFFT(mem_ptr, 1, dimsize);
	
  /* read data from device */
  base.pullData< complex<double> >(mem_ptr, cfft, N);
	
  /* print results */
	
  cout << "Data" << endl;
  for (int i = 0; i < N; i++)
    cout << cdata[i] << "\t";
  cout << endl;
	
  cout << "FFT" << endl;
  for (int i = 0; i < N; i++)
    cout << cfft[i] << "\t";
  cout << endl;
	
		
  return 0;
}

