#include <iostream>
#include <cstdlib>

#include "DKSBase.h"

using namespace std;

typedef struct {
  double x;
  double y;
  double z;
} Part;

void initData(Part *data, int N) {
  for (int i = 0; i < N; i++) {
    data[i].x = rand() / RAND_MAX;
    data[i].y = rand() / RAND_MAX;
    data[i].z = rand() / RAND_MAX;
  }
}

int main() {

  int ierr;
  int N = 100000;

  //__declspec(align(64)) Part *R = new Part[N];
  //__declspec(align(64)) Part *P = new Part[N];
  Part *R = new Part[N];
  Part *P = new Part[N];

  initData(R, N);
  initData(P, N);

  DKSBase dksbase;
  dksbase.setAPI("OpenMP", 6);
  dksbase.setDevice("-mic", 4);
  dksbase.initDevice();

  void *r_ptr, *p_ptr, *dt_ptr;
  r_ptr = dksbase.allocateMemory<Part>(N, ierr);
  p_ptr = dksbase.allocateMemory<Part>(N, ierr);
  dt_ptr = dksbase.allocateMemory<double>(N, ierr);

  dksbase.writeData<Part>(r_ptr, R, N);

  cout << "====================START PUSH====================" << endl;

  for (int i = 0; i < 5; i++) {
    //write r to device
    dksbase.writeData<Part>(r_ptr, R, N);
    //calc push
    dksbase.callParallelTTrackerPush (r_ptr, p_ptr, N, dt_ptr,
				      0.001, 1, false, NULL);
    //read R from device
    dksbase.readDataAsync<Part> (r_ptr, R, N, NULL);
  }

  cout << "====================END PUSH====================" << endl;



  dksbase.freeMemory<Part>(r_ptr, N);
  dksbase.freeMemory<Part>(p_ptr, N);
  dksbase.freeMemory<double>(dt_ptr, N);

  return 0;
}
