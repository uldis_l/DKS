#!/bin/bash 
export  MIC_ENV_PREFIX=MIC
echo $MIC_ENV_PREFIX
export  MIC_OMP_NUM_THREADS=236
echo $MIC_OMP_NUM_THREADS
export MIC_KMP_PLACE_THREADS=59c4t0o
echo $MIC_KMP_PLACE_THREADS
export MIC_USE_2MB_BUFFERS=64K
echo $MIC_USE_2MB_BUFFERS
export MIC_KMP_AFFINITY=scatter
echo $MIC_KMP_AFFINITY

#./testFFT3DRC 256 256 256

echo 'real strides divisible by 4 but not by 8'
#./testFFT3DRC 257 244 268
#./testFFT3DRC 244 268 257
#./testFFT3DRC 268 257 244
#./testFFT3DRC 257 268 244
#./testFFT3DRC 244 257 268
#./testFFT3DRC 268 244 257

echo 'real strides divisible by 8 but not by 16'
#./testFFT3DRC 257 248 263
#./testFFT3DRC 248 263 257
#./testFFT3DRC 263 257 248
#./testFFT3DRC 257 263 248
#./testFFT3DRC 248 257 263
#./testFFT3DRC 263 248 257

echo 'complex strides divisible by 4 but not by 8'
#./testFFT3DRC 257 246 268
#./testFFT3DRC 246 268 257
#./testFFT3DRC 268 257 246
#./testFFT3DRC 257 268 246
#./testFFT3DRC 246 257 268
#./testFFT3DRC 268 246 257

echo 'complex strides divisible by 8 but not by 16'
#./testFFT3DRC 257 206 317
#./testFFT3DRC 206 317 257
#./testFFT3DRC 317 257 206
#./testFFT3DRC 257 317 206
#./testFFT3DRC 206 257 317
#./testFFT3DRC 317 206 257

echo 'perform scaling tests'
export  MIC_OMP_NUM_THREADS=1
echo $MIC_OMP_NUM_THREADS
export MIC_KMP_PLACE_THREADS=1c1t0o
echo $MIC_KMP_PLACE_THREADS
#./testFFT3DRC 256 256 256

export  MIC_OMP_NUM_THREADS=2
echo $MIC_OMP_NUM_THREADS
export MIC_KMP_PLACE_THREADS=1c2t0o
echo $MIC_KMP_PLACE_THREADS
#./testFFT3DRC 256 256 256



export  MIC_OMP_NUM_THREADS=3
echo $MIC_OMP_NUM_THREADS
export MIC_KMP_PLACE_THREADS=1c3t0o
echo $MIC_KMP_PLACE_THREADS
#./testFFT3DRC 256 256 256


export  MIC_OMP_NUM_THREADS=4
echo $MIC_OMP_NUM_THREADS
export MIC_KMP_PLACE_THREADS=1c4t0o
echo $MIC_KMP_PLACE_THREADS
#./testFFT3DRC 256 256 256

NUM_PROC="2 4 8 16 32 59"
for p in $NUM_PROC; do
	t=$(($p * 4))
	echo $t
	export  MIC_OMP_NUM_THREADS=$t
	echo $MIC_OMP_NUM_THREADS
	mystring="$p"
	mystring+="c4t0o"
	export MIC_KMP_PLACE_THREADS=$mystring
	echo $MIC_KMP_PLACE_THREADS
	./testFFT3DRC 256 256 256

	
done









